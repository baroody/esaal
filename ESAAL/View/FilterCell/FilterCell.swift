//
//  FilterCell.swift
//  ESAAL
//
//  Created by Mina Thabet on 11/8/19.
//  Copyright © 2019 HardTask. All rights reserved.
//

import UIKit
import IBAnimatable

class FilterCell: UICollectionViewCell {

    //MARK:- Iboutlet
    @IBOutlet weak var subjectNameLabel: UILabel!
    @IBOutlet weak var contentSubjectLabelView: AnimatableView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override var isSelected: Bool{
        didSet{
            contentSubjectLabelView.borderColor = isSelected ? hexStringToUIColor(hex: "#0A68B0") : hexStringToUIColor(hex: "#EFEFEF")
        }
    }
    
    // MARK:- Init Cell
    func configureCell(subjectName: String) {
        subjectNameLabel.text = subjectName
        setupFont()
    }

    
    /// init fonts depend on language
    func setupFont() {
        mainQueue {
        self.subjectNameLabel.font = UIFont(name: localizedSitringFor(key: "AppRegularFont"), size: iphoneXFactor*36 )
        }
    }

}
