//
//  PaymentHistoryCell.swift
//  ESAAL
//
//  Created by Mina Thabet on 11/6/19.
//  Copyright © 2019 HardTask. All rights reserved.
//

import UIKit

class PaymentHistoryCell: UITableViewCell {

    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet var firstTitleLAbel: UILabel!
    @IBOutlet var packageNameLabel: UILabel!
    @IBOutlet weak var remainingQuestionLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupFont()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
     
    func initCell(withSub sub: UserSubscribtion){
        
        var dString : String = sub.creationDate ?? ""
        dString = dString.replacingOccurrences(of: "T", with: " ")
        dString = UTCToLocal(date: dString, returnFormat: "dd/MM/yyyy")
        firstTitleLAbel.text = "\(dString)"
        let roundedValue2 = String(format: "%.3f", Double(sub.subscription?.price ?? 0))

        titleLabel.text = "\(localizedSitringFor(key: "YouPaid")) \(roundedValue2) \(localizedSitringFor(key: "currency")) \(localizedSitringFor(key: "ForRenew"))"
        packageNameLabel.text = sub.subscription?.Name ?? ""
        
        var dString1 : String = sub.subScriptionDate ?? ""
        dString1 = dString1.replacingOccurrences(of: "T", with: " ")
        dString1 = UTCToLocal(date: dString1, returnFormat: "dd/MM/yyyy")

        var dString2 : String = sub.subsciptionEndDate ?? ""
        dString2 = dString2.replacingOccurrences(of: "T", with: " ")
        dString2 = UTCToLocal(date: dString2, returnFormat: "dd/MM/yyyy")

        dateLabel.text = "\(localizedSitringFor(key: "from")) \(dString1) \(localizedSitringFor(key: "to")) \(dString2)"
        remainingQuestionLabel.text = "\(localizedSitringFor(key: "reminigQuestion")) \(sub.remainQuestionNumber ?? 0)"
    }
    
      /// setup font for controller
      func setupFont(){
          mainQueue {
              self.firstTitleLAbel.font = UIFont(name: localizedSitringFor(key: "AppRegularFont"), size: iphoneXFactor*45 )
              self.dateLabel.font = UIFont(name: localizedSitringFor(key: "AppRegularFont"), size: iphoneXFactor*45 )
              self.titleLabel.font = UIFont(name: localizedSitringFor(key: "AppRegularFont"), size: iphoneXFactor*45 )
              self.packageNameLabel.font = UIFont(name: localizedSitringFor(key: "AppBoldFont"), size: iphoneXFactor*45 )
              self.remainingQuestionLabel.font = UIFont(name: localizedSitringFor(key: "AppRegularFont"), size: iphoneXFactor*45 )
          }
      }
    
    func UTCToLocal(date:String, returnFormat : String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss.SSSS"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
        
        let dt = dateFormatter.date(from: date)
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = returnFormat
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
        
        return dateFormatter.string(from: dt!)
    }
}


extension Double {
    /// Rounds the double to decimal places value
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}

extension NSMutableAttributedString {
    @discardableResult func bold(_ text: String) -> NSMutableAttributedString {
        let attrs: [NSAttributedString.Key: Any] = [.font: UIFont(name: "AvenirNext-Medium", size: 12)!]
        let boldString = NSMutableAttributedString(string:text, attributes: attrs)
        append(boldString)

        return self
    }

    @discardableResult func normal(_ text: String) -> NSMutableAttributedString {
        let normal = NSAttributedString(string: text)
        append(normal)

        return self
    }
}
