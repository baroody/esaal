//
//  MyAccountCenterCell.swift
//  Maarsena
//
//  Created by Mina Thabet on 1/21/20.
//  Copyright © 2020 HardTask. All rights reserved.
//

import UIKit

class MyAccountCenterCell: UICollectionViewCell {

    //MARK:- IBOutlet
    @IBOutlet weak var cellImageView: UIImageView!
    @IBOutlet weak var cellLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupFont()
        // Initialization code
    }

}

//MARK:- Helpers
extension MyAccountCenterCell {
    
    func configureCellWith(accountList: AccountList) {
        cellLabel.text = accountList.label
        cellImageView.image = UIImage(named: accountList.image)
    }
    
    /// init fonts depend on language
    func setupFont() {
        mainQueue {
        self.cellLabel.font = UIFont(name: localizedSitringFor(key: "AppRegularFont"), size: iphoneXFactor*45)
        }
    }
}
