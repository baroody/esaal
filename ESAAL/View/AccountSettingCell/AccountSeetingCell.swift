//
//  AccountSeetingCell.swift
//  ESAAL
//
//  Created by Mina Thabet on 10/31/19.
//  Copyright © 2019 HardTask. All rights reserved.
//

import UIKit

class AccountSeetingCell: UICollectionViewCell {

    //MARK:- IBoutlet
    @IBOutlet weak var accountImageView: UIImageView!
    @IBOutlet weak var accountLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    //MARK:- init cell
    func configureCell(accountList:AccountList){
        accountImageView.image = UIImage(named: accountList.image)
        accountLabel.text = accountList.label
        mainQueue {
            self.accountLabel.font = UIFont(name: localizedSitringFor(key: "AppRegularFont"), size: iphoneXFactor*45 )

        }
    }
}
