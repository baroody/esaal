//
//  TeacherHomeTableViewCell.swift
//  ESAAL
//
//  Created by Mina Thabet on 11/10/19.
//  Copyright © 2019 HardTask. All rights reserved.
//

import UIKit
import IBAnimatable


class TeacherHomeTableViewCell: UITableViewCell {

    //MARK:- IBOutlet
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var descriptionLabel: UILabel!
    @IBOutlet weak var b_essalStudentLabel: UILabel!
    @IBOutlet weak var r_questionDateLabel: UILabel!
    @IBOutlet weak var contentQuestionView: AnimatableView!
    
    var isMyPending = false
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupFont()
    }
    
    //MARK:- Init cell
    func configureCell(question: Question, isPending:Bool = false){
        isMyPending = isPending
        if isMyPending{
            contentQuestionView.backgroundColor = hexStringToUIColor(hex: "#35C2ED")
            r_questionDateLabel.textColor = .black
        }else {
            contentQuestionView.backgroundColor = UIColor.white
            r_questionDateLabel.textColor = .lightGray
        }
        b_essalStudentLabel.text = localizedSitringFor(key: "EssalStudent")
        titleLabel.text = question.material?.name
        descriptionLabel.text = question.description
        r_questionDateLabel.text = getDateFormat(dateString: question.creationDate ?? "")
        
    }

    
    /// init fonts depend on language
    func setupFont() {
        mainQueue {
            self.titleLabel.font = UIFont(name: localizedSitringFor(key: "AppBoldFont"), size: iphoneXFactor*40 )
            self.descriptionLabel.font = UIFont(name: localizedSitringFor(key: "AppRegularFont"), size: iphoneXFactor*40 )
            self.b_essalStudentLabel.font = UIFont(name: localizedSitringFor(key: "AppBoldFont"), size: iphoneXFactor*45 )
            self.r_questionDateLabel.font = UIFont(name: localizedSitringFor(key: "AppRegularFont"), size: iphoneXFactor*30 )
        }
    }
    
    func getDateFormat(dateString: String) -> String {
        var dString = dateString
        dString = dString.replacingOccurrences(of: "T", with: " ")
        if let index = dString.indexInt(of: ".") {
            dString = dString.substring(to: index - 1)
        }
        print(dString,"osama")
        dString = UTCToLocal(date: dString,localForamt: "yyyy-MM-dd HH:mm:ss" ,returnFormat: "MM/dd/yyyy | hh:mm a")
        return dString
    }

}
