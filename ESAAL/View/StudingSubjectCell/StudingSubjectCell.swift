//
//  StudingSubjectCell.swift
//  ESAAL
//
//  Created by Mina Thabet on 11/6/19.
//  Copyright © 2019 HardTask. All rights reserved.
//

import UIKit

class StudingSubjectCell: UICollectionViewCell {

    @IBOutlet weak var subjectNameLabel: UILabel!
    @IBOutlet weak var lineView: UIView!
    
    // Initialization code
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override var isSelected: Bool {
        didSet{
            subjectNameLabel.textColor = isSelected ? hexStringToUIColor(hex: "454545") : hexStringToUIColor(hex: "7B7B7B")
            subjectNameLabel.font = UIFont(name: isSelected ? localizedSitringFor(key: "AppBoldFont") : localizedSitringFor(key: "AppRegularFont"), size: iphoneXFactor*40 )
            lineView.backgroundColor = isSelected ? hexStringToUIColor(hex: "0A68B0") : UIColor.white
        }
    }
    
    func configureCell(subjectName: String) {
        subjectNameLabel.text = subjectName
    }

}
