//
//  QuestionReplayCell.swift
//  ESAAL
//
//  Created by Mina Thabet on 11/14/19.
//  Copyright © 2019 HardTask. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
import ReplayKit
import SDWebImage
import IBAnimatable
import Kingfisher

protocol QuestionReplayCellDelegate: NSObjectProtocol {
    func replayActionClicked(_ sender: QuestionReplayCell)
    func openImageActionClicked(_ sender: QuestionReplayCell)
    func openVideoActionClicked(_ sender: QuestionReplayCell)
    func disLikeActionClicked(_ sender: QuestionReplayCell)
    func likeActionClicked(_ sender: QuestionReplayCell)
}

class QuestionReplayCell: UITableViewCell {

    //MARK:- IBOutlet
    @IBOutlet var esaalLabel: UILabel!
    @IBOutlet weak var essalImageView: AnimatableImageView!
    @IBOutlet var replyLAbel: UILabel!
    @IBOutlet weak var replayView: UIView!
    @IBOutlet weak var questionImageView: UIImageView!
    @IBOutlet weak var questionVideoImageView: UIImageView!
    @IBOutlet var descriptionLabel: UILabel!
    @IBOutlet weak var disLikeView: UIView!
    @IBOutlet weak var likeImageView: UIImageView!
    @IBOutlet weak var dislikeImageView: UIImageView!
    @IBOutlet weak var mediaView: UIView!
    @IBOutlet weak var mediaImageContainerView: UIView!
    @IBOutlet weak var mediaVideoContainerView: UIView!
    @IBOutlet weak var mediaContetnAspectRationConstraint: NSLayoutConstraint!
    
    //MARK:- variables
    weak var deleget: QuestionReplayCellDelegate?
    var isLike: Bool = false
    var isDislike: Bool = false
    
    var videoImage: UIImage? {
        didSet {
            mainQueue {
                self.questionVideoImageView.image = self.videoImage
            }
            
        }
    }
    
    
    //MARK:- Initialization
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    //MARK:- IBAction
    @IBAction func replayAction(_ sender: Any) {
        self.deleget?.replayActionClicked(self)
    }
    
    @IBAction func imageClickedAction(_ sender: Any) {
        self.deleget?.openImageActionClicked(self)
    }
    
    @IBAction func videoClickedAction(_ sender: Any) {
        self.deleget?.openVideoActionClicked(self)
    }
    
    @IBAction func likeAction(_ sender: Any) {
        if isDislike && !isLike {
            isDislike = false
            dislikeImageView.image = UIImage(named: isDislike ? "deslike_sel" : "deslike_unsel" )
        }
        isLike = !isLike
        likeImageView.image = UIImage(named: isLike ? "like_sel" : "like_unsel")
        self.deleget?.likeActionClicked(self)
    }
    
    @IBAction func disLikeAction(_ sender: Any) {
        if isLike && !isDislike {
            isLike = false
            likeImageView.image = UIImage(named: isLike ? "like_sel" : "like_unsel")
        }
        isDislike = !isDislike
        dislikeImageView.image = UIImage(named: isDislike ? "deslike_sel" : "deslike_unsel" )
        self.deleget?.disLikeActionClicked(self)
    }
    
}

//MARK:- Helpers
extension QuestionReplayCell{
    
    /// init cell
    func configureCell(questionReplay: ReplayQuestions) {
        setupFont()
        setUserNameLogic(userId: questionReplay.userId ?? 0)
        descriptionLabel.text = questionReplay.replayMessage
        setupAttachmentMedia(questionReplay: questionReplay)
        if !disLikeView.isHidden{
            setupLikeAndDislike(questionReplay: questionReplay)
        }
    }
    
    /// set user name( essal student or essal teatcher)
    func setUserNameLogic(userId: Int){
        // check if current user is teatcher or student
        if Account.shared.isTeacher {
            // if teatcher
            // 1- hide like and dislike view
            disLikeView.isHidden = true
            // if current id equal replay question id
            if userId == Account.shared.id {
                // hide replay because this replay belong to current user
                // set essal label to essal teatcher
                replayView.isHidden = true
                esaalLabel.text = localizedSitringFor(key: "EssalTeatcher")
                essalImageView.image = UIImage(named: "teacher")
            }else {
                // if current id not equal replay question id
                // show replay view
                // set essal label to essal student
                replayView.isHidden = false
                esaalLabel.text = localizedSitringFor(key: "EssalStudent")
                essalImageView.image = UIImage(named: "student")
            }
        }else {
            // if current user student
            if userId == Account.shared.id {
                // if current id equal replay question id
                // hide replay because this replay belong to current user
                // hide dislike and like view because this replay belong to current user
                // set essal label to essal student
                replayView.isHidden = true
                disLikeView.isHidden = true
                esaalLabel.text = localizedSitringFor(key: "EssalStudent")
                essalImageView.image = UIImage(named: "student")
            }else {
                // if current id not equal replay question id
                // show replay view
                // show dislike and like
                // set essal label to essal teatcher
                replayView.isHidden = false
                disLikeView.isHidden = false
                esaalLabel.text = localizedSitringFor(key: "EssalTeatcher")
                essalImageView.image = UIImage(named: "teacher")
            }
        }
    }
       
   /// init fonts depend on language
   func setupFont() {
       mainQueue {
       self.esaalLabel.font = UIFont(name: localizedSitringFor(key: "AppBoldFont"), size: iphoneXFactor*45 )
       self.replyLAbel.font = UIFont(name: localizedSitringFor(key: "AppRegularFont"), size: iphoneXFactor*45 )
       self.descriptionLabel.font = UIFont(name: localizedSitringFor(key: "AppRegularFont"), size: iphoneXFactor*45 )
       }
   }
    
    /// set media logic
    func setupAttachmentMedia(questionReplay: ReplayQuestions) {
        var count = 0
        var isImageFound = false
        var isVideoFound = false
        if let attachmentMedia = questionReplay.attachments {
            count = attachmentMedia.count
            for media in attachmentMedia {
                if media.fileType == "i"{
                    if let url = media.fileUrl?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) {
                        isImageFound = true
                        if let imageUrl = URL(string: url){
                            questionImageView.kf.indicatorType = .activity
                            questionImageView.kf.setImage(with: imageUrl,placeholder: UIImage(named: "icon_1024"))
                        }
//                        questionImageView.sd_setImage(with: URL(string: url), placeholderImage: UIImage(named: "icon_1024"))
                    }
                }else if media.fileType == "v" {
                    if let url = media.filePlaceholderUrl?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) {
                        if let videoUrl = URL(string: url) {
                            isVideoFound = true
                            self.questionVideoImageView.kf.indicatorType = .activity
                            self.questionVideoImageView.kf.setImage(with: videoUrl)
                        }
                    }
                    else {
                        isVideoFound = true
                    }
                }
            }
        }
        showOrHideMedia(count: count, isImageFound: isImageFound, isVideoFound: isVideoFound)
    }
    
    func showOrHideMedia(count: Int,isImageFound: Bool,isVideoFound: Bool){
        if count == 0{
            // no media
            mediaView.isHidden = true
            if disLikeView.isHidden {
                 mediaContetnAspectRationConstraint.constant = screenWidth * 0.6
            }
        }else {
            mediaView.isHidden = false
            mediaContetnAspectRationConstraint.constant = 0
        }
        
        if !isImageFound {
            mediaImageContainerView.isHidden = true
        }else {
            mediaImageContainerView.isHidden = false
        }
        if !isVideoFound {
            mediaVideoContainerView.isHidden = true
        }else {
            mediaVideoContainerView.isHidden = false
        }
    }
    
    func setupLikeAndDislike(questionReplay: ReplayQuestions){
        isLike = questionReplay.isLiked ?? false
        isDislike = questionReplay.isDisliked ?? false
        likeImageView.image = UIImage(named: isLike ? "like_sel" : "like_unsel")
        dislikeImageView.image = UIImage(named: isDislike ? "deslike_sel" : "deslike_unsel" )
    }
    
    
    ///get image from video url
       func getThumbnailImageFromVideoURL(fromUrl url: URL) -> UIImage? {
           let asset: AVAsset = AVAsset(url: url)
           let imageGenerator = AVAssetImageGenerator(asset: asset)
           imageGenerator.appliesPreferredTrackTransform = true
           do {
               let thumbnailImage = try imageGenerator.copyCGImage(at: CMTimeMake(value: 0, timescale: 1) , actualTime: nil)
               return UIImage(cgImage: thumbnailImage)
           } catch let error {
               print(error)
           }
           
           return nil
       }
    
    func getThumbnailImageFromVideoUrl(url: URL, completion: @escaping ((_ image: UIImage?)->Void)) {
        DispatchQueue.global().async { //1
            let asset = AVAsset(url: url) //2
            let avAssetImageGenerator = AVAssetImageGenerator(asset: asset) //3
            avAssetImageGenerator.appliesPreferredTrackTransform = true //4
            let thumnailTime = CMTimeMake(value: 2, timescale: 1) //5
            do {
                let cgThumbImage = try avAssetImageGenerator.copyCGImage(at: thumnailTime, actualTime: nil) //6
                let thumbNailImage = UIImage(cgImage: cgThumbImage) //7
                DispatchQueue.main.async { //8
                    completion(thumbNailImage) //9
                }
            } catch {
                print(error.localizedDescription) //10
                DispatchQueue.main.async {
                    completion(nil) //11
                }
            }
        }
    }
}
