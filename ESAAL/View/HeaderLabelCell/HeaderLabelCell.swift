//
//  HeaderLabelCell.swift
//  ESAAL
//
//  Created by Mina Thabet on 11/29/19.
//  Copyright © 2019 HardTask. All rights reserved.
//

import UIKit

class HeaderLabelCell: UITableViewCell {

    //MARK:- IBOutlet
    @IBOutlet weak var headerLabel: UILabel!
    
    //MARK:- Initialization
    override func awakeFromNib() {
        super.awakeFromNib()
        setupFonts()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    // MARK:- Init cell
    func configureCell(label: String) {
        headerLabel.text = label
    }
    
    func setupFonts() {
        mainQueue {
            self.headerLabel.font = UIFont(name: localizedSitringFor(key: "AppBoldFont"), size: iphoneXFactor*50 )
        }
    }
    
}
