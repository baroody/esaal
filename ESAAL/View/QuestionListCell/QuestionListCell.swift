//
//  QuestionListCell.swift
//  ESAAL
//
//  Created by Mina Thabet on 10/31/19.
//  Copyright © 2019 HardTask. All rights reserved.
//

import UIKit
import IBAnimatable

class QuestionListCell: UITableViewCell {

    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var detailseLabel: UILabel!
    @IBOutlet weak var circleView: AnimatableView!
    @IBOutlet weak var questionDescriptionLabel: UILabel!
    
    func initCell(withNotification notification: NewNotification){
        setupFont()
        print("isQuestion",notification.isQuestion ?? false)
        if (notification.isQuestion ?? false) {
            titleLabel.isHidden = false
            questionDescriptionLabel.isHidden = false
        }else {
            titleLabel.isHidden = true
            questionDescriptionLabel.isHidden = true
        }
        dateLabel.text = notification.creationDate ?? ""
        titleLabel.text = notification.material ?? ""
        detailseLabel.text = notification.message?.html2String
        questionDescriptionLabel.text = notification.questionDescription ?? ""
        
        
        if notification.isRead ?? false{
            mainQueue {
                self.circleView.borderColor = hexStringToUIColor(hex: "D5D5D5") //UIColor.init(hexString: "D5D5D5")
            }
            

        }else{
            mainQueue {
                self.circleView.borderColor = hexStringToUIColor(hex: "35C2ED")//UIColor.init(hexString: "35C2ED")
            }
            

        }
        
        var dString : String = notification.creationDate ?? ""
        dString = dString.replacingOccurrences(of: "T", with: " ")
        dString = UTCToLocal(date: dString, returnFormat: "MM/dd/yyyy")
        dateLabel.text = "\(dString)"

    }
    

    /// setup font for controller
     func setupFont(){
         mainQueue {
             self.dateLabel.font = UIFont(name: localizedSitringFor(key: "AppRegularFont"), size: iphoneXFactor*45 )
             self.titleLabel.font = UIFont(name: localizedSitringFor(key: "AppBoldFont"), size: iphoneXFactor*45 )
             self.detailseLabel.font = UIFont(name: localizedSitringFor(key: "AppRegularFont"), size: iphoneXFactor*40)
            self.questionDescriptionLabel.font = UIFont(name: localizedSitringFor(key: "AppRegularFont"), size: iphoneXFactor*35)
         }
     }
}
