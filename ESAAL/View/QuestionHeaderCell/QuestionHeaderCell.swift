//
//  QuestionHeaderCell.swift
//  ESAAL
//
//  Created by Mina Thabet on 11/13/19.
//  Copyright © 2019 HardTask. All rights reserved.
//

import UIKit
import IBAnimatable
import AVKit
import AVFoundation
import ReplayKit
import SDWebImage

protocol QuestionHeaderCellDeleget: NSObjectProtocol {
    func reservatioOrReplayOrEdit(_ sender: QuestionHeaderCell, actionType: ActionType)
    func openImageActionClicked(_ sender: QuestionHeaderCell)
    func openVideoActionClicked(_ sender: QuestionHeaderCell)
    func cancelTimerActionClicked(_ sender: QuestionHeaderCell)
}

class QuestionHeaderCell: UITableViewCell {

    //MARK:- IBoutlet
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var matrialLabel: UILabel!
    @IBOutlet weak var replayLabel: UILabel!
    @IBOutlet weak var reservationLabel: UILabel!
    @IBOutlet weak var replayIconImage: UIImageView!
    @IBOutlet weak var editIconImageView: UIImageView!
    @IBOutlet weak var cancelView: UIView!
    @IBOutlet weak var timerLabel: UILabel!
    @IBOutlet weak var questionImageView: UIImageView!
    @IBOutlet weak var questionVideoImageView: UIImageView!
    @IBOutlet weak var questionDescriptionLabel: UILabel!
    @IBOutlet weak var arrowView: AnimatableView!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var mediaViewAspectRatio: NSLayoutConstraint!
    @IBOutlet weak var mediaView: UIView!
    @IBOutlet weak var mediaAspectRationConstraint: NSLayoutConstraint!
    @IBOutlet weak var mediaImageContainerView: UIView!
    @IBOutlet weak var mediaVideoContainerView: UIView!
    
    //MARK:- Variables
    weak var deleget: QuestionHeaderCellDeleget?
    /// action type (edit,reservation,replay,reserved)
    var actionType: ActionType?
    var timer = Timer()
    var remainTime: Int = 0
    
    var videoImage: UIImage? {
        didSet {
            mainQueue {
                self.questionVideoImageView.image = self.videoImage
            }
            
        }
    }
    
    //MARK:- Initialization Cell
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    //MARK:- IBAction
    @IBAction func addOrEditAction(_ sender: Any) {
        self.deleget?.reservatioOrReplayOrEdit(self, actionType: actionType ?? ActionType.Replay)
    }
    
    @IBAction func imageClickedAction(_ sender: Any) {
        self.deleget?.openImageActionClicked(self)
    }
    
    @IBAction func videoClickedAction(_ sender: Any) {
        self.deleget?.openVideoActionClicked(self)
    }
    
    @IBAction func cancelClickedAction(_ sender: Any) {
        self.deleget?.cancelTimerActionClicked(self)
    }
}

//MARK:- Heplers
extension QuestionHeaderCell{
    /// Init Cell
    func configureCell(question: Question) {
        setupFont()
        self.userNameLabel.text = localizedSitringFor(key: "EssalStudent")
        arrowView.isHidden = (question.replayQuestions?.count ?? 0) == 0
        // mediaView.isHidden = (question.attachment?.count ?? 0) == 0
        matrialLabel.text = question.material?.name
        questionDescriptionLabel.text = question.description
        initActionTypeLogic(question: question)
        setupAttachmentMedia(question: question)
    }
    
    /// set action type logic
    func initActionTypeLogic(question: Question) {
        // check if teatcher or student
        if Account.shared.isTeacher {
            // check if question have replay
            if (question.replayQuestions?.count ?? 0) == 0 {
                // if not have replay check is pending or not pending
                if question.isPending ?? true {
                    // if question pending
                    // check if current user is the the teatcher that pending this question
                    if Account.shared.id == question.pendingUserId ?? 0{
                        print("indsie")
                        // 1- show cancel view
                        cancelView.isHidden = false
                        cancelButton.isHidden = false
                        // 2- show replay icon
                        replayIconImage.isHidden = false
                        // 3- set replay label to replay
                        replayLabel.isHidden = false
                        reservationLabel.isHidden = true
                        replayLabel.text = localizedSitringFor(key: "replay")
                        actionType = ActionType.Replay
                    }else {
                        print("outside")
                        // if not
                        // 1- show cancel view
                        cancelView.isHidden = false
                        cancelButton.isHidden = true
                        // 2- show replay icon
                        replayIconImage.isHidden = true
                        // 3- set replay label to Reservation
                        replayLabel.isHidden = true
                        reservationLabel.isHidden = false
                        reservationLabel.text = localizedSitringFor(key: "Reserved")
                        actionType = ActionType.Reserved
                    }

                    // 4- set timer for replay
                    remainTime =  Int(question.remainTime ?? 0.0) / 1000
                    timerLabel.text = repalyTimeString(time: TimeInterval(remainTime))
                    runReplayTimer()
                }else {
                    // if not pending
                    // 1- hide replay icon
                    replayIconImage.isHidden = true
                    // 2- set replay label to Reservation
                    replayLabel.isHidden = true
                    reservationLabel.isHidden = false
                    reservationLabel.text = localizedSitringFor(key: "Reservation")
                    // 3- set action type reservation to action button
                    actionType = ActionType.Reservation
                }
                
            }else {
                // if have replay set action type replay to action button
                actionType = ActionType.Replay
            }
        }else {
            // if student
            // 1- hide replay view and icon
            replayLabel.isHidden = true
            replayIconImage.isHidden = true
            // if not have replay show edit action
            if (question.replayQuestions?.count ?? 0) == 0 {
                editIconImageView.isHidden = false
                actionType = ActionType.Edit
            }else {
                // if have replay hide edit iocn
               editIconImageView.isHidden = true
            }
            
        }
        
    }
    
    /// set media logic
    func setupAttachmentMedia(question: Question) {
        var count = 0
        var isImageFound = false
        var isVideoFound = false
        if let attachmentMedia = question.attachment {
            count = attachmentMedia.count
            for media in attachmentMedia {
                if media.fileType == "i"{
                    if let url = media.fileUrl?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) {
                        isImageFound = true
                        if let imageUrl = URL(string: url){
                            questionImageView.kf.indicatorType = .activity
                            questionImageView.kf.setImage(with: imageUrl,placeholder: UIImage(named: "icon_1024"))
                        }
//                        questionImageView.sd_setImage(with: URL(string: url), placeholderImage: UIImage(named: "icon_1024"))
                    }
                }else if media.fileType == "v" {
                    if let url = media.filePlaceholderUrl?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) {
                        if let videoUrl = URL(string: url) {
                            isVideoFound = true
                            self.questionVideoImageView.kf.indicatorType = .activity
                            self.questionVideoImageView.kf.setImage(with: videoUrl)
                        }
                    }
                    else {
                        isVideoFound = true
                    }
                }
            }
        }
        showOrHideMedia(count: count, isImageFound: isImageFound, isVideoFound: isVideoFound)
    }
    
    func showOrHideMedia(count: Int,isImageFound: Bool,isVideoFound: Bool){
        if count == 0{
            // no media
             mediaViewAspectRatio.constant = screenWidth * 0.8
             mediaView.isHidden = true
        }else if !isImageFound {
            mediaImageContainerView.isHidden = true
        }else if !isVideoFound {
            mediaVideoContainerView.isHidden = true
        }
    }
    
    /// init fonts depend on language
       func setupFont() {
           mainQueue {
           self.userNameLabel.font = UIFont(name: localizedSitringFor(key: "AppBoldFont"), size: iphoneXFactor*45 )
           self.matrialLabel.font = UIFont(name: localizedSitringFor(key: "AppBoldFont"), size: iphoneXFactor*45 )
           self.replayLabel.font = UIFont(name: localizedSitringFor(key: "AppRegularFont"), size: iphoneXFactor*45 )
            self.reservationLabel.font = UIFont(name: localizedSitringFor(key: "AppRegularFont"), size: iphoneXFactor*45 )
           self.questionDescriptionLabel.font = UIFont(name: localizedSitringFor(key: "AppRegularFont"), size: iphoneXFactor*45 )
            self.timerLabel.font = UIFont(name: localizedSitringFor(key: "AppRegularFont"), size: iphoneXFactor*45 )
            self.cancelButton.titleLabel?.font = UIFont(name: localizedSitringFor(key: "AppRegularFont"), size: iphoneXFactor*45 )
           }
       }
    
    ///get image from video url
    func getThumbnailImageFromVideoURL(fromUrl url: URL) -> UIImage? {
        let asset: AVAsset = AVAsset(url: url)
        let imageGenerator = AVAssetImageGenerator(asset: asset)
        do {
            let thumbnailImage = try imageGenerator.copyCGImage(at: CMTimeMake(value: 1, timescale: 60) , actualTime: nil)
            return UIImage(cgImage: thumbnailImage)
        } catch let error {
            print(error)
        }
        
        return nil
    }
    
    
    func runReplayTimer() {
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: (#selector(updateReplayTimer)), userInfo: nil, repeats: true)
    }
    
    @objc func updateReplayTimer() {
        if remainTime < 1 {
            timer.invalidate()
            timerLabel.text = "00:00"
        }else {
            remainTime -= 1
            timerLabel.text = repalyTimeString(time: TimeInterval(remainTime))
        }
    }

    func repalyTimeString(time: TimeInterval) -> String {
           let prodMinutes = Int(time) / 60 % 60
           let prodSeconds = Int(time) % 60

           return String(format: "%02d:%02d", prodMinutes, prodSeconds)
    }
    
}
