//
//  ChoosePackageCell.swift
//  ESAAL
//
//  Created by Mina Thabet on 10/31/19.
//  Copyright © 2019 HardTask. All rights reserved.
//

import UIKit
import IBAnimatable

class ChoosePackageCell: UITableViewCell {

    @IBOutlet weak var BLabel: UILabel!
    @IBOutlet weak var packageNameLabel: UILabel!
    @IBOutlet weak var PriceLabel_B: UILabel!
    @IBOutlet weak var DateLabel_r: UILabel!
    @IBOutlet weak var containerView: AnimatableView!
    @IBOutlet var materialsLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupFont()
    }
    
    func initView(wittSubscription sub: Subscribtion){
        BLabel.text = sub.name
        mainQueue {
            self.containerView.backgroundColor = UIColor.init(hexString: sub.colorCode ?? "")

        }

        containerView.backgroundColor = UIColor.init(hexString: sub.code ?? "")
        let priceRoundedValue = String(format: "%.3f", Double(sub.price ?? 0))
        PriceLabel_B.text = "\(priceRoundedValue) \(localizedSitringFor(key: "currency"))"
        DateLabel_r.text = localizedSitringFor(key: "month")
        packageNameLabel.text = "\(sub.numberOfQuestion ?? 0) \(localizedSitringFor(key: "question"))"
        var str = ""
        for i in 0..<(sub.subscripeMaterials?.count ?? 0){
            str += (sub.subscripeMaterials?[i].material?.name ?? "")
            if i != (sub.subscripeMaterials?.count ?? 0) - 1{
                str += " - "
            }
        }
        
        materialsLabel.text = str
        materialsLabel.numberOfLines = 0
        materialsLabel.lineBreakMode = .byWordWrapping
        self.layoutIfNeeded()
    }
    
    /// setup font for controller
      func setupFont(){
          mainQueue {
            self.BLabel.font = UIFont(name: localizedSitringFor(key: "AppBoldFont"), size: iphoneXFactor*40 )
            self.PriceLabel_B.font = UIFont(name: localizedSitringFor(key: "AppBoldFont"), size: iphoneXFactor*40 )

            self.packageNameLabel.font = UIFont(name: localizedSitringFor(key: "AppRegularFont"), size: iphoneXFactor*40 )
            self.materialsLabel.font = UIFont(name: localizedSitringFor(key: "AppRegularFont"), size: iphoneXFactor*(localizedSitringFor(key: "appLang2") == "en" ? 35 : 35) )
            //self.materialsLabel.font = UIFont(name: localizedSitringFor(key: "AppRegularFont"), size: iphoneXFactor*40 )
          }
      }
}
