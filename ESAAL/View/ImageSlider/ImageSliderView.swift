//
//  AdBannerView.swift
//  GTech
//
//  Created by Eslam Hanafy on 6/25/18.
//  Copyright © 2018 magdsoft. All rights reserved.
//

import UIKit
import ImageSlideshow
import Kingfisher
import SKPhotoBrowser

class ImageSliderView: UIView {

    @IBOutlet var bottomView: UIView!
    @IBOutlet var slider: ImageSlideshow!
    
    var images: [String] = ["https://goo.gl/images/V8FHwL",
                            "https://fptshop.com.vn/Uploads/images/2015/Tin-Tuc/NHT/Thang5-2017/logo-Apple.jpg",
                            "https://img.ibxk.com.br///2014/08/20/20145753165590-t1200x480.jpg"]
    
    
    var onClickAction: (()->())? = nil
    
    var shouldOpenGallery: Bool = false
    var parent: UIViewController!
    var imagesLinks = [SKPhoto]()

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    
    
    /// init slider with the given images
    ///
    /// - Parameters:
    ///   - images: array of image urls
    ///   - openGallery: determine if should open the image gallery when clicking on any of the images or not, true for open the gallery
    func initWith(images: [String], shouldOpenGallery openGallery: Bool = false, withController controller: UIViewController) {
        self.images = images
        print("inside \(images.count)")
        self.shouldOpenGallery = openGallery
        self.parent = controller
        
        imagesLinks.removeAll()
        for img in  images {
            
            let photo = SKPhoto.photoWithImageURL(img)
            photo.shouldCachePhotoURLImage = false // you can use image cache by true(NSCache)
            imagesLinks.append(photo)
        }
        
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.didTapOnAd))
        slider.addGestureRecognizer(gestureRecognizer)
        initImageSlider()
        
        updateSliderInputs()
    }
}

//MARK: - ImageSlideShow
extension ImageSliderView {
    
    /// init image slider
    func initImageSlider() {
        slider.contentScaleMode = .scaleAspectFill
        slider.slideshowInterval = 5.0
        slider.pageIndicatorPosition = PageIndicatorPosition(horizontal: .center, vertical: .under)
        
        let pageIndicator = UIPageControl()
        pageIndicator.currentPageIndicatorTintColor = hexStringToUIColor(hex: "35C2ED")
        pageIndicator.pageIndicatorTintColor = hexStringToUIColor(hex: "C5C5C5")
        slider.pageIndicator = pageIndicator
        
        slider.activityIndicator = DefaultActivityIndicator()
        updateSliderInputs()
        
//        let recognizer = UITapGestureRecognizer(target: self, action: #selector(self.didTapOnAd))
//        slider.addGestureRecognizer(recognizer)
        slider.backgroundColor = UIColor.white
        slider.bringSubviewToFront(bottomView)
    }
    
    @objc func didTapOnAd() {
        print("select image at index: \(slider.currentPage)")
        
        if imagesLinks.count > 0{
            // 2. create PhotoBrowser Instance, and present.
            let browser = SKPhotoBrowser(photos: imagesLinks)
            browser.initializePageIndex(slider.currentPage)
            parent.present(browser, animated: true, completion: {})
        }
        
        onClickAction?()
    }
    
    
    /// init image slider inputs from iamges array
    fileprivate func updateSliderInputs() {
        var inputs: [SDWebImageSource] = []
        
        for image in images {
            if let input = SDWebImageSource(urlString: image) {
                inputs.append(input)
            }
        }
        
        slider.setImageInputs(inputs)
    }
}
