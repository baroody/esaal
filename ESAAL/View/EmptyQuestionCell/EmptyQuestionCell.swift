//
//  EmptyQuestionCell.swift
//  ESAAL
//
//  Created by Mina Thabet on 11/6/19.
//  Copyright © 2019 HardTask. All rights reserved.
//

import UIKit
import IBAnimatable

protocol EmptyQuestionCellDelegate: NSObjectProtocol{
    func addQuestionClicked()
}

class EmptyQuestionCell: UITableViewCell {

    //MARK:- IBOutlet
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var bButton: UIButton!
    @IBOutlet weak var buttonContainerView: AnimatableView!
    
    //MARK:- Variables
    weak var deleget: EmptyQuestionCellDelegate?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCell(label: String, isTeacher: Bool){
        self.label.text = label
        buttonContainerView.isHidden = isTeacher
        setupFont()
    }
    
    
    @IBAction func addQuestionAction(_ sender: Any) {
        deleget?.addQuestionClicked()
    }

    /// setup font for controller
    func setupFont(){
        mainQueue {
            self.label.font = UIFont(name: localizedSitringFor(key: "AppRegularFont"), size: iphoneXFactor*45 )
            self.bButton.titleLabel?.font = UIFont(name: localizedSitringFor(key: "AppBoldFont"), size: iphoneXFactor*50)
        }
    }
    
}
