//
//  HeaderView.swift
//  ESAAL
//
//  Created by Mina Thabet on 11/4/19.
//  Copyright © 2019 HardTask. All rights reserved.
//

import UIKit

protocol HeaderViewDelegate: NSObjectProtocol {
    func filterActionClicked()
    func searchActionClicked()
}

class HeaderView: UIView {

    //MARK:- IBOutlet
    @IBOutlet var headerLabel: UILabel!
    @IBOutlet var backView: UIView!
    @IBOutlet var addQuestionView: UIView!
    @IBOutlet var filterView: UIView!
    @IBOutlet var searchView: UIView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    /// variables
    var parent: UIViewController!
    var title: String = ""
    var isFromRoot = false
    weak var deleget: HeaderViewDelegate?

    /// init view
    public static func initView(fromController controller: UIViewController, andView view: UIView, andTitle title: String = "", isFromRoot: Bool) -> HeaderView{
        
         let popup = Bundle.main.loadNibNamed("HeaderView", owner: controller, options: nil)?.last as! HeaderView
         popup.parent = controller
         popup.headerLabel.text = title
         popup.isFromRoot = isFromRoot
         popup.frame = CGRect(x: 0, y: 0, width: screenWidth, height: view.frame.size.height)
         
        popup.searchBar.setTextField(color: UIColor.white)
        popup.searchBar.tintColor = .white
         mainQueue {
             popup.headerLabel.font = UIFont(name:  localizedSitringFor(key: "AppBoldFont"), size: iphoneXFactor*50)
         }
         view.addSubview(popup)

         return popup

    }

    @IBAction func searchAction(_ sender: Any) {
        self.deleget?.searchActionClicked()
    }
    
    @IBAction func filterAction(_ sender: Any) {
        self.deleget?.filterActionClicked()
    }
    
    @IBAction func backAction(_ sender: Any) {
        parent.navigationController?.popViewController(animated: true)
        parent.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func addQuestionAction(_ sender: Any) {
        if Account.shared.id == 0 {
           // displayAlertConfirmation(localizedSitringFor(key: "Attention"), localizedSitringFor(key: "notLogin")) {
                // go to login if user not login
                pushToView(withId: "LoginViewController")
           // }
            return
        }
        if !Account.shared.isTeacher {
            if !Account.shared.isSubscribe {
                displayAlert("",localizedSitringFor(key: "userNotHavePackage"))
                return
            }
        }
        goNavigation(controllerId: "AddQuestionController", controller: parent)
    }
}
