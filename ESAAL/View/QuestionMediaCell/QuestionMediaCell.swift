//
//  QuestionMediaCell.swift
//  ESAAL
//
//  Created by Mina Thabet on 11/10/19.
//  Copyright © 2019 HardTask. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
import ReplayKit
import SDWebImage
import Kingfisher

protocol MediaQuestionMediaCellDeleget: NSObjectProtocol {
    func cancelMediaClicked(_ sender: QuestionMediaCell)
}

class QuestionMediaCell: UICollectionViewCell {

    //MARK:- Variables
    var isImage = true
    weak var delegte: MediaQuestionMediaCellDeleget?
    var vedioImage:UIImage?{
        didSet{
            mainQueue {
                self.mediaImage.image = self.vedioImage
            }
        }
    }
    
    //MARK:- IBOutlet
    @IBOutlet weak var mediaImage: UIImageView!
    @IBOutlet weak var playButtonImage: UIImageView!
    @IBOutlet weak var cancelMediaView: UIView!
    
    
    //MARK:-  Initialization
    override func awakeFromNib() {
        super.awakeFromNib()
        // playButtonImage.isHidden = isImage
        // Initialization code
    }
    
    @IBAction func cancelAction(_ sender: Any) {
        self.delegte?.cancelMediaClicked(self)
    }
    
    func configureCell(questionMedia: QuestionMedia){
        isImage = questionMedia.isImage
        playButtonImage.isHidden = isImage
        cancelMediaView.isHidden = false
        if isImage {
            if questionMedia.image == nil{
                if let url = URL(string: questionMedia.imageUrl?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? "") {
                    mediaImage.kf.indicatorType = .activity
                    mediaImage.kf.setImage(with: url,placeholder: UIImage(named: "icon_1024"))
                    //mediaImage.sd_setImage(with: url, placeholderImage: UIImage(named: "icon_1024"))
                }
            }else {
                mediaImage.image = questionMedia.image
            }
        }else {
            if questionMedia.video == nil{
                if let url = URL(string: questionMedia.videoImage?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? "") {
                    mediaImage.kf.indicatorType = .activity
                    mediaImage.kf.setImage(with: url)
                }
            }else {
                mediaImage.image = getThumbnailImageFromVideoURL(fromUrl: questionMedia.video! as URL)
            }
            
        }
    }
    
    func configureCell(media: Attachement) {
        isImage = (media.fileType == "i")
        playButtonImage.isHidden = isImage
        cancelMediaView.isHidden = true
        if let url = media.fileUrl?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) {
            if isImage {
                if let imageUrl = URL(string: url){
                    mediaImage.kf.indicatorType = .activity
                    mediaImage.kf.setImage(with: imageUrl,placeholder: UIImage(named: "icon_1024"))
                }
                //mediaImage.sd_setImage(with: URL(string: url), placeholderImage: UIImage(named: "icon_1024"))
            }else {
                if let videoUrl = URL(string: media.filePlaceholderUrl?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""){
                    mediaImage.kf.indicatorType = .activity
                    mediaImage.kf.setImage(with: videoUrl)
                }
            }
        }
        
    }
    
    ///get image from video url
       func getThumbnailImageFromVideoURL(fromUrl url: URL) -> UIImage? {
           let asset: AVAsset = AVAsset(url: url)
           let imageGenerator = AVAssetImageGenerator(asset: asset)
           do {
               let thumbnailImage = try imageGenerator.copyCGImage(at: CMTimeMake(value: 1, timescale: 60) , actualTime: nil)
               return UIImage(cgImage: thumbnailImage)
           } catch let error {
               print(error)
           }
           
           return nil
       }

}
