//
//  LoaderView.swift
//  ESAAL
//
//  Created by Mina Thabet on 2/10/19.
//  Copyright © 2019 HardTask. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class LoaderView: UIView {

    func showLoader()  {
        
        let color = UIColor.darkGray
        
        let size:CGFloat =  UIScreen.main.traitCollection.horizontalSizeClass == .regular ? 100.0 : 50.0
        
        let loader = NVActivityIndicatorView(frame: CGRect(x:center.x - (size / 2) ,y:center.y - (size / 2),width:size,height:size), type: .ballSpinFadeLoader, color: color, padding: nil) 

        self.addSubview(loader)
        
        loader.startAnimating()
        
    }
    

}
