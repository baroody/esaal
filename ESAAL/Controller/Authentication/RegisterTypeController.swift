//
//  RegisterTypeController.swift
//  ESAAL
//
//  Created by Mina Thabet on 10/29/19.
//  Copyright © 2019 HardTask. All rights reserved.
//

import UIKit

class RegisterTypeController: UIViewController {

    //MARK:- IBoutlet
    @IBOutlet var studentLabel: UILabel!
    @IBOutlet var teacherLAbel: UILabel!
    @IBOutlet weak var languageLabel: UILabel!
    
    
    //MARK:- App life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
    }

    @IBAction func backAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func changeLanguageAction(_ sender: Any) {
        changeAppLanguage(controller: self,resetControllerId: "LoginViewController")
    }
}


// MARK:- Helpers
extension RegisterTypeController{
    /// init view for first time
    func initView(){
        setupFont()
    }

    func setupFont() {
        mainQueue {
            self.studentLabel.font = UIFont(name:  localizedSitringFor(key: "AppRegularFont"), size: iphoneXFactor*50)
            self.teacherLAbel.font = UIFont(name:  localizedSitringFor(key: "AppRegularFont"), size: iphoneXFactor*50)
            self.languageLabel.font = UIFont(name: localizedSitringFor(key: "AppLangBoldFont"), size: iphoneXFactor*36)
            self.languageLabel.text = localizedSitringFor(key: "Lang")
        }
    }
}
