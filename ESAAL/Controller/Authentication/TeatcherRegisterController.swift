//
//  TeatcherRegisterController.swift
//  ESAAL
//
//  Created by Mina Thabet on 10/29/19.
//  Copyright © 2019 HardTask. All rights reserved.
//

import UIKit
import TagListView
import IBAnimatable
import Alamofire
import DropDown
import BEMCheckBox

class TeatcherRegisterController: UIViewController {
    
    //MARK:- IBOutlets
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var firstNAmeTextField: UITextField!
    @IBOutlet weak var midNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var countryTextField: UITextField!
    @IBOutlet weak var countryView: AnimatableView!
    @IBOutlet weak var banckNameTextField: UITextField!
    @IBOutlet weak var accountNumberTextField: UITextField!
    @IBOutlet weak var swiftCodeTextField: UITextField!
    @IBOutlet weak var IBANTextField: UITextField!
    @IBOutlet weak var homeAddressTextField: UITextField!
    @IBOutlet weak var banckAddressTextField: UITextField!
    @IBOutlet weak var banckNameView: AnimatableView!
    @IBOutlet weak var accountNumberView: AnimatableView!
    @IBOutlet weak var swiftCodeView: AnimatableView!
    @IBOutlet weak var bankAddressView: AnimatableView!
    @IBOutlet weak var homeView: AnimatableView!
    @IBOutlet weak var IBANView: AnimatableView!
    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var BattachMentLabel: UILabel!
    @IBOutlet weak var rLabel1: UILabel!
    @IBOutlet weak var rLabel2: UILabel!
    @IBOutlet weak var rLabel3: UILabel!
    @IBOutlet weak var frontIdImageView: AnimatableImageView!
    @IBOutlet weak var backIdImageView: AnimatableImageView!
    @IBOutlet weak var qualificationImageView: AnimatableImageView!
    @IBOutlet weak var BsLabel: UILabel!
    @IBOutlet weak var RButton: UIButton!
    @IBOutlet weak var tagList: TagListView!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var r_agrreTermsLabel: UILabel!
    @IBOutlet weak var b_termsConditionLabel: UILabel!
    @IBOutlet weak var agreeTermsCheckBox: BEMCheckBox!
    
    
    //MARK:- Constant
    let NETWORK = NetworkingHelper()
    let countryDropDown = DropDown()
    let GET_COUNTRY = "getCountry"
    let GET_MATRIAL = "getMatrial"
    let SIGNUP_TEACH = "signupTech"
    let ADD_TOKEN = "addToken"
    
    
    //MARK:- Variables
    var matrialList: [Matrial] = []
    var tags: [String] = []
    var selectedTag: [String] = []
    var countryList: [Country] = []
    var selectedCountry: Country?
    var isGulf = false
    var isSelectFrontIdImage = false
    var isSelectBackIdImage = false
    var isSelectQualificationImage = false
    var isSelectProfile = false
    var isAgreeTerms = false

    //MARK:- app life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        tagList.delegate = self
    }
        
    //MARK:- IBAction
    @IBAction func backAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func chooseCountryAction(_ sender: Any) {
        countryDropDown.show()
    }
    
    @IBAction func selectQualificationAction(_ sender: Any) {
        AttachmentHandler.shared.showAttachmentImageActionSheet(vc: self)
               
        AttachmentHandler.shared.imagePickedBlock = { (image) in
            self.qualificationImageView.image = image
            self.isSelectQualificationImage = true
        }
    }
    
    @IBAction func selectBackImageAction(_ sender: Any) {
        AttachmentHandler.shared.showAttachmentImageActionSheet(vc: self)
        
        AttachmentHandler.shared.imagePickedBlock = { (image) in
            self.backIdImageView.image = image
            self.isSelectBackIdImage = true
        }
    }
    
    @IBAction func selectFrontImageAction(_ sender: Any) {
        AttachmentHandler.shared.showAttachmentImageActionSheet(vc: self)
        
        AttachmentHandler.shared.imagePickedBlock = { (image) in
            self.frontIdImageView.image = image
            self.isSelectFrontIdImage = true
        }
    }
    
    @IBAction func chooseProfileImageAction(_ sender: Any) {
        AttachmentHandler.shared.showAttachmentImageActionSheet(vc: self)
        
        AttachmentHandler.shared.imagePickedBlock = { (image) in
            self.profileImageView.image = image
            self.isSelectProfile = true
        }
    }
    
    @IBAction func registerAction(_ sender: Any) {
        if ValidateField() {
            requestSignupTeacherApi()
        }
    }
    
    @IBAction func agreeTermsAction(_ sender: Any) {
        isAgreeTerms = !agreeTermsCheckBox.on
        agreeTermsCheckBox.setOn(isAgreeTerms, animated: true)
    }
    
    @IBAction func termsAndConditionAction(_ sender: Any) {
        let main = UIStoryboard(name: "Main", bundle: nil)
        let vc = main.instantiateViewController(withIdentifier: "AboutUsViewController") as! AboutUsViewController
        vc.htmlType = HtmlType.TeatcherTerms
        vc.headerTitle = localizedSitringFor(key: "terms")
        present(vc, animated: true)

    }
    
}

//MARK:- Helpers
extension TeatcherRegisterController{
    func initView(){
        NETWORK.deleget = self
        setTextViewPlaceholder()
        initTagsView()
        initDropDown()
        setupFont()
        agreeTermsCheckBox.boxType = .square
        requestCountryApi()
        requestMatrialApi()
        
    }
    
    /// init fonts depend on language
    func setupFont() {
        mainQueue {
            self.firstNAmeTextField.font = UIFont(name: localizedSitringFor(key: "AppRegularFont"), size: iphoneXFactor*45 )
            self.midNameTextField.font = UIFont(name: localizedSitringFor(key: "AppRegularFont"), size: iphoneXFactor*45 )
            self.lastNameTextField.font = UIFont(name: localizedSitringFor(key: "AppRegularFont"), size: iphoneXFactor*45 )
            self.emailTextField.font = UIFont(name: localizedSitringFor(key: "AppRegularFont"), size: iphoneXFactor*45 )
            self.countryTextField.font = UIFont(name: localizedSitringFor(key: "AppRegularFont"), size: iphoneXFactor*45 )
            self.banckNameTextField.font = UIFont(name: localizedSitringFor(key: "AppRegularFont"), size: iphoneXFactor*45 )
            self.userNameTextField.font = UIFont(name: localizedSitringFor(key: "AppRegularFont"), size: iphoneXFactor*45 )
            self.accountNumberTextField.font = UIFont(name: localizedSitringFor(key: "AppRegularFont"), size: iphoneXFactor*45 )
            self.swiftCodeTextField.font = UIFont(name: localizedSitringFor(key: "AppRegularFont"), size: iphoneXFactor*45 )
            self.homeAddressTextField.font = UIFont(name: localizedSitringFor(key: "AppRegularFont"), size: iphoneXFactor*45 )
            self.banckAddressTextField.font = UIFont(name: localizedSitringFor(key: "AppRegularFont"), size: iphoneXFactor*45 )
            self.banckNameTextField.font = UIFont(name: localizedSitringFor(key: "AppRegularFont"), size: iphoneXFactor*45 )
            self.IBANTextField.font = UIFont(name: localizedSitringFor(key: "AppRegularFont"), size: iphoneXFactor*45 )
            self.descriptionTextView.font = UIFont(name: localizedSitringFor(key: "AppRegularFont"), size: iphoneXFactor*45 )
            self.rLabel1.font = UIFont(name: localizedSitringFor(key: "AppRegularFont"), size: iphoneXFactor*45 )
            self.rLabel1.font = UIFont(name: localizedSitringFor(key: "AppRegularFont"), size: iphoneXFactor*45 )
            self.rLabel1.font = UIFont(name: localizedSitringFor(key: "AppRegularFont"), size: iphoneXFactor*45 )
            self.titleLabel.font = UIFont(name: localizedSitringFor(key: "AppRegularFont"), size: iphoneXFactor*45 )

            self.BsLabel.font = UIFont(name: localizedSitringFor(key: "AppBoldFont"), size: iphoneXFactor*45 )
            self.BattachMentLabel.font = UIFont(name: localizedSitringFor(key: "AppBoldFont"), size: iphoneXFactor*45 )
            self.RButton.titleLabel?.font = UIFont(name: localizedSitringFor(key: "AppBoldFont"), size: iphoneXFactor*50)
            
            self.r_agrreTermsLabel.font = UIFont(name: localizedSitringFor(key: "AppRegularFont"), size: iphoneXFactor*40)
            self.b_termsConditionLabel.font = UIFont(name: localizedSitringFor(key: "AppBoldFont"), size: iphoneXFactor*45)
            self.r_agrreTermsLabel.text = localizedSitringFor(key: "agree")
            self.b_termsConditionLabel.text = localizedSitringFor(key: "terms")
        }
        
    }
    
    func setTextViewPlaceholder() {
        descriptionTextView.delegate = self
        descriptionTextView.text = localizedSitringFor(key: "Description")
        descriptionTextView.textColor = UIColor.lightGray
    }
    
    /// init tags view
    func initTagsView(){
        mainQueue {
            self.tagList.textFont = isIpad ? UIFont.systemFont(ofSize: 30) : UIFont.systemFont(ofSize: 15)
            self.tagList.addTags(self.tags)
        }
    }
    
    /// init drop downs
    func initDropDown() {
        if UIScreen.main.traitCollection.horizontalSizeClass == .regular
        {
            DropDown.appearance().cellHeight = 60
            DropDown.appearance().textFont = UIFont.systemFont(ofSize: 34)
        }
        else
        {
            DropDown.appearance().cellHeight = 40
            DropDown.appearance().textFont = UIFont.systemFont(ofSize: 17)
        }
        DropDown.appearance().semanticContentAttribute = .forceLeftToRight
        
        initCountryDropDown()
       
    }
    
    /// init country drop down
    func initCountryDropDown() {
       countryDropDown.anchorView = countryView
       countryDropDown.dataSource = [localizedSitringFor(key: "loading")]
       countryDropDown.direction = .any
       countryDropDown.selectionAction = {[unowned self] (index:Int,item:String) in
       if self.countryList.count == 0 {
           return
       }
       
       self.selectedCountry = self.countryList[index]
       self.isGulf = self.countryList[index].isGulfCountry ?? false
       self.countryTextField.text = self.countryList[index].name
       self.countrySelectedAction()
      }
  }
    
    // to show and hide some detail depend on gulf
    func countrySelectedAction(){
        print("isgulf ", isGulf)
        if isGulf {
            mainQueue {
                self.banckNameView.isHidden = false
                self.accountNumberView.isHidden = true
                self.swiftCodeView.isHidden = false
                self.bankAddressView.isHidden = true
                self.homeView.isHidden = true
                self.IBANView.isHidden = false
            }
        }else {
            mainQueue {
                self.banckNameView.isHidden = false
                self.accountNumberView.isHidden = false
                self.swiftCodeView.isHidden = false
                self.bankAddressView.isHidden = false
                self.homeView.isHidden = false
                self.IBANView.isHidden = true
            }
        }
    }
    
    
    /// get matrials ids form selected tags
    func getMatrialsIds() -> String {
        var ids = ""
        for (index,title) in selectedTag.enumerated() {
            if index == selectedTag.count - 1 {
                ids += String(matrialList.filter { $0.name == title}.first?.id ?? 0)
            }else {
                ids += String(matrialList.filter { $0.name == title}.first?.id ?? 0) + ","
            }
            
        }
        return ids
    }
    
    /// validate Fields
    ///
    /// - Returns: true or false
    func ValidateField() -> Bool{
        if firstNAmeTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty == true {
           displayMessage(message: localizedSitringFor(key: "emptyFirstName"), messageError: true)
           return false
       }

       if midNameTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty == true {
           displayMessage(message: localizedSitringFor(key: "emptyMidName"), messageError: true)
           return false
       }

       if lastNameTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty == true {
           displayMessage(message: localizedSitringFor(key: "emptyLastName"), messageError: true)
           return false
       }

     if phoneTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty == true || phoneTextField.text?.count ?? 0 < 8{
           displayMessage(message: localizedSitringFor(key: "emptyPhone"), messageError: true)
           return false
       }


        
        if countryTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty == true {
            displayMessage(message: localizedSitringFor(key: "emptyCountry"), messageError: true)
            return false
        }
        
        if isGulf {
           
           if IBANTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty == true {
               displayMessage(message: localizedSitringFor(key: "emptyIban"), messageError: true)
               return false
           }
        }else {
            if accountNumberTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty == true {
                displayMessage(message: localizedSitringFor(key: "emptyAccountNumber"), messageError: true)
                return false
            }
            if homeAddressTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty == true {
                displayMessage(message: localizedSitringFor(key: "emptyhomeAddrees"), messageError: true)
                return false
            }
            if banckAddressTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty == true {
                displayMessage(message: localizedSitringFor(key: "emptybankAddrees"), messageError: true)
                return false
            }
        }
        
        if banckNameTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty == true {
           displayMessage(message: localizedSitringFor(key: "emptyBankName"), messageError: true)
           return false
        }
        
       if swiftCodeTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty == true {
          displayMessage(message: localizedSitringFor(key: "emptyswiftCode"), messageError: true)
          return false
       }
        
         if !isValidEmail(emailTextField.text ?? ""){
            displayMessage(message: localizedSitringFor(key: "emptyEmail"), messageError: true)
            return false
        }
        
     if userNameTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty == true {
        displayMessage(message: localizedSitringFor(key: "emptyUserName"), messageError: true)
        return false
     }
        
        if passwordTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty == true ||  passwordTextField.text?.count ?? 0 < 6 {
            displayMessage(message: localizedSitringFor(key: "emptyPassword"), messageError: true)
            return false
        }

        if descriptionTextView.text?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty == true {
            displayMessage(message: localizedSitringFor(key: "emptyDescription"), messageError: true)
            return false
        }
        
        if !isSelectFrontIdImage{
            displayMessage(message: localizedSitringFor(key: "emptySelectFrontId"), messageError: true)
            return false
        }
        
        if !isSelectBackIdImage{
            displayMessage(message: localizedSitringFor(key: "emptySelectBackId"), messageError: true)
            return false
        }
        
        if !isSelectQualificationImage{
            displayMessage(message: localizedSitringFor(key: "emptySelectQualification"), messageError: true)
            return false
        }
        
        
        if selectedTag.count < 1 {
            displayMessage(message: localizedSitringFor(key: "emptyMatrial"), messageError: true)
            return false
        }
        
        if !isAgreeTerms {
            displayMessage(message: localizedSitringFor(key: "agreeTerms"), messageError: true)
            return false
        }
        
        return true
    }}


//MARK:- tag list view delegate
extension TeatcherRegisterController: TagListViewDelegate {
    func tagPressed(_ title: String, tagView: TagView, sender: TagListView) {
             print("Tag pressed: \(title), \(sender)")

        var isFound = false
        selectedTag.filter({ $0 == title
        }).forEach({_ in
            isFound = true
            selectedTag = selectedTag.filter { $0 != title}
            tagView.isSelected = !tagView.isSelected

        })
        
        if !isFound{
            selectedTag.append(title)
            tagView.isSelected = !tagView.isSelected
        }

    }
        
}


//MARK:- UI Text View Delegate
extension TeatcherRegisterController: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = localizedSitringFor(key: "Description")
            textView.textColor = UIColor.lightGray
        }
    }
}


//MARK:- Networking
extension TeatcherRegisterController: NetworkingHelperDeleget {
    func onHelper(getData data: DataResponse<String>, fromApiName name: String, withIdentifier id: String) {
        if id == GET_COUNTRY { handleGetCountryResponse(response: data) }
        else if id == GET_MATRIAL { handleMatrialResponse(response: data) }
        else if id == SIGNUP_TEACH { handleSignupTeacherResponse(response: data) }
        else if id == ADD_TOKEN { handleToken(forResponse: data) }
        
    }
    
    func onHelper(getError error: String, fromApiName name: String, withIdentifier id: String) {
        print("e7na hena")
        displayMessage(message: localizedSitringFor(key: "unkwonError") , messageError: true)
    }
    
    // MARK:- request apis from server
    
    func requestCountryApi() {
        NETWORK.connectWithHeaderTo(api: ApiNames.COUNTRIES, andIdentifier: GET_COUNTRY, withLoader: true,forController: self, methodType: .get)
    }
    
    func requestMatrialApi() {
        NETWORK.connectWithHeaderTo(api: ApiNames.MATERIALS,  andIdentifier: GET_MATRIAL, forController: self, methodType: .get)
    }
    
    func requestSignupTeacherApi() {
        var paramters: [String: String] = [:]
        paramters["FirstName"] = firstNAmeTextField.text ?? ""
        paramters["MiddleName"] = midNameTextField.text ?? ""
        paramters["LastName"] = lastNameTextField.text ?? ""
        paramters["Mobile"] = phoneTextField.text ?? ""
        paramters["Email"] = emailTextField.text ?? ""
        paramters["UserName"] = userNameTextField.text ?? ""
        paramters["Password"] = passwordTextField.text ?? ""
        paramters["MaterialIds"] = getMatrialsIds()
        paramters["CountryId"] = String(selectedCountry?.id ?? 0)
        paramters["IBAN"] = IBANTextField.text ?? ""
        paramters["SwiftCode"] = swiftCodeTextField.text ?? ""
        paramters["AccountNumber"] = accountNumberTextField.text ?? ""
        paramters["BankName"] = banckNameTextField.text ?? ""
        paramters["BankAddress"] = banckAddressTextField.text ?? ""
        paramters["PersonalAddress"] = homeAddressTextField.text ?? ""
        paramters["Description"] = descriptionTextView.text
        
        print(paramters)
        var imagesss: [String:UIImage] = [:]
        if isSelectProfile {
            imagesss["userimage"] = profileImageView.image!
            
        }
        imagesss["useridcardfront"] = frontIdImageView.image!
        imagesss["useridcardback"] = backIdImageView.image!
        imagesss["usercertification"] = qualificationImageView.image!

        NETWORK.connectToUploadObject(videos: [:], toApi: ApiNames.SIGN_UP_TEACHER, withParameters: paramters, andIdentifier: SIGNUP_TEACH, images: imagesss)
    }
    
    func getTokenRequest(userId: Int){
        NETWORK.connectWithHeaderTo(api: ApiNames.TOKEN + "?userid=\(userId)&DeviceType=1&token=\(UserDefaults.standard.string(forKey: "deviceToken") ?? "")&DeviceId=\(deviceID)",andIdentifier: ADD_TOKEN ,withLoader: false, methodType: .post)
    }
    
 
    
    // MARK:- handle response server
    func handleGetCountryResponse(response: DataResponse<String>){
        print(response,"mina")
        switch response.response?.statusCode {
        case 200:
            do{
              let country = try JSONDecoder().decode([Country].self, from: response.data ?? Data())
              countryList = country
              countryDropDown.dataSource = country.compactMap({$0.name})
              countryDropDown.reloadAllComponents()
          }catch let error{
              print(error,"mina")
              displayMessage(message: localizedSitringFor(key: "unknownError"), messageError: true)
          }
        case 202:
            displayMessage(message: localizedSitringFor(key: "EmptyCountries"), messageError: true)
        case 400:
            displayMessage(message: localizedSitringFor(key: "badRequst"), messageError: true)
        case 401:
            displayMessage(message: localizedSitringFor(key: "Unauthorized"), messageError: true)
        default:
            displayMessage(message: localizedSitringFor(key: "unknownError"), messageError: true)
        }
    }
    
    func handleMatrialResponse(response: DataResponse<String>) {
        
        switch response.response?.statusCode {
        case 200:
            do{
                let matrial = try JSONDecoder().decode([Matrial].self, from: response.data ?? Data())
                matrialList = matrial
                tags = matrial.compactMap({$0.name})
                self.tagList.addTags(self.tags)
          }catch{
              displayMessage(message: localizedSitringFor(key: "unknownError"), messageError: true)
          }
        case 202:
            displayMessage(message: localizedSitringFor(key: "MisMaterial"), messageError: true)
        case 400:
            displayMessage(message: localizedSitringFor(key: "badRequst"), messageError: true)
        case 401:
            displayMessage(message: localizedSitringFor(key: "Unauthorized"), messageError: true)
        default:
            displayMessage(message: localizedSitringFor(key: "unknownError"), messageError: true)
        }
    }
    
    func handleSignupTeacherResponse(response: DataResponse<String>){
        switch response.response?.statusCode {
        case 200:
            do{
                let appUser = try JSONDecoder().decode(AppUser.self, from: response.data ?? Data())
                if !(appUser.user?.isActive ?? false) {
                    displayMessageWithTime(message: localizedSitringFor(key: "notActive"), messageError: true, time: 5)
                    getTokenRequest(userId: appUser.user?.id ?? 0)
                    goToView(withId: "LoginViewController")
                    return
                }
                
                Account.shared.Token = appUser.token ?? ""
                Account.shared.id = appUser.user?.id ?? 0
                Account.shared.isTeacher = appUser.user?.isTeacher ?? false
                Account.shared.userName = appUser.user?.userName ?? ""
                Account.shared.isRequestes = appUser.user?.isRequest ?? false
                Account.shared.requestDate = appUser.user?.requestDate ?? ""
                Account.shared.storeData()
                getTokenRequest(userId: appUser.user?.id ?? 0)
                print(Account.shared.Token,"osamsa")
                goToView(withId: "TabBarViewController")
          }catch{
              displayMessage(message: localizedSitringFor(key: "unknownError"), messageError: true)
          }
        case 201:
            displayMessage(message: localizedSitringFor(key: "ExistMail"), messageError: true)
        case 202:
            displayMessage(message: localizedSitringFor(key: "ExistUserName"), messageError: true)
        case 203:
            displayMessage(message: localizedSitringFor(key: "ExistPhone"), messageError: true)
         case 204:
         displayMessage(message: localizedSitringFor(key: "NoUserFound"), messageError: true)
        case 400:
         displayMessage(message: localizedSitringFor(key: "badRequst"), messageError: true)
        case 401:
            displayMessage(message: localizedSitringFor(key: "Unauthorized"), messageError: true)
        default:
            displayMessage(message: localizedSitringFor(key: "unknownError"), messageError: true)
        }
    }
    
    func handleToken(forResponse response: DataResponse<String>){
         print("token_added")
         print(response.description)
     }
    
    
}
