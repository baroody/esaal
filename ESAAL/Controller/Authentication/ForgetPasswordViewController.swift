//
//  ForgetPasswordViewController.swift
//  ESAAL
//
//  Created by Mina Thabet on 11/1/19.
//  Copyright © 2019 HardTask. All rights reserved.
//

import UIKit
import Alamofire

class ForgetPasswordViewController: UIViewController {

    // MARK:- IBOutlet
    @IBOutlet var emailTextField: UITextField!
    @IBOutlet var sendButton: UIButton!
    
    //MARK:- Constant
    let NETWORK = NetworkingHelper()
    let LOGOUT = "logout"

    
    //MARK:- App life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
    }

    // MARK:- IBAction
    @IBAction func sendAction(_ sender: Any) {
        if ValidateField(){
            requestForgetPasswordApi()
        }
    }
    
    @IBAction func backAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}


//MARK:- Helpers
extension ForgetPasswordViewController{
    /// init view for first time
    func initView(){
        NETWORK.deleget = self
        setupFont()
    }
    
    /// setup font for controller
    func setupFont(){
        mainQueue {
            self.emailTextField.font = UIFont(name: localizedSitringFor(key: "AppRegularFont"), size: iphoneXFactor*45 )
            self.sendButton.titleLabel?.font = UIFont(name: localizedSitringFor(key: "AppBoldFont"), size: iphoneXFactor*50)
        }
    }
    
    
    /// validate Fields
    ///
    /// - Returns: true or false
    func ValidateField() -> Bool{
        
        if !isValidEmail(emailTextField.text ?? "") {
            displayMessage(message: localizedSitringFor(key: "emptyEmail"), messageError: true)
            return false
        }
        return true
    }

    

}

//MARK:- Networking
extension ForgetPasswordViewController: NetworkingHelperDeleget {
    func onHelper(getData data: DataResponse<String>, fromApiName name: String, withIdentifier id: String) {
        if id == LOGOUT{ handleLogout(forResponse: data) }
        else{
            handleForgetPasswordResponse(forResponse: data)
        }
    }
    
    func onHelper(getError error: String, fromApiName name: String, withIdentifier id: String) {
        displayMessage(message: localizedSitringFor(key: "unknownError"), messageError: true)
    }
    
    // MARK:- request apis from server
        /// uses for connecting to server and recive data
        func requestForgetPasswordApi()
        {
            var parameters: [String: Any] = [:]
            parameters["email"] = emailTextField.text ?? ""
            
            NETWORK.connectWithHeaderTo(api: ApiNames.FORGET_PASSWORD+"?email=\(emailTextField.text ?? "")", withParameters: parameters, withLoader: true ,forController: self, methodType: .post)
        }
        
        
    // MARK:- handle response server
        /// handle login json response from server
        ///
        /// - Parameter response: server response
        func handleForgetPasswordResponse(forResponse response: DataResponse<String>)
        {
            switch response.response?.statusCode {
            case 200:
                dismiss(animated: true) {
                    displayMessage(message: localizedSitringFor(key: "MessageSentSuccessfully"), messageError: false)
                }
            case 201:
                displayMessage(message: localizedSitringFor(key: "emptyMailResponse"), messageError: true)
            case 203:
                displayMessage(message: localizedSitringFor(key: "EmailNotFound"), messageError: true)
            case 400:
                displayMessage(message: localizedSitringFor(key: "badRequst"), messageError: true)
            case 401:
                displayMessage(message: localizedSitringFor(key: "Unauthorized"), messageError: true)
            case 207:
                displayMessage(message: localizedSitringFor(key: "UserISNotActive"), messageError: true)
                delay(timeSession) {
                    self.logoutRequest()
                }
            default:
                displayMessage(message: localizedSitringFor(key: "unknownError"), messageError: true)
            }
        }
    
    /// uses for connecting to server and recive data
    func logoutRequest()
    {
        var parameters: [String: Any] = [:]
        parameters["userid"] = Account.shared.id
        parameters["DeviceId"] = deviceID
        
        var url = ApiNames.LOGOUT + "?userid=" + String(Account.shared.id)
        url += "&DeviceId=" + deviceID
        
        NETWORK.connectWithHeaderTo(api: url, withParameters: parameters, andIdentifier: LOGOUT, withLoader: true, forController: self, methodType: .post)
    }
    
    
    /// handle login json response from server
    ///
    /// - Parameter response: server response
    func handleLogout(forResponse response: DataResponse<String>)
    {
        switch response.response?.statusCode {
        case 200:
            Account.shared.logout()
            pushToView(withId: baseSession)
        case 400:
            displayMessage(message: localizedSitringFor(key: "invalidUserOrPassword"), messageError: true)

        case 401:
            displayMessage(message: localizedSitringFor(key: "Unauthorized"), messageError: true)
        default:
            displayMessage(message: localizedSitringFor(key: "unknownError"), messageError: true)
        }
    }
}
