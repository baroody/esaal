//
//  LoginViewController.swift
//  ESAAL
//
//  Created by Mina Thabet on 10/28/19.
//  Copyright © 2019 HardTask. All rights reserved.
//

import UIKit
import Alamofire


class LoginViewController: UIViewController {
    
    //MARK:- IBoutlet
    @IBOutlet var userNameTextField: UITextField!
    @IBOutlet var passwordTextField: UITextField!
    @IBOutlet var forgetButton: UIButton!
    @IBOutlet var loginButton: UIButton!
    @IBOutlet var registerButton: UIButton!
    @IBOutlet weak var languageLabel: UILabel!
    @IBOutlet weak var continueGuestButton: UIButton!
    
    
    ///CONSTANTS
    let NETWORK = NetworkingHelper()
    let LOGIN = "login"
    let ADD_TOKEN = "addToken"

    
    //MARK:- app life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        initview()
    }

    //MARK:- IBAction
    @IBAction func loginAction() {
        if ValidateField(){
            loginRequest()
        }
    }
    
    @IBAction func continueGuestAction(_ sender: Any) {
        pushToView(withId: "TabBarViewController")
    }
    
    @IBAction func changeLanguageAction(_ sender: Any) {
        changeAppLanguage(controller: self,resetControllerId: "LoginViewController")
    }
    
}

//MARK:- helpers
extension LoginViewController{
    
    /// init view for first time
    func initview(){
        hideKeyboardWhenTappedAround()
        NETWORK.deleget = self
        setupFont()
        
    }
    
    func setupFont() {
        mainQueue {
            self.passwordTextField.font = UIFont(name: localizedSitringFor(key: "AppRegularFont"), size: iphoneXFactor*50 )
            self.userNameTextField.font = UIFont(name: localizedSitringFor(key: "AppRegularFont"), size: iphoneXFactor*50 )
            self.forgetButton.titleLabel?.font = UIFont(name: localizedSitringFor(key: "AppBoldFont"), size: iphoneXFactor*50)
            self.loginButton.titleLabel?.font = UIFont(name: localizedSitringFor(key: "AppBoldFont"), size: iphoneXFactor*50)
            self.registerButton.titleLabel?.font = UIFont(name: localizedSitringFor(key: "AppBoldFont"), size: iphoneXFactor*50)
            self.languageLabel.font = UIFont(name: localizedSitringFor(key: "AppLangBoldFont"), size: iphoneXFactor*36)
            self.languageLabel.text = localizedSitringFor(key: "Lang")
        }
    }
    
    /// validate Fields
    ///
    /// - Returns: true or false
    func ValidateField() -> Bool{
        
        if userNameTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty == true {
            displayMessage(message: localizedSitringFor(key: "emptyUserName"), messageError: true)
            return false
        }
        
        if passwordTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty == true {
            displayMessage(message: localizedSitringFor(key: "emptyPassword"), messageError: true)
            return false
        }
        return true
    }
    
    func goToPackage() {
        let main = UIStoryboard(name: "Main", bundle: nil)
        let vc = main.instantiateViewController(withIdentifier: "StudentRegisterPackageController") as! StudentRegisterPackageController
        vc.isFromRegister = true
        self.present(vc, animated: true, completion: nil)
    }
}


//MARK:- Networking
extension LoginViewController: NetworkingHelperDeleget{
    func onHelper(getData data: DataResponse<String>, fromApiName name: String, withIdentifier id: String) {
        if id == LOGIN { handleLogin(forResponse: data) }
        else if id == ADD_TOKEN { handleToken(forResponse: data) }
    }
    
    func onHelper(getError error: String, fromApiName name: String, withIdentifier id: String) {
        displayMessage(message: localizedSitringFor(key: "unknownError"), messageError: true)
    }
    
    
        /// uses for connecting to server and recive data
        func loginRequest()
        {
            var parameters: [String: Any] = [:]
            parameters["username"] = userNameTextField.text ?? ""
            parameters["password"] = passwordTextField.text ?? ""
            
            NETWORK.connectWithHeaderTo(api: ApiNames.LOGIN, withParameters: parameters, andIdentifier: LOGIN, withLoader: true, forController: self, methodType: .post)
        }
    
    
        //MARK: - add device token
        func getTokenRequest(){
            NETWORK.connectWithHeaderTo(api: ApiNames.TOKEN + "?userid=\(Account.shared.id)&DeviceType=1&token=\(UserDefaults.standard.string(forKey: "deviceToken") ?? "")&DeviceId=\(deviceID)",andIdentifier: ADD_TOKEN ,withLoader: false, methodType: .post)
        }
    
        //MARK: - response device token
        func handleToken(forResponse response: DataResponse<String>){
            print("token_added")
            print(response.description)
        }
        
        
        /// handle login json response from server
        ///
        /// - Parameter response: server response
        func handleLogin(forResponse response: DataResponse<String>)
        {
            switch response.response?.statusCode {
            case 200:
                do{
                    let appUser = try JSONDecoder().decode(AppUser.self, from: response.data ?? Data())
                    if !(appUser.user?.isActive ?? false) {
                        displayMessageWithTime(message: localizedSitringFor(key: "notActive"), messageError: true, time: 5)
                        return
                    }
                    Account.shared.Token = appUser.token ?? ""
                    Account.shared.id = appUser.user?.id ?? 0
                    Account.shared.isTeacher = appUser.user?.isTeacher ?? false
                    Account.shared.userName = appUser.user?.userName ?? ""
                    Account.shared.isSubscribe = appUser.issubscribe ?? false
                    Account.shared.isRequestes = appUser.user?.isRequest ?? false
                    Account.shared.requestDate = appUser.user?.requestDate ?? ""
                    Account.shared.storeData()
                    getTokenRequest()
                    if !Account.shared.isTeacher && !Account.shared.isSubscribe {
                        goToPackage()
                    }else {
                        pushToView(withId: "TabBarViewController")
                    }
              }catch let error{
                print("error", error)
                  displayMessage(message: localizedSitringFor(key: "unknownError"), messageError: true)
              }
            case 400:
                displayMessage(message: localizedSitringFor(key: "invalidUserOrPassword"), messageError: true)

            case 401:
                displayMessage(message: localizedSitringFor(key: "Unauthorized"), messageError: true)
            default:
                displayMessage(message: localizedSitringFor(key: "unknownError"), messageError: true)
            }
        }

}
