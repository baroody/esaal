//
//  StudentRegisterPackageController.swift
//  ESAAL
//
//  Created by Mina Thabet on 10/31/19.
//  Copyright © 2019 HardTask. All rights reserved.
//

import UIKit
import Alamofire

class StudentRegisterPackageController: UIViewController {
    
    // MARK:- IBOutlet
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var backView: UIView!
    
    
    ///CONSTANTS
    let NETWORK = NetworkingHelper()
    let GET_SUBSCRIBE = "GET_SUBSCRIBE"
    let MAKE_SUBSCRIPTION = "MAKE_SUBSCRIPTION"
    let LOGOUT = "logout"

    /// Variable
    var isFromRegister = false
    var subscriptions: [Subscribtion] = []

    
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        hideLoaderForController(self)
    }
    
    
    //MARK:- IBAction
    @IBAction func backAction(_ sender: Any) {
        if isFromRegister{
            pushToView(withId: "TabBarViewController")
        }else {
            self.navigationController?.popViewController(animated: true)
        }
    }
}


// MARK:- Helpers
extension StudentRegisterPackageController{
    
    ///init view for first time
    func initView() {
        setupTableView()
        NETWORK.deleget = self
        GetSubscriptionRequest()
    }
    
    
    /// init table view
    func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: CellIdentifier.ChoosePackageCell.rawValue, bundle: nil), forCellReuseIdentifier: CellIdentifier.ChoosePackageCell.rawValue)
        tableView.estimatedRowHeight = 100
        tableView.rowHeight = UITableView.automaticDimension
    }
    

}



// MARK:- table view delegate and data source
extension StudentRegisterPackageController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return subscriptions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier.ChoosePackageCell.rawValue, for: indexPath) as! ChoosePackageCell
        cell.initView(wittSubscription: subscriptions[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.view.frame.size.height
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        MakeSubscribtionRequest(withId: subscriptions[indexPath.row].id ?? 0)
    }
    
}



// MARK: - NetworkingHelperDeleget
extension StudentRegisterPackageController: NetworkingHelperDeleget{
    func onHelper(getData data: DataResponse<String>, fromApiName name: String, withIdentifier id: String) {
        if id == GET_SUBSCRIBE{ handleGetSubscriptions(forResponse: data) }
        else if id == MAKE_SUBSCRIPTION { handleSubscribtionRequest(forResponse: data) }
        else if id == LOGOUT{ handleLogout(forResponse: data) }

    }
    
    func onHelper(getError error: String, fromApiName name: String, withIdentifier id: String) {
    }
    
    /// uses for connecting to server and recive data
    func GetSubscriptionRequest(){
        NETWORK.connectWithHeaderTo(api: ApiNames.GET_SUBSCRIBTIONS, andIdentifier : GET_SUBSCRIBE, withLoader: true, forController: self, methodType: .get)
    }
    
    /// handle  response from server
    ///
    /// - Parameter response: server response
    func handleGetSubscriptions(forResponse response: DataResponse<String>){
        switch response.response?.statusCode {
        case 200:
              do{
                  let sett = try JSONDecoder().decode([Subscribtion].self, from: response.data ?? Data())
                self.subscriptions = sett
                mainQueue {
                    self.tableView.reloadData()
                }

              }catch{
                displayMessage(message: localizedSitringFor(key: "unknownError"), messageError: true)
            }

            
        case 400:
            displayMessage(message: localizedSitringFor(key: "badRequst"), messageError: true)
        case 401:
            displayMessage(message: localizedSitringFor(key: "Unauthorized"), messageError: true)
        default:
            displayMessage(message: localizedSitringFor(key: "unknownError"), messageError: true)
        }
    }
    
    /// uses for connecting to server and recive data
    func MakeSubscribtionRequest(withId id: Int){
        var parameters : [String:Any] = [:]
        parameters["userid"] = Account.shared.id
        parameters["subscriptionid"] = id
        
        print(parameters)

        NETWORK.connectWithHeaderTo(api: ApiNames.MAKE_SUBSCRIBTIONS+"?userid=\(Account.shared.id)&subscriptionid=\(id)",withParameters: parameters, andIdentifier : MAKE_SUBSCRIPTION, withLoader: true, forController: self, methodType: .post)
    }
    
    
    /// handle info response from server
    ///
    /// - Parameter response: server response
    func handleSubscribtionRequest(forResponse response: DataResponse<String>){
        switch response.response?.statusCode {
        case 200:
                  do{
                      let paymentResponse = try JSONDecoder().decode(PaymentResponse.self, from: response.data ?? Data())
                    
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "PaymentViewController") as! PaymentViewController
                    vc.urlString = paymentResponse.returnUrl ?? ""
                    
                    self.present(vc, animated: true, completion: nil)

                  }catch{
                    displayMessage(message: localizedSitringFor(key: "unknownError"), messageError: true)
                }

        case 204:
            displayMessage(message: localizedSitringFor(key: "SubscriptionNyoFound"), messageError: true)
        case 203:
            displayMessage(message: localizedSitringFor(key: "NoUserFound"), messageError: true)
            
        case 400:
            displayMessage(message: localizedSitringFor(key: "badRequst"), messageError: true)
        case 401:
                displayMessage(message: localizedSitringFor(key: "Unauthorized"), messageError: true)
        case 207:
            displayMessage(message: localizedSitringFor(key: "UserISNotActive"), messageError: true)
            delay(timeSession) {
                self.logoutRequest()
            }
        default:
            displayMessage(message: localizedSitringFor(key: "unknownzzzzzzError"), messageError: true)
        }
        
    }

    /// uses for connecting to server and recive data
    func logoutRequest()
    {
        var parameters: [String: Any] = [:]
        parameters["userid"] = Account.shared.id
        parameters["DeviceId"] = deviceID
        
        var url = ApiNames.LOGOUT + "?userid=" + String(Account.shared.id)
        url += "&DeviceId=" + deviceID
        
        NETWORK.connectWithHeaderTo(api: url, withParameters: parameters, andIdentifier: LOGOUT, withLoader: true, forController: self, methodType: .post)
    }
    
    
    /// handle login json response from server
    ///
    /// - Parameter response: server response
    func handleLogout(forResponse response: DataResponse<String>)
    {
        switch response.response?.statusCode {
        case 200:
            Account.shared.logout()
            pushToView(withId: baseSession)
        case 400:
            displayMessage(message: localizedSitringFor(key: "invalidUserOrPassword"), messageError: true)

        case 401:
            displayMessage(message: localizedSitringFor(key: "Unauthorized"), messageError: true)
        default:
            displayMessage(message: localizedSitringFor(key: "unknownError"), messageError: true)
        }
    }
}

