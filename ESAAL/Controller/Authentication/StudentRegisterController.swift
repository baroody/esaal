//
//  StudentRegisterController.swift
//  ESAAL
//
//  Created by Mina Thabet on 10/29/19.
//  Copyright © 2019 HardTask. All rights reserved.
//

import UIKit
import Alamofire
import BEMCheckBox

class StudentRegisterController: UIViewController {

    //MARK:- IBOutlets
    @IBOutlet weak var firstNAmeTextField: UITextField!
    @IBOutlet weak var midNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet var registerButton: UIButton!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var r_agrreTermsLabel: UILabel!
    @IBOutlet weak var b_termsConditionLabel: UILabel!
    @IBOutlet weak var agreeTermsCheckBox: BEMCheckBox!
    
    /// Variables
    var isAgreeTerms = false
    
    /// Constant
    let NETWORK = NetworkingHelper()
    let STUDENT_REGISTER = "studentRegister"
    let ADD_TOKEN = "addToken"
    let DUMMY_PACKAGE = "bummyPackage"

    
    // MARK:- App Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        initView()
    }
    
    //MARK:- IBActions
    @IBAction func backAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func registerAction(_ sender: Any) {
        if ValidateField(){
            requestSignupStudentApi()
        }
    }
    
    @IBAction func agreeTermsAction(_ sender: Any) {
        isAgreeTerms = !agreeTermsCheckBox.on
        agreeTermsCheckBox.setOn(isAgreeTerms, animated: true)
    }
    
    @IBAction func termsAndConditionAction(_ sender: Any) {
        let main = UIStoryboard(name: "Main", bundle: nil)
        let vc = main.instantiateViewController(withIdentifier: "AboutUsViewController") as! AboutUsViewController
        vc.htmlType = HtmlType.StudentTerms
        vc.headerTitle = localizedSitringFor(key: "terms")
        present(vc, animated: true)

    }
    
}

//MARK:- helpers
extension StudentRegisterController{
    
    func initView() {
        NETWORK.deleget = self
        agreeTermsCheckBox.boxType = .square
        hideKeyboardWhenTappedAround()
        setupFont()
    }
    
    /// init fonts depend on language
    func setupFont() {
        mainQueue {
            self.firstNAmeTextField.font = UIFont(name: localizedSitringFor(key: "AppRegularFont"), size: iphoneXFactor*45 )
            self.midNameTextField.font = UIFont(name: localizedSitringFor(key: "AppRegularFont"), size: iphoneXFactor*45 )
            self.lastNameTextField.font = UIFont(name: localizedSitringFor(key: "AppRegularFont"), size: iphoneXFactor*45 )
            self.emailTextField.font = UIFont(name: localizedSitringFor(key: "AppRegularFont"), size: iphoneXFactor*45 )
            self.userNameTextField.font = UIFont(name: localizedSitringFor(key: "AppRegularFont"), size: iphoneXFactor*45 )
            self.phoneTextField.font = UIFont(name: localizedSitringFor(key: "AppRegularFont"), size: iphoneXFactor*45 )
            self.registerButton.titleLabel?.font = UIFont(name: localizedSitringFor(key: "AppBoldFont"), size: iphoneXFactor*50)
            self.r_agrreTermsLabel.font = UIFont(name: localizedSitringFor(key: "AppRegularFont"), size: iphoneXFactor*40)
            self.b_termsConditionLabel.font = UIFont(name: localizedSitringFor(key: "AppBoldFont"), size: iphoneXFactor*45)
            self.r_agrreTermsLabel.text = localizedSitringFor(key: "agree")
            self.b_termsConditionLabel.text = localizedSitringFor(key: "terms")
        }
    }
    /// validate Fields
    ///
    /// - Returns: true or false
    func ValidateField() -> Bool{
        
        if firstNAmeTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty == true {
            displayMessage(message: localizedSitringFor(key: "emptyFirstName"), messageError: true)
            return false
        }

        if midNameTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty == true {
            displayMessage(message: localizedSitringFor(key: "emptyMidName"), messageError: true)
            return false
        }

        if lastNameTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty == true {
            displayMessage(message: localizedSitringFor(key: "emptyLastName"), messageError: true)
            return false
        }

        if phoneTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty == true || phoneTextField.text?.count ?? 0 < 8{
            displayMessage(message: localizedSitringFor(key: "emptyPhone"), messageError: true)
            return false
        }

        if !isValidEmail(emailTextField.text ?? "") {
            displayMessage(message: localizedSitringFor(key: "emptyEmail"), messageError: true)
            return false
        }
        
        if userNameTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty == true {
              displayMessage(message: localizedSitringFor(key: "emptyUserName"), messageError: true)
              return false
          }

        if passwordTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty == true ||  passwordTextField.text?.count ?? 0 < 6 {
            displayMessage(message: localizedSitringFor(key: "emptyPassword"), messageError: true)
            return false
        }
        
        if !isAgreeTerms {
            displayMessage(message: localizedSitringFor(key: "agreeTerms"), messageError: true)
            return false
        }
        return true
    }
    
    func goToPackage() {
        let main = UIStoryboard(name: "Main", bundle: nil)
        let vc = main.instantiateViewController(withIdentifier: "StudentRegisterPackageController") as! StudentRegisterPackageController
        vc.isFromRegister = true
        self.present(vc, animated: true, completion: nil)
    }
    
}

//MARK:- Networking
extension StudentRegisterController: NetworkingHelperDeleget{
    func onHelper(getData data: DataResponse<String>, fromApiName name: String, withIdentifier id: String) {
        if id == STUDENT_REGISTER { handleSignupStudentResponse(response: data) }
        else if id == ADD_TOKEN { handleToken(forResponse: data) }
        else if id == DUMMY_PACKAGE { handleUserDummyPackageResponse(response: data) }
    }
    
    func onHelper(getError error: String, fromApiName name: String, withIdentifier id: String) {
        displayMessage(message: localizedSitringFor(key: "unknowError") , messageError: true)
    }
    
    // MARK:- request apis from server
    func requestSignupStudentApi() {
        var paramters: [String: String] = [:]
        paramters["firstname"] = firstNAmeTextField.text ?? ""
        paramters["middlename"] = midNameTextField.text ?? ""
        paramters["lastname"] = lastNameTextField.text ?? ""
        paramters["mobile"] = phoneTextField.text ?? ""
        paramters["email"] = emailTextField.text ?? ""
        paramters["username"] = userNameTextField.text ?? ""
        paramters["password"] = passwordTextField.text ?? ""
        
        print(paramters)
        NETWORK.connectWithHeaderTo(api: ApiNames.SIGN_UP_STUDENT ,withParameters: paramters,andIdentifier: STUDENT_REGISTER , withLoader: true, forController: self, methodType: .post)
    }
    
    func getTokenRequest(){
        print("paramters","?userid=\(Account.shared.id)&DeviceType=1&token=\(UserDefaults.standard.string(forKey: "deviceToken") ?? "")&DeviceId=\(deviceID)")
        NETWORK.connectWithHeaderTo(api: ApiNames.TOKEN + "?userid=\(Account.shared.id)&DeviceType=1&token=\(UserDefaults.standard.string(forKey: "deviceToken") ?? "")&DeviceId=\(deviceID)",andIdentifier: ADD_TOKEN ,withLoader: false, methodType: .post)
    }
    
    func requestUserDummyPackage() {
        NETWORK.connectWithHeaderTo(api: ApiNames.userDummyPackageURL + "?userid=\(Account.shared.id)",andIdentifier: DUMMY_PACKAGE ,withLoader: true, methodType: .post)
    }
    
    // MARK:- handle response server
    
    func handleSignupStudentResponse(response: DataResponse<String>){

        switch response.response?.statusCode {
        case 200:
            do{
                let appUser = try JSONDecoder().decode(AppUser.self, from: response.data ?? Data())
                if !(appUser.user?.isActive ?? false) {
                    displayMessageWithTime(message: localizedSitringFor(key: "notActive"), messageError: true, time: 5)
                    goToView(withId: "LoginViewController")
                    return
                }
                Account.shared.Token = appUser.token ?? ""
                Account.shared.id = appUser.user?.id ?? 0
                Account.shared.isTeacher = appUser.user?.isTeacher ?? false
                Account.shared.userName = appUser.user?.userName ?? ""
                Account.shared.isSubscribe = appUser.issubscribe ?? false
                Account.shared.storeData()
                getTokenRequest()
                if Account.shared.showPayment == false && Account.shared.onlineAppVersion == appVersion {
                    requestUserDummyPackage()
                }else {
                    goToPackage()
                }
                
          }catch{
              displayMessage(message: localizedSitringFor(key: "unknownError"), messageError: true)
          }
        case 201:
            displayMessage(message: localizedSitringFor(key: "ExistMail"), messageError: true)
        case 202:
            displayMessage(message: localizedSitringFor(key: "ExistUserName"), messageError: true)
        case 203:
            displayMessage(message: localizedSitringFor(key: "ExistPhone"), messageError: true)
         case 204:
         displayMessage(message: localizedSitringFor(key: "NoUserFound"), messageError: true)
        case 400:
         displayMessage(message: localizedSitringFor(key: "badRequst"), messageError: true)
        case 401:
            displayMessage(message: localizedSitringFor(key: "Unauthorized"), messageError: true)
        default:
            displayMessage(message: localizedSitringFor(key: "unknownError"), messageError: true)
        }
    }
    
    
    func handleToken(forResponse response: DataResponse<String>){
         print("token_added")
         print(response.description)
     }
    
    func handleUserDummyPackageResponse(response: DataResponse<String>){

        switch response.response?.statusCode {
        case 200:
            print("success dummy package")
            Account.shared.isSubscribe = true
            Account.shared.storeData()
            pushToView(withId: "TabBarViewController")
        default:
            print(response.response?.statusCode ?? 0)
            pushToView(withId: "TabBarViewController")
            //displayMessage(message: localizedSitringFor(key: "unknownError"), messageError: true)
        }
    }
    
    
}
