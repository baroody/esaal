//
//  ResetPasswordController.swift
//  ESAAL
//
//  Created by Mina Thabet on 11/8/19.
//  Copyright © 2019 HardTask. All rights reserved.
//

import UIKit
import Alamofire

class ResetPasswordController: UIViewController {
    
        // MARK: - IBOutlets
        @IBOutlet weak var oldPasswordTextField: UITextField!
        @IBOutlet weak var newPasswordTextField: UITextField!
        @IBOutlet weak var confirmationPasswordTextField: UITextField!
        @IBOutlet var sendButton: UIButton!
        
        ///variables
        var header: HeaderView!
        var headerIsLoaded = false
        
        ///CONSTANTS
        let NETWORK = NetworkingHelper()
        let LOGOUT = "logout"
        let RESET = "RESET"

    
        override func viewDidLoad() {
            super.viewDidLoad()
            
            initView()
        }

    @IBAction func backAction() {
        navigationController?.popViewController(animated: true)
    }
    
        @IBAction func sendAction() {
            if validate(){
                changePasswordRequest()
            }
        }
        
    }


// MARK: - helpers methods
extension ResetPasswordController{
    
    func initView(){
        hideKeyboardWhenTappedAround()
        NETWORK.deleget = self
            mainQueue {
                self.oldPasswordTextField.font = UIFont(name: localizedSitringFor(key: "AppRegularFont"), size: iphoneXFactor*45)
                self.newPasswordTextField.font = UIFont(name: localizedSitringFor(key: "AppRegularFont"), size: iphoneXFactor*45)
                self.confirmationPasswordTextField.font = UIFont(name: localizedSitringFor(key: "AppRegularFont"), size: iphoneXFactor*45)
                self.sendButton.titleLabel?.font = UIFont(name: localizedSitringFor(key: "AppBoldFont"), size: iphoneXFactor*50)
            }
    }
    func validate() -> Bool{
        
        if oldPasswordTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty == true {
            displayMessage(message: localizedSitringFor(key: "emptyOldPassword"), messageError: true)
            return false
        }
        
        if newPasswordTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty == true {
            displayMessage(message: localizedSitringFor(key: "emptyNewPassword"), messageError: true)

            return false
        }
        
        if confirmationPasswordTextField.text != newPasswordTextField.text {
            displayMessage(message: localizedSitringFor(key: "equalPassword"), messageError: true)

            return false
        }
        return true
    }
}


// MARK: - networking
extension ResetPasswordController: NetworkingHelperDeleget{
    func onHelper(getData data: DataResponse<String>, fromApiName name: String, withIdentifier id: String) {
        if id == LOGOUT{ handleLogout(forResponse: data) }
        else{
            handleData(forResponse: data)
        }
        hideLoaderForController(self)
        
    }
    
    func onHelper(getError error: String, fromApiName name: String, withIdentifier id: String) {
        //failure code
        displayAlert("",localizedSitringFor(key: "unknownError"))
        hideLoaderForController(self)
        
    }
    
    /// uses for connecting to server and recive data
    func changePasswordRequest(withLoader hasLoader: Bool = true)
    {
        var parameters : [String:Any] = [:]
        parameters["userid"] = Account.shared.id
        parameters["oldPassword"] = oldPasswordTextField.text ?? ""
        parameters["password"] = newPasswordTextField.text ?? ""

        NETWORK.connectWithHeaderTo(api: ApiNames.CHANGE_PASSWORD,withParameters: parameters, withLoader: hasLoader, forController: self, methodType: .post)
    }
    
    /// handle data response from server
    ///
    /// - Parameter response: server response
    func handleData(forResponse response: DataResponse<String>)
    {
        switch response.response?.statusCode {
        case 200:
            self.navigationController?.popViewController(animated: true)
            displayMessage(message: localizedSitringFor(key: "success"), messageError: false)
        case 201:
            displayMessage(message: localizedSitringFor(key: "UserNotFound"), messageError: true)
        case 202:
            displayMessage(message: localizedSitringFor(key: "wrongOldPassword"), messageError: true)
        case 207:
            displayMessage(message: localizedSitringFor(key: "UserISNotActive"), messageError: true)
            delay(timeSession) {
                self.logoutRequest()
            }
        case 400:
            displayMessage(message: localizedSitringFor(key: "badRequst"), messageError: true)
        case 401:
            displayMessage(message: localizedSitringFor(key: "Unauthorized"), messageError: true)
        default:
            displayMessage(message: localizedSitringFor(key: "unknownError"), messageError: true)
        }
    }
    

    /// uses for connecting to server and recive data
    func logoutRequest()
    {
        var parameters: [String: Any] = [:]
        parameters["userid"] = Account.shared.id
        parameters["DeviceId"] = deviceID
        
        var url = ApiNames.LOGOUT + "?userid=" + String(Account.shared.id)
        url += "&DeviceId=" + deviceID
        
        NETWORK.connectWithHeaderTo(api: url, withParameters: parameters, andIdentifier: LOGOUT, withLoader: true, forController: self, methodType: .post)
    }
    
    
    /// handle login json response from server
    ///
    /// - Parameter response: server response
    func handleLogout(forResponse response: DataResponse<String>)
    {
        switch response.response?.statusCode {
        case 200:
            Account.shared.logout()
            pushToView(withId: baseSession)
        case 400:
            displayMessage(message: localizedSitringFor(key: "invalidUserOrPassword"), messageError: true)

        case 401:
            displayMessage(message: localizedSitringFor(key: "Unauthorized"), messageError: true)
        default:
            displayMessage(message: localizedSitringFor(key: "unknownError"), messageError: true)
        }
    }
}


