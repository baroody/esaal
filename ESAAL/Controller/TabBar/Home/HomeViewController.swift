//
//  HomeViewController.swift
//  ESAAL
//
//  Created by Mina Thabet on 11/4/19.
//  Copyright © 2019 HardTask. All rights reserved.
//

import UIKit
import Alamofire

class HomeViewController: UIViewController {
    
    //MARK:- IBOutlet
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var headerView: UIView!
    
    ///variables
    var header: HeaderView!
    var sliderPhotoList: [SliderImage] = []
    var questionList: [Question] = []
    var searchQuestionList: [Question] = []
    var page = 1
    var searchPage = 1
    var selectedMatrial: [String] = []
    var searchText = ""
    var isFilter = false
    var isSearch = false
    var myPendingQuetionId = -1
    
    ///constant
    let NETWORK = NetworkingHelper()
    let SLIDER_PHOTO = "sliderPhoto"
    let GET_USER_QUESTION = "getUserQuestion"
    let GET_NOTIFICATIONS = "GETNOTIFICATIONS"
    let GET_USER_QUESTION_BY_MATERIALS = "getUserQuestionByMaterials"
    let GET_USER_SEARCH_QUESTIONS = "getUserSearchQuestion"
    let LOGOUT = "logout"

    
    //MARK:- App life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        myPendingQuetionId = -1
        page = 1
        questionList = []
        print(Account.shared.id,"will")
        hideLoaderForController(self)
        if Account.shared.id != 0 {
            requestGetQuestionApi()
            getNotificationsRequest()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        if header == nil{
            header = HeaderView.initView(fromController: self, andView: headerView, andTitle: localizedSitringFor(key: "Home"), isFromRoot: true)
            header.addQuestionView.isHidden = Account.shared.isTeacher && Account.shared.id != 0
            header?.searchBar.isHidden = Account.shared.id == 0//false
            header?.filterView.isHidden = Account.shared.id == 0//false
            header?.searchView.isHidden = Account.shared.id == 0//false
            header?.deleget = self
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let filterVC = segue.destination as? FilterViewController{
            filterVC.filterDeleget = self
            if selectedMatrial.count > 0{
                filterVC.selectedMatrial = selectedMatrial
            }
        }
    }

}


//MARK:- Helpers
extension HomeViewController{
    func initView() {
        NETWORK.deleget = self
        initCommonTable()
        requestSliderPhotoApi()
       // requestGetQuestionApi()
    }
    
    func initCommonTable() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: CellIdentifier.EmptyQuestionCell.rawValue, bundle: nil), forCellReuseIdentifier: CellIdentifier.EmptyQuestionCell.rawValue)
        tableView.register(UINib(nibName: CellIdentifier.HeaderLabelCell.rawValue, bundle: nil), forCellReuseIdentifier: CellIdentifier.HeaderLabelCell.rawValue)
        tableView.register(UINib(nibName: CellIdentifier.TeacherHomeTableViewCell.rawValue, bundle: nil), forCellReuseIdentifier: CellIdentifier.TeacherHomeTableViewCell.rawValue)
    }
    
    func checkIfTeatcherHavePendingQuestion() {
        if Account.shared.isTeacher{
            questionLoop:for (index,quetion) in questionList.enumerated() {
                if quetion.pendingUserId == Account.shared.id && quetion.isPending == true {
                    print("inside if")
                    myPendingQuetionId = quetion.id ?? -1
                    questionList.remove(at: index)
                    questionList.insert(quetion, at: 0)
                    break questionLoop
                }
            }
        }
    }
    
    
    /// get next page for the list
    ///
    /// - Parameter index: the current cell indexPath
    func getNextPageIfNeeded(fromIndex index: IndexPath) {
        let count = isSearch ? searchQuestionList.count : questionList.count
        if index.row == count - 1 {
            if isSearch {
                searchPage += 1
                requestGetUserSearchQuestionApi()
            }else if isFilter {
                page += 1
                requestUserQuestionsByMaterialsApi()
            }
            else {
                page += 1
                requestGetQuestionApi()
            }
            
        }
    }
    
    /// get matrials ids form selected tags
    func getMatrialsIds() -> String {
        var ids = ""
        for (index,id) in selectedMatrial.enumerated() {
            if index == selectedMatrial.count - 1 {
                ids += id
            }else {
                ids += id + ","
            }
            
        }
        return ids
    }
}



//MARK:- table view delegat and datasource
extension HomeViewController: UITableViewDelegate,UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0 {
            let slider = Bundle.main.loadNibNamed("ImageSliderView", owner: self, options: nil)?.last as! ImageSliderView
            
            slider.initWith(images: sliderPhotoList.compactMap({ $0.imagePath?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""}), shouldOpenGallery: false, withController: self)
            return slider
        }else if section == 1 {
            if questionList.count > 0 {
                 let headerCell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier.HeaderLabelCell.rawValue) as! HeaderLabelCell
                headerCell.configureCell(label: localizedSitringFor(key: "lastQuetion"))
                return headerCell
            }
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return screenWidth * 0.7
        }else if section == 1 {
            if questionList.count > 0 {
                return screenWidth * 0.1
            }
        }
        return 0.0001
    }
    

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.00001
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 0
        }else if section == 1 {
            if questionList.count == 0 && isFilter == false && isSearch == false{
                return 1
            }
            return isSearch ? searchQuestionList.count : questionList.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 1 {
            if questionList.count == 0 && isFilter == false && isSearch == false{
                let emptyQuestionCell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier.EmptyQuestionCell.rawValue, for: indexPath) as! EmptyQuestionCell
                
                let isTeacher = Account.shared.isTeacher && Account.shared.id != 0
                
                let teacherLabel = localizedSitringFor(key: "NoPendingQuestionTeatcher")
                let studentLabel = localizedSitringFor(key: "NoPendingQuestionStudent")
                emptyQuestionCell.configureCell(label: isTeacher ? teacherLabel : studentLabel, isTeacher: isTeacher)
                
                emptyQuestionCell.deleget = self
                
                return emptyQuestionCell
            }
            let questionCell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier.TeacherHomeTableViewCell.rawValue, for: indexPath) as! TeacherHomeTableViewCell
            let questionOb = isSearch ? searchQuestionList[indexPath.item] : questionList[indexPath.item]
            if questionOb.id == myPendingQuetionId {
                questionCell.configureCell(question: questionOb,isPending: true)
            }else {
                questionCell.configureCell(question: questionOb)
            }
            getNextPageIfNeeded(fromIndex: indexPath)
            return questionCell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if questionList.count == 0 && isFilter == false && isSearch == false{
            return screenHeight * 0.4
        }
        return screenWidth * 0.35
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         if questionList.count > 0 {
            let main = UIStoryboard(name: "Main", bundle: nil)
            let vc = main.instantiateViewController(withIdentifier: "QuestionAndReplayController") as! QuestionAndReplayController
            vc.questionId = (isSearch ? searchQuestionList[indexPath.item].id ?? 0: questionList[indexPath.item].id ?? 0 )
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
        
}


//MARK:- Empty Question Cell Delegate
extension HomeViewController: EmptyQuestionCellDelegate{
    func addQuestionClicked() {
        if Account.shared.id == 0 {
            //displayAlertConfirmation(localizedSitringFor(key: "Attention"), localizedSitringFor(key: "notLogin")) {
                // go to login if user not login
                pushToView(withId: "LoginViewController")
            //}
            return
        }
        if !Account.shared.isTeacher {
            if !Account.shared.isSubscribe {
                displayAlert("",localizedSitringFor(key: "userNotHavePackage"))
                return
            }
        }
        performSegue(withIdentifier: "showAddQuestion", sender: nil)
    }
}


//MARK:- Header view deleget
extension HomeViewController: HeaderViewDelegate{
    func filterActionClicked() {
        performSegue(withIdentifier: "showFilter", sender: nil)
    }
    
    func searchActionClicked() {
        print("searchClicked")
        mainQueue {
            self.header?.searchBar.isHidden = false
            self.header.addQuestionView.isHidden = true
            self.header?.headerLabel.isHidden = true
            self.header?.filterView.isHidden = true
            self.header?.searchView.isHidden = true
        }
        UIView.animate(withDuration: 0.2) {
            self.header?.searchBar.alpha = 1
        }
        header?.searchBar.becomeFirstResponder()
        header?.searchBar.delegate = self
    }
}

//MARK:- Filter Selected Matrial Deleget
extension HomeViewController: FilterSelectedMatrialDeleget{
    func selectedMatrialClicked(selectedMatrials: [String]) {
        selectedMatrial = selectedMatrials
        page = 1
        questionList = []
        if selectedMatrials.count == 0 {
            isFilter = false
            requestGetQuestionApi()
        }else {
            isFilter = true
            requestUserQuestionsByMaterialsApi()
        }
        
    }
}


//MARK:- UI Search Bar Delegate
extension HomeViewController: UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        isSearch = true
        searchText = searchBar.text ?? ""
        searchPage = 1
        searchQuestionList = []
        if searchText != ""{
            searchBar.resignFirstResponder()
            requestGetUserSearchQuestionApi()
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        UIView.animate(withDuration: 0.2) {
            self.header?.searchBar.alpha = 0
        }
        mainQueue {
            self.header?.searchBar.isHidden = true
            self.header?.headerLabel.isHidden = false
            self.header?.addQuestionView.isHidden = Account.shared.isTeacher && Account.shared.id != 0
            self.header?.filterView.isHidden = false
            self.header?.searchView.isHidden = false
        }
        searchText = ""
        isSearch = false
        searchBar.resignFirstResponder()
        tableView.reloadData()
    }
}





//MARK:- Networking
extension HomeViewController: NetworkingHelperDeleget {
    func onHelper(getData data: DataResponse<String>, fromApiName name: String, withIdentifier id: String) {
        if id == SLIDER_PHOTO { handleSliderPhotoResponse(response: data) }
        else if id == GET_USER_QUESTION { handleGetQuestionResponse(response: data) }
        else if id == GET_NOTIFICATIONS { handleNotifications(forResponse: data) }
        else if id == GET_USER_QUESTION_BY_MATERIALS { handleGetQuestionByMaterialsResponse(forResponse: data) }
        else if id == GET_USER_SEARCH_QUESTIONS { handleGetUserSearchQuestionResponse(forResponse: data) }
        else if id == LOGOUT{ handleLogout(forResponse: data) }

    }
    
    func onHelper(getError error: String, fromApiName name: String, withIdentifier id: String) {
        displayMessage(message: localizedSitringFor(key: "unkwonError") , messageError: true)
    }
    
    // MARK:- request apis from server
    
    func requestSliderPhotoApi() {
        NETWORK.connectWithHeaderTo(api: ApiNames.SLIDER_PHOTO, andIdentifier: SLIDER_PHOTO, withLoader: false,forController: self, methodType: .get)
    }
    
    func requestGetQuestionApi() {
        var paramters: [String:Any] = [:]
        paramters["userid"] = Account.shared.id
        paramters["pageNum"] = page
        
        let urlData = "?userid=" + String(Account.shared.id) + "&pageNum=" + String(page)
        
        NETWORK.connectWithHeaderTo(api: ApiNames.GET_USER_QUESTIONS + urlData, andIdentifier: GET_USER_QUESTION, withLoader: true, forController: self, methodType: .get)
    }
    
    /// uses for connecting to server and recive data
       func getNotificationsRequest()
       {
           var parameters: [String: Any] = [:]
           parameters["userid"] = Account.shared.id
           parameters["pageNum"] = 1
           print(parameters)
           NETWORK.connectWithHeaderTo(api: ApiNames.GET_USER_NOTIFICATIONS+"?userid=\(Account.shared.id)&pageNum=\(1)", andIdentifier: GET_NOTIFICATIONS, withLoader: false, forController: self, methodType: .get)
       }
    
    func requestUserQuestionsByMaterialsApi() {
       var paramters: [String:Any] = [:]
       paramters["userid"] = Account.shared.id
       paramters["materialid"] = getMatrialsIds()//selectedMaterialId
       paramters["pageNum"] = page
       
        print(paramters)
        
       let urlData = "?userid=" + String(Account.shared.id) + "&materialid=" + getMatrialsIds() + "&pageNum=" + String(page)
       
        NETWORK.connectWithHeaderTo(api: ApiNames.GET_USER_QUESTIONS_BY_MATERIALS + urlData, andIdentifier: GET_USER_QUESTION_BY_MATERIALS, withLoader: true, forController: self, methodType: .get)
    }
    
    func requestGetUserSearchQuestionApi() {
       let strSearch = searchText.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
       var paramters: [String:Any] = [:]
       paramters["userid"] = Account.shared.id
       paramters["strSearch"] = strSearch
       paramters["pageNum"] = searchPage
       
       let urlData = "?userid=" + String(Account.shared.id) + "&strSearch=" + strSearch + "&pageNum=" + String(searchPage)
       
        NETWORK.connectWithHeaderTo(api: ApiNames.GET_USER_SEARCH_QUESTIONS + urlData, andIdentifier: GET_USER_SEARCH_QUESTIONS, withLoader: true, forController: self, methodType: .get)
    }
       
       
   /// handle notifications json response from server
   ///
   /// - Parameter response: server response
   func handleNotifications(forResponse response: DataResponse<String>)
   {
       switch response.response?.statusCode {
       case 200:
            do{
                let notification = try JSONDecoder().decode([NewNotification].self, from: response.data ?? Data())
                var found = false
                
                for notify in notification{
                    if !(notify.isRead ?? true) {
                            found = true
                    }
                }
                
                if found {
                    if UIApplication.isRTL(){
                        // for atabic
                        addRedDotAtTabBarItemIndex(parant: self, index: 1)
                    }else {
                        // for english
                        addRedDotAtTabBarItemIndex(parant: self, index: 2)
                    }
                }
                

         }catch let error{
           print("error", error)
             displayMessage(message: localizedSitringFor(key: "unknownError"), messageError: true)
         }
       case 207:
            displayMessage(message: localizedSitringFor(key: "UserISNotActive"), messageError: true)
            delay(timeSession) {
                self.logoutRequest()
            }
       case 203:
               displayMessage(message: localizedSitringFor(key: "UserNotFound"), messageError: true)

       case 204:
                //displayMessage(message: localizedSitringFor(key: "emptyNotifications"), messageError: true)
        print("empty")
       case 400:
        print("empty")
           //displayMessage(message: localizedSitringFor(key: "badRequst"), messageError: true)
       case 401:
        print("empty")
           displayMessage(message: localizedSitringFor(key: "Unauthorized"), messageError: true)
       default:
           displayMessage(message: response.description, messageError: true)
       }
   }
   
    
    
    // MARK:- handle response server
    func handleSliderPhotoResponse(response: DataResponse<String>){
        switch response.response?.statusCode {
        case 200:
            do{
                let sliderPhoto = try JSONDecoder().decode([SliderImage].self, from: response.data ?? Data())
                sliderPhotoList = sliderPhoto
                print("slider count:",sliderPhotoList.count)
                tableView.reloadData()
          }catch{
              displayMessage(message: localizedSitringFor(key: "unknownError"), messageError: true)
          }
        case 400:
            displayMessage(message: localizedSitringFor(key: "badRequst"), messageError: true)
        case 401:
            displayMessage(message: localizedSitringFor(key: "Unauthorized"), messageError: true)
        case 207:
            displayMessage(message: localizedSitringFor(key: "UserISNotActive"), messageError: true)
            delay(timeSession) {
                self.logoutRequest()
            }
        default:
            displayMessage(message: localizedSitringFor(key: "unknownError"), messageError: true)
        }
    }
    
    
    
    func handleGetQuestionResponse(response: DataResponse<String>){
           switch response.response?.statusCode {
           case 200:
               do{
                let questions = try JSONDecoder().decode([Question].self, from: response.data ?? Data())

                questionList += questions
                if myPendingQuetionId == -1{
                     checkIfTeatcherHavePendingQuestion()
                 }
                if questions.count == 0 && questionList.count == 0{
                    displayMessage(message: localizedSitringFor(key: "emptyResult"), messageError: false)
                    tableView.reloadData()
                    return
                }else if questions.count == 0{
                    return
                }
 
                tableView.reloadData()
             }catch let error{
                print(error)
                 displayMessage(message: localizedSitringFor(key: "unknownError"), messageError: true)
             }
           case 203:
               displayMessage(message: localizedSitringFor(key: "UserNotFound"), messageError: true)
           case 201:
               print(localizedSitringFor(key: "UserNotHaveQuestions"))
               // displayMessage(message: localizedSitringFor(key: "UserNotHaveQuestions"), messageError: true)
           case 400:
               displayMessage(message: localizedSitringFor(key: "badRequst"), messageError: true)
           case 401:
               displayMessage(message: localizedSitringFor(key: "Unauthorized"), messageError: true)
            case 207:
                displayMessage(message: localizedSitringFor(key: "UserISNotActive"), messageError: true)
                delay(timeSession) {
                    self.logoutRequest()
                }
           default:
               displayMessage(message: localizedSitringFor(key: "unknownError"), messageError: true)
           }
        mainQueue {
            hideLoaderForController(self)
        }
           
       }
    
    func handleGetQuestionByMaterialsResponse(forResponse response: DataResponse<String>){
        switch response.response?.statusCode {
        case 200:

              do{
                  let questions = try JSONDecoder().decode([Question].self, from: response.data ?? Data())
                  questionList += questions
                  if questions.count == 0 && questionList.count == 0{
                      displayMessage(message: localizedSitringFor(key: "emptyResult"), messageError: false)
                      tableView.reloadData()
                      return
                  }else if questions.count == 0{
                      return
                  }
                  tableView.reloadData()
            }catch let error{
               print(error)
                displayMessage(message: localizedSitringFor(key: "unknownError"), messageError: true)
            }
            
        case 203:
            displayMessage(message: localizedSitringFor(key: "UserNotFound"), messageError: true)
        case 201:
            questionList = []
            tableView.reloadData()
            displayMessage(message: localizedSitringFor(key: "UserNotHaveQuestions"), messageError: true)
        case 400:
            displayMessage(message: localizedSitringFor(key: "badRequst"), messageError: true)
        case 401:
            displayMessage(message: localizedSitringFor(key: "Unauthorized"), messageError: true)
            case 207:
                displayMessage(message: localizedSitringFor(key: "UserISNotActive"), messageError: true)
                delay(timeSession) {
                    self.logoutRequest()
                }
        default:
            displayMessage(message: localizedSitringFor(key: "unknownError"), messageError: true)
        }
    }
    
    
    func handleGetUserSearchQuestionResponse(forResponse response: DataResponse<String>){
        switch response.response?.statusCode {
        case 200:

              do{
                  let questions = try JSONDecoder().decode([Question].self, from: response.data ?? Data())
                searchQuestionList += questions
                if questions.count == 0 && searchQuestionList.count == 0{
                    displayMessage(message: localizedSitringFor(key: "emptyResult"), messageError: false)
                    tableView.reloadData()
                    return
                }else if questions.count == 0{
                    return
                }
                  tableView.reloadData()
            }catch let error{
               print(error)
                displayMessage(message: localizedSitringFor(key: "unknownError"), messageError: true)
            }
            
        case 203:
            displayMessage(message: localizedSitringFor(key: "UserNotFound"), messageError: true)
        case 201:
            searchQuestionList = []
            tableView.reloadData()
            displayMessage(message: localizedSitringFor(key: "UserNotHaveQuestions"), messageError: true)
        case 400:
            displayMessage(message: localizedSitringFor(key: "badRequst"), messageError: true)
        case 401:
            displayMessage(message: localizedSitringFor(key: "Unauthorized"), messageError: true)
            case 207:
                displayMessage(message: localizedSitringFor(key: "UserISNotActive"), messageError: true)
                delay(timeSession) {
                    self.logoutRequest()
                }
        default:
            displayMessage(message: localizedSitringFor(key: "unknownError"), messageError: true)
        }
    }
    
    /// uses for connecting to server and recive data
    func logoutRequest()
    {
        var parameters: [String: Any] = [:]
        parameters["userid"] = Account.shared.id
        parameters["DeviceId"] = deviceID
        
        var url = ApiNames.LOGOUT + "?userid=" + String(Account.shared.id)
        url += "&DeviceId=" + deviceID
        
        NETWORK.connectWithHeaderTo(api: url, withParameters: parameters, andIdentifier: LOGOUT, withLoader: true, forController: self, methodType: .post)
    }
    
    
    /// handle login json response from server
    ///
    /// - Parameter response: server response
    func handleLogout(forResponse response: DataResponse<String>)
    {
        switch response.response?.statusCode {
        case 200:
            Account.shared.logout()
            pushToView(withId: baseSession)
        case 400:
            displayMessage(message: localizedSitringFor(key: "invalidUserOrPassword"), messageError: true)

        case 401:
            displayMessage(message: localizedSitringFor(key: "Unauthorized"), messageError: true)
        default:
            displayMessage(message: localizedSitringFor(key: "unknownError"), messageError: true)
        }
    }
}

