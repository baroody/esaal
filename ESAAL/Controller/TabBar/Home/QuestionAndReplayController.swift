//
//  QuestionAndReplayController.swift
//  ESAAL
//
//  Created by Mina Thabet on 11/14/19.
//  Copyright © 2019 HardTask. All rights reserved.
//

import UIKit
import Alamofire
import AVKit
import AVFoundation
import ReplayKit
import SKPhotoBrowser
import MediaPlayer



class QuestionAndReplayController: UIViewController {
    
    //MARK:- IBOutlet
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var headerView: GradientView!
    
    ///constant
    let NETWORK = NetworkingHelper()
    let GET_QUESTION = "getQuestion"
    let PEND_QUESTION = "pendQuestion"
    let REMOVE_PEND_QUESTION = "removePendQuestion"
    let LIKE_REPLAY = "likeReplay"
    let DISLIKE_REPLAY = "disLikeReplay"
    let LOGOUT = "logout"

    
    ///variables
    var header: HeaderView?
    var questionId = 0
    var questionDetails: Question?
    var imageForHeader = UIImage()
    var imagesArr: [UIImage] = []
    
    //MARK:- App Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        hideLoaderForController(self)
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if header == nil{
            header = HeaderView.initView(fromController: self, andView: headerView, andTitle: localizedSitringFor(key: "A & Q"), isFromRoot: true)
            header?.backView.isHidden = false
        }
        requestGetQuestionApi()
        
    }
}

//MARK:- Helpers
extension QuestionAndReplayController{
    func initView() {
        NETWORK.deleget = self
        initCommonTableView()
    }
    
    func initCommonTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: CellIdentifier.QuestionHeaderCell.rawValue, bundle: nil), forCellReuseIdentifier: CellIdentifier.QuestionHeaderCell.rawValue)
        tableView.register(UINib(nibName: CellIdentifier.QuestionReplayCell.rawValue, bundle: nil), forCellReuseIdentifier: CellIdentifier.QuestionReplayCell.rawValue)
    }
    
    func openPhotoWithUrl(imageUrl: String) {
       let skPhoto = SKPhoto.photoWithImageURL(imageUrl)
        skPhoto.shouldCachePhotoURLImage = true
       let browser = SKPhotoBrowser(photos: [skPhoto])
       browser.initializePageIndex(0)
       present(browser, animated: true, completion: {})
    }
    
    func openPhotoWithUIImage(image: UIImage) {
       let skPhoto = SKPhoto.photoWithImage(image)
       skPhoto.shouldCachePhotoURLImage = true
       let browser = SKPhotoBrowser(photos: [skPhoto])
       browser.initializePageIndex(0)
       present(browser, animated: true, completion: {})
    }
    
    
    func palyVideo(videoURL: NSURL) {
        
        goToVideoPlayer(link: videoURL as URL)
    }
    
    func handelImageClicked(attachmentMedia: [Attachement]) {
        for media in attachmentMedia {
           if media.fileType == "i"{
               if let image = media.fileUrl {
                   openPhotoWithUrl(imageUrl: image)
               }
           }
        }
    }
    
    func handelVideoCliecked(attachmentMedia: [Attachement]) {
        for media in attachmentMedia {
            if media.fileType == "v"{
                if let url = media.fileUrl {
                    if let videoUrl = URL(string: url) {
                        palyVideo(videoURL: videoUrl as NSURL)
                    }
                }
            }
        }
    }
    
    func goToReplayController() {
       let main = UIStoryboard(name: "Main", bundle: nil)
       let vc = main.instantiateViewController(withIdentifier: "AnswerQuestionController") as! AnswerQuestionController
        vc.question = questionDetails
       self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func goToEditQuestion() {
        let main = UIStoryboard(name: "Main", bundle: nil)
        let vc = main.instantiateViewController(withIdentifier: "AddQuestionController") as! AddQuestionController
        vc.isUpdateQuestion = true
        vc.questionDetails = questionDetails
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func getThumbnailImageFromVideoURL(fromUrl url: URL) -> UIImage? {
        let asset: AVAsset = AVAsset(url: url)
        let imageGenerator = AVAssetImageGenerator(asset: asset)
        do {
            let thumbnailImage = try imageGenerator.copyCGImage(at: CMTimeMake(value: 1, timescale: 60) , actualTime: nil)
            return UIImage(cgImage: thumbnailImage)
        } catch let error {
            print(error)
        }
        
        return nil
    }
    
    func goToVideoPlayer(link: URL) {
        let main = UIStoryboard(name: "Main", bundle: nil)
        let vc = main.instantiateViewController(withIdentifier: "VideoPlayerViewController") as! VideoPlayerViewController
        vc.videoLink = link
        self.present(vc, animated: true, completion: nil)
    }
}


//MARK:- tabel view deleget and datasource
extension QuestionAndReplayController: UITableViewDataSource,UITableViewDelegate {
    

    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if let question = questionDetails {
            let headerTable = tableView.dequeueReusableCell(withIdentifier: CellIdentifier.QuestionHeaderCell.rawValue) as! QuestionHeaderCell
                headerTable.deleget = self
                headerTable.configureCell(question: question)
            return headerTable

        }
        return nil
    }
    

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return questionDetails?.replayQuestions?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier.QuestionReplayCell.rawValue, for: indexPath) as! QuestionReplayCell
        cell.deleget = self

        if let question = questionDetails {
            if let replayQuestion = question.replayQuestions?[indexPath.row] {
                cell.configureCell(questionReplay: replayQuestion)
            }
        }
        return cell
    }
    
    
}

//MARK:- Question Header Cell Deleget
extension QuestionAndReplayController: QuestionHeaderCellDeleget {
    func reservatioOrReplayOrEdit(_ sender: QuestionHeaderCell, actionType: ActionType) {
        switch actionType {
        case .Reservation:
            self.requestPendQuestionApi()
        case .Replay:
            self.goToReplayController()
        case .Edit:
            self.goToEditQuestion()
        case .Reserved:
            displayAlert(localizedSitringFor(key: "addImageTitle") , localizedSitringFor(key: "reservedQuestion"))
        default:
            print("error")
        }
    }
    
    func openImageActionClicked(_ sender: QuestionHeaderCell) {
        openPhotoWithUIImage(image: sender.questionImageView.image ?? UIImage())
//        if let attachmentMedia = questionDetails?.attachment {
//            handelImageClicked(attachmentMedia: attachmentMedia)
//        }
        
    }
    
    func openVideoActionClicked(_ sender: QuestionHeaderCell) {
        if let attachmentMedia = questionDetails?.attachment {
            handelVideoCliecked(attachmentMedia: attachmentMedia)
        }
    }
    
    func cancelTimerActionClicked(_ sender: QuestionHeaderCell) {
        self.requestRemovePendQuestionApi()
    }
    
    
}


//MARK:- Question Replay Cell Delegate
extension QuestionAndReplayController: QuestionReplayCellDelegate {

    
    func replayActionClicked(_ sender: QuestionReplayCell) {
        self.goToReplayController()
    }
    
    func openImageActionClicked(_ sender: QuestionReplayCell) {
        guard let tappedIndexPath = tableView.indexPath(for: sender) else {
            return
        }
        print(tappedIndexPath)
        openPhotoWithUIImage(image: sender.questionImageView.image ?? UIImage())
//        if let question = questionDetails {
//           if let replayQuestion = question.replayQuestions?[tappedIndexPath.row] {
//            if let attachmentMedia = replayQuestion.attachments {
//                handelImageClicked(attachmentMedia: attachmentMedia)
//              }
//           }
//        }
        
    }
    
    func openVideoActionClicked(_ sender: QuestionReplayCell) {
        guard let tappedIndexPath = tableView.indexPath(for: sender) else {
            return
        }
        if let question = questionDetails {
          if let replayQuestion = question.replayQuestions?[tappedIndexPath.row] {
            if let attachmentMedia = replayQuestion.attachments{
                handelVideoCliecked(attachmentMedia: attachmentMedia)
             }
          }
        }
        
    }
    
    func disLikeActionClicked(_ sender: QuestionReplayCell) {
        guard let tappedIndexPath = tableView.indexPath(for: sender) else {
            return
        }
        if let isDislike = questionDetails?.replayQuestions?[tappedIndexPath.row].isDisliked{
            questionDetails?.replayQuestions?[tappedIndexPath.row].isDisliked = !isDislike
        }
        questionDetails?.replayQuestions?[tappedIndexPath.row].isLiked = false
        requestDisikeReplayApi(replayId: questionDetails?.replayQuestions?[tappedIndexPath.row].id ?? 0)
    }
    
    func likeActionClicked(_ sender: QuestionReplayCell) {
        guard let tappedIndexPath = tableView.indexPath(for: sender) else {
            return
        }
        if let islike = questionDetails?.replayQuestions?[tappedIndexPath.row].isLiked {
            questionDetails?.replayQuestions?[tappedIndexPath.row].isLiked = !islike
        }
        questionDetails?.replayQuestions?[tappedIndexPath.row].isDisliked = false
        requestLikeReplayApi(replayId: questionDetails?.replayQuestions?[tappedIndexPath.row].id ?? 0)
    }
    
    
}


//MARK:- Networking
extension QuestionAndReplayController: NetworkingHelperDeleget {
    func onHelper(getData data: DataResponse<String>, fromApiName name: String, withIdentifier id: String) {
        if id == GET_QUESTION { handleGetQuestionResponse(response: data) }
        else if id == PEND_QUESTION { handlePendQuestionResponse(response: data) }
        else if id == REMOVE_PEND_QUESTION { handleRemovePendQuestionResponse(response: data) }
        else if id == LIKE_REPLAY { handleLikeReplayResponse(response: data) }
        else if id == DISLIKE_REPLAY { handleDisLikeReplayResponse(response: data) }
        else if id == LOGOUT{ handleLogout(forResponse: data) }

    }
    
    func onHelper(getError error: String, fromApiName name: String, withIdentifier id: String) {
        displayMessage(message: localizedSitringFor(key: "unkwonError") , messageError: true)
    }
    
    // MARK:- request apis from server
    func requestGetQuestionApi() {
        var paramters: [String:Any] = [:]
        paramters["questionid"] = questionId
        
        var urlData = "?questionid=" + String(questionId)
        urlData += "&userid=\(Account.shared.id)"
        
        
        NETWORK.connectWithHeaderTo(api: ApiNames.GET_QUESTION + urlData, andIdentifier: GET_QUESTION, withLoader: true, forController: self, methodType: .get)
    }
    
    func requestPendQuestionApi() {
        
        var paramters: [String:Any] = [:]
        paramters["requestquestionid"] = questionId
        paramters["userid"] = Account.shared.id
        
        let dataUrl = "?requestquestionid=" + String(questionId) + "&userid=" + String(Account.shared.id)
        
        NETWORK.connectWithHeaderTo(api: ApiNames.PEND_QUESTION + dataUrl, withParameters: paramters, andIdentifier: PEND_QUESTION, withLoader: true,  forController: self, methodType: .post)
    }
    
    func requestRemovePendQuestionApi() {
       var paramters: [String:Any] = [:]
       paramters["requestquestionid"] = questionId
       paramters["userid"] = Account.shared.id
        
       let dataUrl = "?requestquestionid=" + String(questionId) + "&userid=" + String(Account.shared.id)
       
       NETWORK.connectWithHeaderTo(api: ApiNames.REMOVE_PEND_QUESTION + dataUrl, withParameters: paramters, andIdentifier: REMOVE_PEND_QUESTION, withLoader: true,  forController: self, methodType: .post)
    }
    
    func requestLikeReplayApi(replayId: Int) {

        var paramters: [String:Any] = [:]
        paramters["replayid"] = replayId
               
        let dataUrl = "?replayid=" + String(replayId)
              
        NETWORK.connectWithHeaderTo(api: ApiNames.LIKE_REPLY + dataUrl, withParameters: paramters, andIdentifier: LIKE_REPLAY, withLoader: true,  forController: self, methodType: .post)
    }
    
    func requestDisikeReplayApi(replayId: Int) {
        var paramters: [String:Any] = [:]
        paramters["replayid"] = replayId
               
        let dataUrl = "?replayid=" + String(replayId)
              
        NETWORK.connectWithHeaderTo(api: ApiNames.DIS_LIKE_REPLY + dataUrl, withParameters: paramters, andIdentifier: DISLIKE_REPLAY, withLoader: true,  forController: self, methodType: .post)
    }
    
    
    // MARK:- handle response server
    
    func handleGetQuestionResponse(response: DataResponse<String>){
        print("question,",response)
           switch response.response?.statusCode {
           case 200:
               do{
                   let question = try JSONDecoder().decode([Question].self, from: response.data ?? Data())
                if question.count == 0 {
                    self.navigationController?.popViewController(animated: true)
                    self.dismiss(animated: true, completion: nil)
                    displayMessage(message: localizedSitringFor(key: "notAvaliableQuestion"), messageError: true)
                }else {
                    questionDetails = question[0]
                    tableView.reloadData()
                }
             }catch let error{
                 print(error)
                 displayMessage(message: localizedSitringFor(key: "unknownError"), messageError: true)
             }
           case 203:
              displayMessage(message: localizedSitringFor(key: "MisQuestions"), messageError: true)
        case 207:
            displayMessage(message: localizedSitringFor(key: "UserISNotActive"), messageError: true)
            delay(timeSession) {
                self.logoutRequest()
            }
          case 400:
              displayMessage(message: localizedSitringFor(key: "badRequst"), messageError: true)
          case 401:
              displayMessage(message: localizedSitringFor(key: "Unauthorized"), messageError: true)
          default:
              displayMessage(message: localizedSitringFor(key: "unknownError"), messageError: true)
           }
       }
    
    func handlePendQuestionResponse(response: DataResponse<String>){
        switch response.response?.statusCode {
        case 200:
            do{
                let question = try JSONDecoder().decode(Question.self, from: response.data ?? Data())
                questionDetails?.isPending = question.isPending
                questionDetails?.pendingUserId = question.pendingUserId
                questionDetails?.pendingDate = question.pendingDate
                questionDetails?.pendingEndDate = question.pendingEndDate
                questionDetails?.remainTime = question.remainTime
                tableView.reloadData()
          }catch let error{
             print(error)
              displayMessage(message: localizedSitringFor(key: "unknownError"), messageError: true)
          }
        case 201:
            displayMessage(message: localizedSitringFor(key: "MisQuestions"), messageError: true)
        case 202:
             displayMessage(message: localizedSitringFor(key: "TeacherMustReplayfistonquestion"), messageError: true)
        case 203:
             displayMessage(message: localizedSitringFor(key: "UserNotFound"), messageError: true)
        case 204:
            displayMessage(message: localizedSitringFor(key: "YouCantMakeMoreThanOnequestionPending"), messageError: true)
        case 205:
            displayMessage(message: localizedSitringFor(key: "Thisquestionnowpendingwithanotherteacher"), messageError: true)
        case 207:
            displayMessage(message: localizedSitringFor(key: "UserISNotActive"), messageError: true)
            delay(timeSession) {
                self.logoutRequest()
            }
        case 400:
            displayMessage(message: localizedSitringFor(key: "badRequst"), messageError: true)
        case 401:
            displayMessage(message: localizedSitringFor(key: "Unauthorized"), messageError: true)
        default:
            displayMessage(message: localizedSitringFor(key: "unknownError"), messageError: true)
        }
    }
    
    func handleRemovePendQuestionResponse(response: DataResponse<String>){
        switch response.response?.statusCode {
        case 200:
            do{
                let question = try JSONDecoder().decode(Question.self, from: response.data ?? Data())
                questionDetails?.isPending = question.isPending
                questionDetails?.pendingUserId = question.pendingUserId
                questionDetails?.pendingDate = question.pendingDate
                questionDetails?.pendingEndDate = question.pendingEndDate
                questionDetails?.remainTime = question.remainTime
                tableView.reloadData()
          }catch let error{
             print(error)
              displayMessage(message: localizedSitringFor(key: "unknownError"), messageError: true)
          }
           case 201:
               displayMessage(message: localizedSitringFor(key: "MisQuestions"), messageError: true)
           case 202:
                displayMessage(message: localizedSitringFor(key: "TeacherMustReplayfistonquestion"), messageError: true)
           case 203:
                displayMessage(message: localizedSitringFor(key: "UserNotFound"), messageError: true)
           case 204:
               displayMessage(message: localizedSitringFor(key: "YouCantMakeMoreThanOnequestionPending"), messageError: true)
           case 205:
               displayMessage(message: localizedSitringFor(key: "Thisquestionnowpendingwithanotherteacher"), messageError: true)
            case 207:
                displayMessage(message: localizedSitringFor(key: "UserISNotActive"), messageError: true)
                delay(timeSession) {
                    self.logoutRequest()
                }
           case 400:
               displayMessage(message: localizedSitringFor(key: "badRequst"), messageError: true)
           case 401:
               displayMessage(message: localizedSitringFor(key: "Unauthorized"), messageError: true)
           default:
               displayMessage(message: localizedSitringFor(key: "unknownError"), messageError: true)
        }
    }
        
        
    func handleLikeReplayResponse(response: DataResponse<String>){
        switch response.response?.statusCode {
        case 200:
             displayMessage(message: localizedSitringFor(key: "success"), messageError: false)
        case 203:
            displayMessage(message: localizedSitringFor(key: "MisQuestions"), messageError: true)
        case 207:
            displayMessage(message: localizedSitringFor(key: "UserISNotActive"), messageError: true)
            delay(timeSession) {
                self.logoutRequest()
            }
        case 400:
            displayMessage(message: localizedSitringFor(key: "badRequst"), messageError: true)
        case 401:
            displayMessage(message: localizedSitringFor(key: "Unauthorized"), messageError: true)
        default:
            displayMessage(message: localizedSitringFor(key: "unknownError"), messageError: true)
        }
    }
    
    func handleDisLikeReplayResponse(response: DataResponse<String>){
        switch response.response?.statusCode {
        case 200:
            displayMessage(message: localizedSitringFor(key: "success"), messageError: false)
        case 203:
            displayMessage(message: localizedSitringFor(key: "MisQuestions"), messageError: true)
        case 207:
            displayMessage(message: localizedSitringFor(key: "UserISNotActive"), messageError: true)
            delay(timeSession) {
                self.logoutRequest()
            }
        case 400:
            displayMessage(message: localizedSitringFor(key: "badRequst"), messageError: true)
        case 401:
            displayMessage(message: localizedSitringFor(key: "Unauthorized"), messageError: true)
        default:
            displayMessage(message: localizedSitringFor(key: "unknownError"), messageError: true)
        }
    }
    
    /// uses for connecting to server and recive data
    func logoutRequest()
    {
        var parameters: [String: Any] = [:]
        parameters["userid"] = Account.shared.id
        parameters["DeviceId"] = deviceID
        
        var url = ApiNames.LOGOUT + "?userid=" + String(Account.shared.id)
        url += "&DeviceId=" + deviceID
        
        NETWORK.connectWithHeaderTo(api: url, withParameters: parameters, andIdentifier: LOGOUT, withLoader: true, forController: self, methodType: .post)
    }
    
    
    /// handle login json response from server
    ///
    /// - Parameter response: server response
    func handleLogout(forResponse response: DataResponse<String>)
    {
        switch response.response?.statusCode {
        case 200:
            Account.shared.logout()
            pushToView(withId: baseSession)
        case 400:
            displayMessage(message: localizedSitringFor(key: "invalidUserOrPassword"), messageError: true)

        case 401:
            displayMessage(message: localizedSitringFor(key: "Unauthorized"), messageError: true)
        default:
            displayMessage(message: localizedSitringFor(key: "unknownError"), messageError: true)
        }
    }

}



