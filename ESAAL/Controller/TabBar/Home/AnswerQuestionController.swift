//
//  AnswerQuestionController.swift
//  ESAAL
//
//  Created by Mina Thabet on 11/7/19.
//  Copyright © 2019 HardTask. All rights reserved.
//

import UIKit
import Alamofire
import AVKit
import AVFoundation
import ReplayKit
import SKPhotoBrowser
import IBAnimatable

class AnswerQuestionController: UIViewController {

    // MARK:- IBOutlet
    @IBOutlet weak var headerView: GradientView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var questionMediaCollectionView: UICollectionView!
    @IBOutlet weak var answerMediaCollectionView: UICollectionView!
    @IBOutlet weak var bsUbjectLabel: UILabel!
    @IBOutlet weak var questionLabel: UITextView!
    @IBOutlet weak var teacherNameLabel: UILabel!
    @IBOutlet weak var messageLabel: UITextView!
    @IBOutlet weak var bButton: UIButton!
    @IBOutlet weak var rALAbel: UILabel!
    @IBOutlet weak var rLAbel: UILabel!
    @IBOutlet weak var answerUserImageView: AnimatableImageView!
    @IBOutlet weak var questionMediaHeight: NSLayoutConstraint!
    @IBOutlet weak var r_videoRulesLabel: UILabel!
    
    ///Constant
    let NETWORK = NetworkingHelper()
    let ADD_REPLAY = "addReplay"
    let LOGOUT = "logout"

    ///Variables
    var header: HeaderView?
    var question: Question?
    var questionMediaList: [Attachement] = []
    var replayMediaList: [QuestionMedia] = []
    var videoThumblainImage = UIImage()
    
    
    /// computed variables
    var questionImage: UIImage? {
        didSet{
            if questionImage != nil {
                // check if there photo added before to rplace if not append
                var found = false
                for (index,value) in replayMediaList.enumerated() {
                    if value.isImage {
                        found = true
                        replayMediaList[index] = QuestionMedia(image: questionImage, video: nil, isImage: true)
                    }
                }
                if !found {
                    replayMediaList.append(QuestionMedia(image: questionImage, video: nil, isImage: true))
                }

                answerMediaCollectionView.reloadData()
                
            }
        }
    }
    
    var questionVideo: NSURL? {
        didSet{
            if questionVideo != nil{
                // check if there video added before to rplace if not append
                  var found = false
                  for (index,value) in replayMediaList.enumerated() {
                      if !value.isImage{
                          found = true
                          replayMediaList[index] = QuestionMedia(image: nil, video: questionVideo, isImage: false)
                      }
                  }
                  if !found {
                      replayMediaList.append(QuestionMedia(image: nil, video: questionVideo, isImage: false))

                  }
                   answerMediaCollectionView.reloadData()
            }
        }
    }


   
   //MARK:- App life cycle
   override func viewDidLoad() {
       super.viewDidLoad()
       initView()
    
       // Do any additional setup after loading the view.
   }
   
   override func viewDidAppear(_ animated: Bool) {
       super.viewDidAppear(animated)
       if header == nil{
           header = HeaderView.initView(fromController: self, andView: headerView, andTitle: localizedSitringFor(key: "replayQuestion"), isFromRoot: true)
           header?.backView.isHidden = false
       }
   }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        initMainQuestionData()
    }
    
    @IBAction func addVideoAction(_ sender: Any) {
        
        for media in replayMediaList {
               if !media.isImage {
                   displayAlert(localizedSitringFor(key: "addVideoTitle"),localizedSitringFor(key: "addVideoBody"),forController: self)
                   return
               }
        }
        AttachmentHandler.shared.showAttachmentVideoActionSheet(vc: self)
               
        AttachmentHandler.shared.videoPickedBlock = { (url) in
           let asset = AVURLAsset(url: url as URL)
           let durationInSeconds = asset.duration.seconds
           print(durationInSeconds,"video time osama")
           if durationInSeconds <= 120.0{
               self.questionVideo = url
//               self.videoThumblainImage = self.getThumbnailImageFromVideoURL(fromUrl: url as URL) ?? UIImage()
           }else {
               displayMessage(message: localizedSitringFor(key: "videoTime"), messageError: true)
               
           }
        }
    }
    
    @IBAction func addImageAction(_ sender: Any) {
        for media in replayMediaList {
              if media.isImage {
                  displayAlert(localizedSitringFor(key: "addImageTitle"),localizedSitringFor(key: "addImageBody"),forController: self)
                  return
              }
        }
        AttachmentHandler.shared.showAttachmentImageActionSheet(vc: self)
                     
        AttachmentHandler.shared.imagePickedBlock = { (image) in
            self.questionImage = image
        }
    }
    
    @IBAction func sendAction(_ sender: Any) {
        if ValidateField() {
            requestReplayQuestionApi()
        }
    }
    
}


//MARK:- Helpers
extension AnswerQuestionController {
    func initView(){
        NETWORK.deleget = self
        initCommonQuestionMediaCollectionView()
        initCommonAnswerMediaCollectionView()
        setupFonts()
        setTextViewPlaceholder()
    }
    
    
    func initCommonQuestionMediaCollectionView() {
        questionMediaCollectionView.delegate = self
        questionMediaCollectionView.dataSource = self
        questionMediaCollectionView.register(UINib(nibName: CellIdentifier.QuestionMediaCell.rawValue, bundle: nil), forCellWithReuseIdentifier: CellIdentifier.QuestionMediaCell.rawValue)
    }
    
    func initCommonAnswerMediaCollectionView() {
        answerMediaCollectionView.delegate = self
        answerMediaCollectionView.dataSource = self
        answerMediaCollectionView.register(UINib(nibName: CellIdentifier.QuestionMediaCell.rawValue, bundle: nil), forCellWithReuseIdentifier: CellIdentifier.QuestionMediaCell.rawValue)
    }

    func setupFonts(){
        mainQueue {
            self.nameLabel.font = UIFont(name: localizedSitringFor(key: "AppBoldFont"), size: iphoneXFactor*45 )
            self.bsUbjectLabel.font = UIFont(name: localizedSitringFor(key: "AppBoldFont"), size: iphoneXFactor*45 )
            self.questionLabel.font = UIFont(name: localizedSitringFor(key: "AppRegularFont"), size: iphoneXFactor*45 )
            self.teacherNameLabel.font = UIFont(name: localizedSitringFor(key: "AppBoldFont"), size: iphoneXFactor*45 )
            self.messageLabel.font = UIFont(name: localizedSitringFor(key: "AppRegularFont"), size: iphoneXFactor*40 )
            self.rLAbel.font = UIFont(name: localizedSitringFor(key: "AppRegularFont"), size: iphoneXFactor*40 )
            self.rALAbel.font = UIFont(name: localizedSitringFor(key: "AppRegularFont"), size: iphoneXFactor*40 )
            self.bButton.titleLabel?.font = UIFont(name: localizedSitringFor(key: "AppBoldFont"), size: iphoneXFactor*50)
            self.r_videoRulesLabel.font = UIFont(name: localizedSitringFor(key: "AppRegularFont"), size: iphoneXFactor*35)
            self.r_videoRulesLabel.text = localizedSitringFor(key: "videoTime")
        }
    }
    
    
    func initMainQuestionData() {
        nameLabel.text = localizedSitringFor(key: "EssalStudent")
        teacherNameLabel.text = Account.shared.isTeacher ? localizedSitringFor(key: "EssalTeatcher") : localizedSitringFor(key: "EssalStudent")
        answerUserImageView.image = UIImage(named: Account.shared.isTeacher ? "teacher" : "student")
        bsUbjectLabel.text = question?.material?.name
        questionLabel.text = question?.description
        questionMediaList = question?.attachment ?? []
        if questionMediaList.count == 0{
            questionMediaHeight.constant = -(screenWidth * 0.15)
        }
        questionMediaCollectionView.reloadData()
    }
    
    func setTextViewPlaceholder() {
       messageLabel.delegate = self
       messageLabel.text = localizedSitringFor(key: "Message")
       messageLabel.textColor = UIColor.lightGray
    }
    
    func palyVideoFromServer(videoURL: URL) {
        goToVideoPlayer(link: videoURL)
    }
    
    func goToVideoPlayer(link: URL) {
        let main = UIStoryboard(name: "Main", bundle: nil)
        let vc = main.instantiateViewController(withIdentifier: "VideoPlayerViewController") as! VideoPlayerViewController
        vc.videoLink = link
        self.present(vc, animated: true, completion: nil)
    }
    
    func deleteMediaFromCollection(index: Int) {
       if self.replayMediaList[index].isImage {
           self.replayMediaList.remove(at: index)
           self.questionImage = nil
       }else {
           self.replayMediaList.remove(at: index)
           self.questionVideo = nil
       }
       self.answerMediaCollectionView.reloadData()
    }
    
    /// validate Fields
    ///
    /// - Returns: true or false
    func ValidateField() -> Bool{
        
        if messageLabel.text?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty == true ||  messageLabel.textColor == UIColor.lightGray {
           displayMessage(message: localizedSitringFor(key: "emptyMessage"), messageError: true)
           return false
       }
        
        return true
    }
    
    func palyVideo(videoURL: NSURL) {
        let player = AVPlayer(url: videoURL as URL)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        parent?.present(playerViewController, animated: true) {
            playerViewController.player!.play()
        }
    }
    
    func getThumbnailImageFromVideoURL(fromUrl url: URL) -> UIImage? {
        let asset: AVAsset = AVAsset(url: url)
        let imageGenerator = AVAssetImageGenerator(asset: asset)
        do {
            let thumbnailImage = try imageGenerator.copyCGImage(at: CMTimeMake(value: 1, timescale: 60) , actualTime: nil)
            return UIImage(cgImage: thumbnailImage)
        } catch let error {
            print(error)
        }
        
        return nil
    }

}


//MARK:- UI Text View Delegate
extension AnswerQuestionController: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = localizedSitringFor(key: "Description")
            textView.textColor = UIColor.lightGray
        }
    }
}


//MARK:- collection view delegat and datasource
extension AnswerQuestionController: UICollectionViewDelegate,UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == questionMediaCollectionView.self {
            return questionMediaList.count
        }else {
            return replayMediaList.count
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let mediaCell = collectionView.dequeueReusableCell(withReuseIdentifier: CellIdentifier.QuestionMediaCell.rawValue, for: indexPath) as! QuestionMediaCell
        if collectionView == questionMediaCollectionView.self {
            mediaCell.configureCell(media: questionMediaList[indexPath.item])
        }else {
            mediaCell.configureCell(questionMedia: replayMediaList[indexPath.item])
        }
        
        mediaCell.delegte = self
        
        return mediaCell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == questionMediaCollectionView.self {
            if questionMediaList[indexPath.item].fileType == "i" {
                if let cell = collectionView.cellForItem(at: indexPath) as? QuestionMediaCell{
                    openPhotoLocale(image: cell.mediaImage.image ?? UIImage())
                }
//               if let image = questionMediaList[indexPath.item].fileUrl{
//                  openPhotoWithUrl(imageUrl: image)
//              }
                          
           }else if questionMediaList[indexPath.item].fileType == "v"{
               if let url = URL(string:questionMediaList[indexPath.item].fileUrl ?? ""){
                   //palyVideo(videoURL: url as NSURL)
                  palyVideoFromServer(videoURL: url)
               }
           }
            
        }else {
            if replayMediaList[indexPath.item].isImage {
                if let image = replayMediaList[indexPath.item].image {
                    openPhotoLocale(image: image)
                }
            }else{
                if let videoUrl = replayMediaList[indexPath.item].video {
                    palyVideo(videoURL: videoUrl)
                }
            }
        }
        
    }
    
    func openPhotoWithUrl(imageUrl: String) {
        let skPhoto = SKPhoto.photoWithImageURL(imageUrl)
         skPhoto.shouldCachePhotoURLImage = true
        let browser = SKPhotoBrowser(photos: [skPhoto])
        browser.initializePageIndex(0)
        present(browser, animated: true, completion: {})
    }
    
    func openPhotoLocale(image: UIImage) {
        let skPhoto = SKPhoto.photoWithImage(image)
        let browser = SKPhotoBrowser(photos: [skPhoto])
        browser.initializePageIndex(0)
        present(browser, animated: true, completion: {})
    }
    
}

//MARK:- collection view flow layout
extension AnswerQuestionController: UICollectionViewDelegateFlowLayout{

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
           return CGSize(width: collectionView.frame.size.width * 0.22, height: collectionView.frame.size.height)
       }
}


// MARK:- Media Question Media Cell Deleget
extension AnswerQuestionController: MediaQuestionMediaCellDeleget{
    func videoCaptureDidSet(videoImage: UIImage) {
        print("Capture")
    }
    
    func cancelMediaClicked(_ sender: QuestionMediaCell) {
        guard let tappedIndexPath = answerMediaCollectionView.indexPath(for: sender) else {
                   return
        }
        displayAlertConfirmation(localizedSitringFor(key: "cancelFileTitle"),localizedSitringFor(key: "cancelFileBody")
        ,forController: self) {
            self.deleteMediaFromCollection(index: tappedIndexPath.item)

        }
    }
    
}




//MARK:- Networking
extension AnswerQuestionController: NetworkingHelperDeleget {
    func onHelper(getData data: DataResponse<String>, fromApiName name: String, withIdentifier id: String) {
        if id == ADD_REPLAY { handleReplayQuestionResponse(response: data) }
        else if id == LOGOUT{ handleLogout(forResponse: data) }

    }
    
    func onHelper(getError error: String, fromApiName name: String, withIdentifier id: String) {
        displayMessage(message: localizedSitringFor(key: "unkwonError") , messageError: true)
    }
    
    // MARK:- request apis from server
    func requestReplayQuestionApi() {
        var paramters: [String: String] = [:]
       // paramters["MaterialId"] =  selectedMatrial
        paramters["RequestQuestionId"] = String(question?.id ?? 0)
        paramters["UserId"] = String(Account.shared.id)
        paramters["ReplayMessage"] =  messageLabel.text
        
        var questionImages: [String: UIImage] = [:]
        if questionImage != nil {
            questionImages["attachmentimage"] = questionImage
        }
        
        var questionVideos: [String: AnyObject] = [:]
        //var videoThumblain: [String: UIImage] = [:]
        if questionVideo != nil {
            questionVideos["attachmentvideo"] = questionVideo
            //videoThumblain["videoplaceholderimage"] = videoThumblainImage
        }
     

        NETWORK.connectToUploadObject(videos: questionVideos,toApi: ApiNames.ADD_REPLY, withParameters: paramters ,andIdentifier: ADD_REPLAY, images: questionImages)
    }
    
    
    // MARK:- handle response server
    
    func handleReplayQuestionResponse(response: DataResponse<String>){
        switch response.response?.statusCode {
        case 200:
            self.navigationController?.popViewController(animated: true)
            displayMessage(message: localizedSitringFor(key: "MessageSentSuccessfully"), messageError: false)
        case 400:
            displayMessage(message: localizedSitringFor(key: "badRequst"), messageError: true)
        case 201:
            displayMessage(message: localizedSitringFor(key: "badRequst"), messageError: true)
        case 202:
            displayMessage(message: localizedSitringFor(key: "MisMaterial"), messageError: true)
        case 203:
            displayMessage(message: localizedSitringFor(key: "UserNotFound"), messageError: true)
        case 401:
            displayMessage(message: localizedSitringFor(key: "Unauthorized"), messageError: true)
        case 207:
            displayMessage(message: localizedSitringFor(key: "UserISNotActive"), messageError: true)
            delay(timeSession) {
                self.logoutRequest()
            }
        default:
            displayMessage(message: localizedSitringFor(key: "unknownError"), messageError: true)
        }
    }
    
    /// uses for connecting to server and recive data
    func logoutRequest()
    {
        var parameters: [String: Any] = [:]
        parameters["userid"] = Account.shared.id
        parameters["DeviceId"] = deviceID
        
        var url = ApiNames.LOGOUT + "?userid=" + String(Account.shared.id)
        url += "&DeviceId=" + deviceID
        
        NETWORK.connectWithHeaderTo(api: url, withParameters: parameters, andIdentifier: LOGOUT, withLoader: true, forController: self, methodType: .post)
    }
    
    
    /// handle login json response from server
    ///
    /// - Parameter response: server response
    func handleLogout(forResponse response: DataResponse<String>)
    {
        switch response.response?.statusCode {
        case 200:
            Account.shared.logout()
            pushToView(withId: baseSession)
        case 400:
            displayMessage(message: localizedSitringFor(key: "invalidUserOrPassword"), messageError: true)

        case 401:
            displayMessage(message: localizedSitringFor(key: "Unauthorized"), messageError: true)
        default:
            displayMessage(message: localizedSitringFor(key: "unknownError"), messageError: true)
        }
    }
    
}


