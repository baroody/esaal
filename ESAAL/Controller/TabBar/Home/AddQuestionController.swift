//
//  AddQuestionController.swift
//  ESAAL
//
//  Created by Mina Thabet on 11/6/19.
//  Copyright © 2019 HardTask. All rights reserved.
//

import UIKit
import Alamofire
import AVKit
import AVFoundation
import ReplayKit
import SKPhotoBrowser

struct QuestionMedia {
    var id: Int?
    var image: UIImage?
    var imageUrl: String?
    var video: NSURL?
    var videoUrl: String?
    var videoImage: String?
    var isImage: Bool
    var isFromBackend: Bool?
}

class AddQuestionController: UIViewController {

    //MARK:- IBOutlet
    @IBOutlet weak var headerView: GradientView!
    @IBOutlet weak var subjectCollectionView: UICollectionView!
    @IBOutlet weak var mediaCollectionView: UICollectionView!
    @IBOutlet weak var messageTextView: UITextView!
    @IBOutlet weak var rLabel: UILabel!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var rvLAbel: UILabel!
    @IBOutlet weak var BButton: UIButton!
    @IBOutlet weak var r_videoRulesLabel: UILabel!
    
    
    ///constant
    let NETWORK = NetworkingHelper()
    let GET_MATRIAL = "getMatrial"
    let ADD_Question = "addQuestion"
    let DELETE_ATTACHMENT = "deleteAttacmnet"
    let LOGOUT = "logout"

    
    ///variables
    var header: HeaderView?
    var matrialList: [Matrial] = []
    var selectedMatrial = ""
    var questionMediaList: [QuestionMedia] = []
    var questionDetails: Question?
    var isUpdateQuestion = false
    var deletedIndexPath: Int?
    var videoThumblainImage = UIImage()
    
    
    /// computed variables
    var selectedMatrialIndexPath: IndexPath? {
        didSet {
            subjectCollectionView.reloadData()
            subjectCollectionView.selectItem(at: selectedMatrialIndexPath, animated: true, scrollPosition: .centeredHorizontally)
        }
    }
    
    var questionImage: UIImage? {
        didSet{
            // check if there photo added before to rplace if not append
            if questionImage != nil{
                var found = false
                for (index,value) in questionMediaList.enumerated() {
                    if value.isImage{
                        found = true
                        questionMediaList[index] = QuestionMedia(image: questionImage, video: nil, isImage: true, isFromBackend: false)
                    }
                }
                if !found {
                    questionMediaList.append(QuestionMedia(image: questionImage, video: nil, isImage: true, isFromBackend: false))
                }

                mediaCollectionView.reloadData()
            }
            
        }
    }
    
    var questionVideo: NSURL? {
        didSet{
            if questionVideo != nil{
                // check if there video added before to rplace if not append
                  var found = false
                  for (index,value) in questionMediaList.enumerated() {
                      if !value.isImage{
                          found = true
                          questionMediaList[index] = QuestionMedia(image: nil, video: questionVideo, isImage: false, isFromBackend: false)
                      }
                  }
                  if !found {
                      questionMediaList.append(QuestionMedia(image: nil, video: questionVideo, isImage: false, isFromBackend: false))

                  }
                   mediaCollectionView.reloadData()
            }
           
        }
    }
    

   
   //MARK:- App life cycle
   override func viewDidLoad() {
       super.viewDidLoad()
       initView()
       if isUpdateQuestion{
         initQuestioDataIfUpdateQuestion()
       }
       // Do any additional setup after loading the view.
   }
   
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

    }
    
   override func viewDidAppear(_ animated: Bool) {
       super.viewDidAppear(animated)
       if header == nil{
        if isUpdateQuestion {
            header = HeaderView.initView(fromController: self, andView: headerView, andTitle: localizedSitringFor(key: "updateQuestion"), isFromRoot: true)
        }else {
            header = HeaderView.initView(fromController: self, andView: headerView, andTitle: localizedSitringFor(key: "addQuestion"), isFromRoot: true)
        }
           header?.backView.isHidden = false
       }
   }
    
    @IBAction func addImageAction(_ sender: Any) {
        for media in questionMediaList {
            if media.isImage {
                displayAlert(localizedSitringFor(key: "addImageTitle"),localizedSitringFor(key: "addImageBody"),forController: self)
                return
            }
        }
        AttachmentHandler.shared.showAttachmentImageActionSheet(vc: self)
                     
        AttachmentHandler.shared.imagePickedBlock = { (image) in
            self.questionImage = image
        }
    }
    
    @IBAction func addvideoAction(_ sender: Any) {
        for media in questionMediaList {
            if !media.isImage {
                displayAlert(localizedSitringFor(key: "addVideoTitle"),localizedSitringFor(key: "addVideoBody"),forController: self)
                return
            }
        }
        AttachmentHandler.shared.showAttachmentVideoActionSheet(vc: self)
        
        AttachmentHandler.shared.videoPickedBlock = { (url) in
            let asset = AVURLAsset(url: url as URL)
            let durationInSeconds = asset.duration.seconds
            print(durationInSeconds,"video time osama")
            if durationInSeconds <= 120.0{
                self.questionVideo = url
//                self.videoThumblainImage = self.getThumbnailImageFromVideoURL(fromUrl: url as URL) ?? UIImage()
            }else {
                displayMessage(message: localizedSitringFor(key: "videoTime"), messageError: true)
                
            }
            
        }
    }
    
    @IBAction func sendAction(_ sender: Any) {
        if ValidateField() {
            if isUpdateQuestion{
                requestUpdateQuestionApi()
            }else{
               requestAddQuestionApi()
            }
        }
    }
    
}


//MARK:- Helpers
extension AddQuestionController {
    
    /// init view for first time
    func initView(){
        NETWORK.deleget = self
        requestMatrialApi()
        initCommonSubjectCollectionView()
        initCommonMediaCollectionView()
        setupFonts()
        setTextViewPlaceholder()
    }
    
    func setupFonts(){
        mainQueue {
            self.messageTextView.font = UIFont(name: localizedSitringFor(key: "AppRegularFont"), size: iphoneXFactor*45 )
            self.userName.font = UIFont(name: localizedSitringFor(key: "AppRegularFont"), size: iphoneXFactor*45 )
            self.userName.text = localizedSitringFor(key: "EssalStudent")
            self.rvLAbel.font = UIFont(name: localizedSitringFor(key: "AppRegularFont"), size: iphoneXFactor*36 )
            self.rLabel.font = UIFont(name: localizedSitringFor(key: "AppRegularFont"), size: iphoneXFactor*36 )
            self.BButton.titleLabel?.font = UIFont(name: localizedSitringFor(key: "AppBoldFont"), size: iphoneXFactor*50)
            self.r_videoRulesLabel.font = UIFont(name: localizedSitringFor(key: "AppRegularFont"), size: iphoneXFactor*35)
            self.r_videoRulesLabel.text = localizedSitringFor(key: "videoTime")
        }

    }
    
    /// init collection view
    func initCommonSubjectCollectionView() {
        subjectCollectionView.delegate = self
        subjectCollectionView.dataSource = self
        subjectCollectionView.register(UINib(nibName: CellIdentifier.StudingSubjectCell.rawValue, bundle: nil), forCellWithReuseIdentifier: CellIdentifier.StudingSubjectCell.rawValue)
    }
    
    /// init collection view
    func initCommonMediaCollectionView() {
        mediaCollectionView.delegate = self
        mediaCollectionView.dataSource = self
        mediaCollectionView.register(UINib(nibName: CellIdentifier.QuestionMediaCell.rawValue, bundle: nil), forCellWithReuseIdentifier: CellIdentifier.QuestionMediaCell.rawValue)
    }
    
    // init question mataril and message question and image or videos if update question
    func initQuestioDataIfUpdateQuestion() {
        if let question = questionDetails {
            messageTextView.text = question.description
            populateMediaListFromQuestion()
        }
    }
    
    func setMatrialFromQuestion() {
        for (index,value) in matrialList.enumerated(){
            if value.id == questionDetails?.materialId {
                selectedMatrial = String(value.id ?? 0)
                let indexPath = IndexPath(row: index, section: 0)
                subjectCollectionView.selectItem(at: indexPath, animated: true, scrollPosition: [])
            }
        }
    }
    
    func populateMediaListFromQuestion() {
        if let attachment = questionDetails?.attachment {
            for media in attachment {
                if media.fileType == "i" {
                    questionMediaList.append(QuestionMedia(id: media.id, image: nil, imageUrl: media.fileUrl ?? "", video: nil, isImage: true,isFromBackend: true))
                }else if media.fileType == "v" {
                    questionMediaList.append(QuestionMedia(id: media.id, image: nil, imageUrl: nil, video: nil,videoUrl: media.fileUrl ?? "",videoImage: media.filePlaceholderUrl ,isImage: false,isFromBackend: true))
                    
                }
            }
            mediaCollectionView.reloadData()
        }
    }
    
    
    func setTextViewPlaceholder() {
       messageTextView.delegate = self
        if !isUpdateQuestion {
            messageTextView.text = localizedSitringFor(key: "Message")
            messageTextView.textColor = UIColor.lightGray
        }
       
    }
    
    func palyVideoFromServer(videoURL: URL) {
        goToVideoPlayer(link: videoURL)
    }
    
    func goToVideoPlayer(link: URL) {
        let main = UIStoryboard(name: "Main", bundle: nil)
        let vc = main.instantiateViewController(withIdentifier: "VideoPlayerViewController") as! VideoPlayerViewController
        vc.videoLink = link
        self.present(vc, animated: true, completion: nil)
    }
    
    func openPhotoWithUrl(imageUrl: String) {
        let skPhoto = SKPhoto.photoWithImageURL(imageUrl)
         skPhoto.shouldCachePhotoURLImage = true
        let browser = SKPhotoBrowser(photos: [skPhoto])
        browser.initializePageIndex(0)
        present(browser, animated: true, completion: {})
    }
    
    func openPhotoLocale(image: UIImage) {
        let skPhoto = SKPhoto.photoWithImage(image)
        let browser = SKPhotoBrowser(photos: [skPhoto])
        browser.initializePageIndex(0)
        present(browser, animated: true, completion: {})
    }
    
    func palyVideo(videoURL: NSURL) {
        let player = AVPlayer(url: videoURL as URL)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        parent?.present(playerViewController, animated: true) {
            playerViewController.player!.play()
        }
    }
    
    func deleteMediaFromCollection(index: Int) {
       if self.questionMediaList[index].isImage {
           self.questionMediaList.remove(at: index)
           self.questionImage = nil
       }else {
           self.questionMediaList.remove(at: index)
           self.questionVideo = nil
       }
       self.mediaCollectionView.reloadData()
    }
    
    
    /// validate Fields
    ///
    /// - Returns: true or false
    func ValidateField() -> Bool{
        if selectedMatrial ==  ""{
            displayMessage(message: localizedSitringFor(key: "emptyMatrial"), messageError: true)
            return false
        }
        
        if messageTextView.text?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty == true || messageTextView.textColor == UIColor.lightGray {
           displayMessage(message: localizedSitringFor(key: "emptyQuestions"), messageError: true)
           return false
       }
        
        return true
    }
    
    func getThumbnailImageFromVideoURL(fromUrl url: URL) -> UIImage? {
        let asset: AVAsset = AVAsset(url: url)
        let imageGenerator = AVAssetImageGenerator(asset: asset)
        do {
            let thumbnailImage = try imageGenerator.copyCGImage(at: CMTimeMake(value: 1, timescale: 60) , actualTime: nil)
            return UIImage(cgImage: thumbnailImage)
        } catch let error {
            print(error)
        }
        
        return nil
    }
    
}

//MARK:- UI Text View Delegate
extension AddQuestionController: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            if !isUpdateQuestion{
                textView.text = nil
            }
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = localizedSitringFor(key: "Message")
            textView.textColor = UIColor.lightGray
        }
    }
}



//MARK:- collection view delegat and datasource
extension AddQuestionController: UICollectionViewDelegate,UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == subjectCollectionView.self{
            return  matrialList.count
        }
        return questionMediaList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == subjectCollectionView.self{
            let subjectCell = collectionView.dequeueReusableCell(withReuseIdentifier: CellIdentifier.StudingSubjectCell.rawValue, for: indexPath) as! StudingSubjectCell
                   subjectCell.configureCell(subjectName: matrialList[indexPath.item].name ?? "")
                   return subjectCell
        }
        
        let mediaCell = collectionView.dequeueReusableCell(withReuseIdentifier: CellIdentifier.QuestionMediaCell.rawValue, for: indexPath) as! QuestionMediaCell
        mediaCell.delegte = self
        mediaCell.configureCell(questionMedia: questionMediaList[indexPath.item])
        return mediaCell
       
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == subjectCollectionView.self{
            selectedMatrial =  String(matrialList[indexPath.item].id ?? 0)
            selectedMatrialIndexPath = indexPath
        }else {
            if questionMediaList[indexPath.item].isImage {
                if questionMediaList[indexPath.item].image == nil{
                    if let cell = collectionView.cellForItem(at: indexPath) as? QuestionMediaCell{
                        openPhotoLocale(image: cell.mediaImage.image ?? UIImage())
                    }
//                    if let imageUrl = questionMediaList[indexPath.item].imageUrl{
//                        openPhotoWithUrl(imageUrl: imageUrl)
//                    }
                } else{
                    if let image = questionMediaList[indexPath.item].image{
                        openPhotoLocale(image: image)
                    }
                }
            }else {
                if let videoUrl = questionMediaList[indexPath.item].video{
                     palyVideo(videoURL: videoUrl)
                }else if let videoUrl = questionMediaList[indexPath.item].videoUrl {
                    // show player online
                    print(videoUrl)
                    if let url = URL(string: videoUrl) {
                        palyVideoFromServer(videoURL: url)
                    }
                    
                }
               
            }
            
        }
        
    }
    
}

// MARK:- collection view flow layout
extension AddQuestionController: UICollectionViewDelegateFlowLayout{

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width * 0.22, height: collectionView.frame.size.height)
    }
    
}


// MARK:- Media Question Media Cell Deleget
extension AddQuestionController: MediaQuestionMediaCellDeleget{
    
    func cancelMediaClicked(_ sender: QuestionMediaCell) {
 
        guard let tappedIndexPath = mediaCollectionView.indexPath(for: sender) else {
                   return
        }
        displayAlertConfirmation(localizedSitringFor(key: "cancelFileTitle"),localizedSitringFor(key: "cancelFileBody"),forController: self) {
            if let isFromBackend = self.questionMediaList[tappedIndexPath.item].isFromBackend{
                if isFromBackend{
                    if let id = self.questionMediaList[tappedIndexPath.item].id {
                        self.deletedIndexPath = tappedIndexPath.item
                        self.requestDeleteAttachmentApi(requestAttchmentId: id)
                    }
                }else {
                    self.deleteMediaFromCollection(index: tappedIndexPath.item)
                }
             }
           
            }
        
    }
    
}


//MARK:- Networking
extension AddQuestionController: NetworkingHelperDeleget {
    func onHelper(getData data: DataResponse<String>, fromApiName name: String, withIdentifier id: String) {
        if id == ADD_Question { handleAddQuestionResponse(response: data) }
        else if id == GET_MATRIAL { handleMatrialResponse(response: data) }
        else if id == DELETE_ATTACHMENT { handleDeleteAttachmenResponse(response: data) }
        else if id == LOGOUT{ handleLogout(forResponse: data) }

        
    }
    
    func onHelper(getError error: String, fromApiName name: String, withIdentifier id: String) {
        displayMessage(message: localizedSitringFor(key: "unkwonError") , messageError: true)
    }
    
    // MARK:- request apis from server

    
    func requestMatrialApi() {
        let url = ApiNames.USER_MATERIAL + "?userid=" + String(Account.shared.id)
        NETWORK.connectWithHeaderTo(api: url ,andIdentifier: GET_MATRIAL,withLoader: true ,forController: self, methodType: .get)
    }
    
    func requestDeleteAttachmentApi(requestAttchmentId: Int) {
        var paramters: [String: String] = [:]
        paramters["requestattachmentid"] = String(requestAttchmentId)
        
        let url = ApiNames.DEALETE_QUESTIONS_ATTACHMENT + "?requestattachmentid=" + String(requestAttchmentId)
        NETWORK.connectWithHeaderTo(api: url ,withParameters: paramters, andIdentifier: DELETE_ATTACHMENT,withLoader: true ,forController: self, methodType: .post)
    }
    
    func requestAddQuestionApi() {
        var paramters: [String: String] = [:]
        paramters["MaterialId"] =  selectedMatrial
        paramters["UserId"] = String(Account.shared.id)
        paramters["Description"] =  messageTextView.text
        
        var questionImages: [String: UIImage] = [:]
        if questionImage != nil {
            questionImages["attachmentimage"] = questionImage
        }
        
        var questionVideos: [String: AnyObject] = [:]
        
        //var videoThumblain: [String: UIImage] = [:]
        if questionVideo != nil {
            questionVideos["attachmentvideo"] = questionVideo
            //videoThumblain["videoplaceholderimage"] = videoThumblainImage
        }
     
        print("paramters==\(paramters)")
        print("questionImages==\(questionImages)")
        print("questionVideos==\(questionVideos)")

        NETWORK.connectToUploadObject(videos: questionVideos,toApi: ApiNames.ADD_QUESTIONS, withParameters: paramters ,andIdentifier: ADD_Question, images: questionImages)
    }
    
    
    func requestUpdateQuestionApi() {
           var paramters: [String: String] = [:]
           paramters["RequestQuestionId"] = String(questionDetails?.id ?? 0)
           paramters["MaterialId"] =  selectedMatrial
           paramters["UserId"] = String(Account.shared.id)
           paramters["Description"] =  messageTextView.text
           
           var questionImages: [String: UIImage] = [:]
           if questionImage != nil {
               questionImages["attachmentimage"] = questionImage
           }
           
           var questionVideos: [String: AnyObject] = [:]
           //var videoThumblain: [String: UIImage] = [:]
           if questionVideo != nil {
               questionVideos["attachmentvideo"] = questionVideo
               //videoThumblain["videoplaceholderimage"] = videoThumblainImage
           }
        

        NETWORK.connectToUploadObject(videos: questionVideos,toApi: ApiNames.UPDATE_QUESTION, withParameters: paramters ,andIdentifier: ADD_Question, images: questionImages)
       }
    
    
    // MARK:- handle response server
    
    
    func handleMatrialResponse(response: DataResponse<String>) {
        
        switch response.response?.statusCode {
        case 200:
            do{
                let matrial = try JSONDecoder().decode([Matrial].self, from: response.data ?? Data())
                matrialList = matrial
                subjectCollectionView.reloadData()
                if isUpdateQuestion {
                    setMatrialFromQuestion()
                }
          }catch{
              displayMessage(message: localizedSitringFor(key: "unknownError"), messageError: true)
          }
        case 201:
            displayMessage(message: localizedSitringFor(key: "notHaveBalance"), messageError: true)

        case 202:
            displayMessage(message: localizedSitringFor(key: "MisMaterial"), messageError: true)
        case 203:
            displayMessage(message: localizedSitringFor(key: "UserNotFound"), messageError: true)
        case 400:
            displayMessage(message: localizedSitringFor(key: "badRequst"), messageError: true)
        case 207:
            displayMessage(message: localizedSitringFor(key: "UserISNotActive"), messageError: true)
            delay(timeSession) {
                self.logoutRequest()
            }
        case 401:
            displayMessage(message: localizedSitringFor(key: "Unauthorized"), messageError: true)
            
        default:
            displayMessage(message: localizedSitringFor(key: "unknownError"), messageError: true)
        }
    }
    
    func handleAddQuestionResponse(response: DataResponse<String>){
        switch response.response?.statusCode {
        case 200:
            self.navigationController?.popViewController(animated: true)
            displayMessage(message: localizedSitringFor(key: "MessageSentSuccessfully"), messageError: false)
        case 400:
            displayMessage(message: localizedSitringFor(key: "badRequst"), messageError: true)
        case 201:
            displayMessage(message: localizedSitringFor(key: "notHaveBalanceToAsk"), messageError: true)
        case 202:
            displayMessage(message: localizedSitringFor(key: "MisMaterial"), messageError: true)
        case 203:
            displayMessage(message: localizedSitringFor(key: "UserNotFound"), messageError: true)
        case 207:
            displayMessage(message: localizedSitringFor(key: "UserISNotActive"), messageError: true)
            delay(timeSession) {
                self.logoutRequest()
            }
        case 401:
            displayMessage(message: localizedSitringFor(key: "Unauthorized"), messageError: true)
        default:
            displayMessage(message: localizedSitringFor(key: "unknownError"), messageError: true)
        }
    }
    
    
    func handleDeleteAttachmenResponse(response: DataResponse<String>){
        switch response.response?.statusCode {
        case 200:
            displayMessage(message: localizedSitringFor(key: "successDelete"), messageError: false)
            if let mediaIndex = deletedIndexPath {
                deleteMediaFromCollection(index: mediaIndex)
            }
        case 201:
            displayMessage(message: localizedSitringFor(key: "attachmentNotFound"), messageError: true)
        case 400:
            displayMessage(message: localizedSitringFor(key: "badRequst"), messageError: true)
        case 401:
            displayMessage(message: localizedSitringFor(key: "Unauthorized"), messageError: true)
        case 207:
            displayMessage(message: localizedSitringFor(key: "UserISNotActive"), messageError: true)
            delay(timeSession) {
                self.logoutRequest()
            }
        default:
            displayMessage(message: localizedSitringFor(key: "unknownError"), messageError: true)
        }
    }
    
    func handleUpdateQuestionResponse(response: DataResponse<String>){
        switch response.response?.statusCode {
        case 200:
            self.navigationController?.popViewController(animated: true)
            displayMessage(message: localizedSitringFor(key: "MessageSentSuccessfully"), messageError: false)
        case 400:
            displayMessage(message: localizedSitringFor(key: "badRequst"), messageError: true)
        case 201:
            displayMessage(message: localizedSitringFor(key: "badRequst"), messageError: true)
        case 202:
            displayMessage(message: localizedSitringFor(key: "MisMaterial"), messageError: true)
        case 203:
            displayMessage(message: localizedSitringFor(key: "UserNotFound"), messageError: true)
        case 207:
            displayMessage(message: localizedSitringFor(key: "UserISNotActive"), messageError: true)
            delay(timeSession) {
                self.logoutRequest()
            }
        case 401:
            displayMessage(message: localizedSitringFor(key: "Unauthorized"), messageError: true)
        default:
            displayMessage(message: localizedSitringFor(key: "unknownError"), messageError: true)
        }
    }
    
    /// uses for connecting to server and recive data
    func logoutRequest()
    {
        var parameters: [String: Any] = [:]
        parameters["userid"] = Account.shared.id
        parameters["DeviceId"] = deviceID
        
        var url = ApiNames.LOGOUT + "?userid=" + String(Account.shared.id)
        url += "&DeviceId=" + deviceID
        
        NETWORK.connectWithHeaderTo(api: url, withParameters: parameters, andIdentifier: LOGOUT, withLoader: true, forController: self, methodType: .post)
    }
    
    
    /// handle login json response from server
    ///
    /// - Parameter response: server response
    func handleLogout(forResponse response: DataResponse<String>)
    {
        switch response.response?.statusCode {
        case 200:
            Account.shared.logout()
            pushToView(withId: baseSession)
        case 400:
            displayMessage(message: localizedSitringFor(key: "invalidUserOrPassword"), messageError: true)

        case 401:
            displayMessage(message: localizedSitringFor(key: "Unauthorized"), messageError: true)
        default:
            displayMessage(message: localizedSitringFor(key: "unknownError"), messageError: true)
        }
    }
}

