//
//  PaymentHistoryController.swift
//  ESAAL
//
//  Created by Mina Thabet on 11/6/19.
//  Copyright © 2019 HardTask. All rights reserved.
//

import UIKit
import Alamofire

class PaymentHistoryController: UIViewController {
    
    //MARK:- IBOutlet
    @IBOutlet weak var headerView: GradientView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var addPackageButton: UIButton!
    
    //MARK:- Constant
    let NETWORK = NetworkingHelper()
    let LOGOUT = "logout"

    ///variables
    var header: HeaderView?
    var subscriptios: [UserSubscribtion] = []

    
    //MARK:- App life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()

        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if header == nil{
            header = HeaderView.initView(fromController: self, andView: headerView, andTitle: localizedSitringFor(key: "myPackege"), isFromRoot: true)
            header?.backView.isHidden = false
        }
    }
    
    @IBAction func addPackageAction() {
        goNavigation(controllerId: "StudentRegisterPackageController", controller: self)
    }
}


//MARK:- Helpers
extension PaymentHistoryController{
    func initView(){
        initCommonTable()
        requestUserSubscriptionsApi()
        NETWORK.deleget = self
        
        mainQueue {
            self.addPackageButton.titleLabel?.font = UIFont(name: localizedSitringFor(key: "AppBoldFont"), size: iphoneXFactor*50)
        }
    }
    
    func initCommonTable() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.estimatedRowHeight = 100
        tableView.register(UINib(nibName: CellIdentifier.PaymentHistoryCell.rawValue, bundle: nil), forCellReuseIdentifier: CellIdentifier.PaymentHistoryCell.rawValue)
    }
}

//MARK:- Table view deleget and datasource
extension PaymentHistoryController: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return subscriptios.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier.PaymentHistoryCell.rawValue, for: indexPath) as! PaymentHistoryCell
        cell.initCell(withSub: subscriptios[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}


//MARK:- Networking
extension PaymentHistoryController: NetworkingHelperDeleget {
    func onHelper(getData data: DataResponse<String>, fromApiName name: String, withIdentifier id: String) {
        if id == LOGOUT{ handleLogout(forResponse: data) }
        else{
            handleUserSubscriptionsResponse(forResponse: data)
        }
    }
    
    func onHelper(getError error: String, fromApiName name: String, withIdentifier id: String) {
        displayMessage(message: localizedSitringFor(key: "unknownError"), messageError: true)
    }
    
    // MARK:- request apis from server
        /// uses for connecting to server and recive data
        func requestUserSubscriptionsApi()
        {
            
            NETWORK.connectWithHeaderTo(api: ApiNames.GET_USER_SUBSCRIBTIONS+"?userid=\(Account.shared.id)", withLoader: true ,forController: self, methodType: .get)
        }
        
        
    // MARK:- handle response server
        /// handle  json response from server
        ///
        /// - Parameter response: server response
        func handleUserSubscriptionsResponse(forResponse response: DataResponse<String>)
        {
            switch response.response?.statusCode {
            case 200:
                 do{
                      let subs = try JSONDecoder().decode([UserSubscribtion].self, from: response.data ?? Data())
                    self.subscriptios = subs
                    mainQueue {
                        self.tableView.reloadData()
                    }
                }catch let error{
                  print("error", error)
                    displayMessage(message: localizedSitringFor(key: "unknownError"), messageError: true)
                }
            case 203:
                displayMessage(message: localizedSitringFor(key: "UserNotFound"), messageError: true)
            case 204:
                displayMessage(message: localizedSitringFor(key: "userNotHaveAubscriptions"), messageError: true)
            case 207:
                displayMessage(message: localizedSitringFor(key: "UserISNotActive"), messageError: true)
                delay(timeSession) {
                    self.logoutRequest()
                }
            case 400:
                displayMessage(message: localizedSitringFor(key: "badRequst"), messageError: true)
            case 401:
                displayMessage(message: localizedSitringFor(key: "Unauthorized"), messageError: true)
            default:
                displayMessage(message: localizedSitringFor(key: "unknownError"), messageError: true)
            }
        }
    
    /// uses for connecting to server and recive data
    func logoutRequest()
    {
        var parameters: [String: Any] = [:]
        parameters["userid"] = Account.shared.id
        parameters["DeviceId"] = deviceID
        
        var url = ApiNames.LOGOUT + "?userid=" + String(Account.shared.id)
        url += "&DeviceId=" + deviceID
        
        NETWORK.connectWithHeaderTo(api: url, withParameters: parameters, andIdentifier: LOGOUT, withLoader: true, forController: self, methodType: .post)
    }
    
    
    /// handle login json response from server
    ///
    /// - Parameter response: server response
    func handleLogout(forResponse response: DataResponse<String>)
    {
        switch response.response?.statusCode {
        case 200:
            Account.shared.logout()
            pushToView(withId: baseSession)
        case 400:
            displayMessage(message: localizedSitringFor(key: "invalidUserOrPassword"), messageError: true)

        case 401:
            displayMessage(message: localizedSitringFor(key: "Unauthorized"), messageError: true)
        default:
            displayMessage(message: localizedSitringFor(key: "unknownError"), messageError: true)
        }
    }

}
