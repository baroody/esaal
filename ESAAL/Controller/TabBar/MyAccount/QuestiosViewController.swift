//
//  QuestiosViewController.swift
//  ESAAL
//
//  Created by YoussefRomany on 11/18/19.
//  Copyright © 2019 HardTask. All rights reserved.
//

import UIKit
import Alamofire


class QuestiosViewController: UIViewController {

    //MARK:- IBOutlet
    @IBOutlet weak var headerView: GradientView!
    @IBOutlet weak var tableView: UITableView!
    
    //MARK:- Constant
    let NETWORK = NetworkingHelper()
    let GET_USER_QUESTION = "getUserQuestion"
    let GET_USER_QUESTION_BY_MATERIALS = "getUserQuestionByMaterials"
    let GET_USER_SEARCH_QUESTIONS = "getUserSearchQuestion"
    let LOGOUT = "logout"


    ///variables
    var header: HeaderView?
    var questionList: [Question] = []
    var searchQuestionList: [Question] = []
    var page = 1
    var searchPage = 1
    var selectedMaterialId:Int?
    var selectedMatrial: [String] = []
    var searchText = ""
    var isFilter = false
    var isSearch = false

    
    //MARK:- App life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if header == nil{
            header = HeaderView.initView(fromController: self, andView: headerView, andTitle: localizedSitringFor(key: "A & Q"), isFromRoot: true)
            
            header?.searchBar.isHidden = false
            header?.backView.isHidden = false
            header?.filterView.isHidden = false
            header?.searchView.isHidden = false
            header?.deleget = self
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let filterVC = segue.destination as? FilterViewController{
            filterVC.filterDeleget = self
            if selectedMatrial.count > 0{
                filterVC.selectedMatrial = selectedMatrial
            }
        }
    }
}



//MARK:- Helpers
extension QuestiosViewController{
    func initView(){
        initCommonTable()
        NETWORK.deleget = self
        requestUserQuestionsApi()

    }
    
    func initCommonTable() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.estimatedRowHeight = 100
        tableView.register(UINib(nibName: CellIdentifier.TeacherHomeTableViewCell.rawValue, bundle: nil), forCellReuseIdentifier: CellIdentifier.TeacherHomeTableViewCell.rawValue)
    }
    
    /// get next page for the list
    ///
    /// - Parameter index: the current cell indexPath
    func getNextPageIfNeeded(fromIndex index: IndexPath) {
        let count = isSearch ? searchQuestionList.count : questionList.count
        if index.row == count - 1 {
            if isSearch {
                searchPage += 1
                requestGetUserSearchQuestionApi()
            }else if isFilter {
                page += 1
                requestUserQuestionsByMaterialsApi()
            }
            else {
                page += 1
                requestUserQuestionsApi()
            }
            
        }
    }
    
    /// get matrials ids form selected tags
    func getMatrialsIds() -> String {
        var ids = ""
        for (index,id) in selectedMatrial.enumerated() {
            if index == selectedMatrial.count - 1 {
                ids += id
            }else {
                ids += id + ","
            }
            
        }
        return ids
    }

    
}

//MARK:- Table view deleget and datasource
extension QuestiosViewController: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return isSearch ? searchQuestionList.count : questionList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let questionCell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier.TeacherHomeTableViewCell.rawValue, for: indexPath) as! TeacherHomeTableViewCell
        questionCell.configureCell(question: isSearch ? searchQuestionList[indexPath.item] : questionList[indexPath.item])
        getNextPageIfNeeded(fromIndex: indexPath)
        return questionCell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return screenWidth * 0.35

    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if questionList.count > 0 {
           let main = UIStoryboard(name: "Main", bundle: nil)
           let vc = main.instantiateViewController(withIdentifier: "QuestionAndReplayController") as! QuestionAndReplayController
            vc.questionId = (isSearch ? searchQuestionList[indexPath.item].id ?? 0: questionList[indexPath.item].id ?? 0 )
           self.navigationController?.pushViewController(vc, animated: true)
       }
    }
    
}


//MARK:- Header view deleget
extension QuestiosViewController: HeaderViewDelegate{
    func filterActionClicked() {
        performSegue(withIdentifier: "showFilter", sender: nil)
    }
    
    func searchActionClicked() {
        print("searchClicked")
        mainQueue {
            self.header?.searchBar.isHidden = false
            self.header?.headerLabel.isHidden = true
            self.header?.filterView.isHidden = true
            self.header?.searchView.isHidden = true
        }
        UIView.animate(withDuration: 0.2) {
            self.header?.searchBar.alpha = 1
        }
        header?.searchBar.becomeFirstResponder()
        header?.searchBar.delegate = self
    }
}

//MARK:- Filter Selected Matrial Deleget
extension QuestiosViewController: FilterSelectedMatrialDeleget{
    func selectedMatrialClicked(selectedMatrials: [String]) {
        selectedMatrial = selectedMatrials
        page = 1
        questionList = []
        if selectedMatrials.count == 0 {
            isFilter = false
            requestUserQuestionsApi()
        }else {
            isFilter = true
            requestUserQuestionsByMaterialsApi()
        }
        
    }
}


//MARK:- UI Search Bar Delegate
extension QuestiosViewController: UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        isSearch = true
        searchText = searchBar.text ?? ""
        searchPage = 1
        searchQuestionList = []
        if searchText != ""{
            searchBar.resignFirstResponder()
            requestGetUserSearchQuestionApi()
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        UIView.animate(withDuration: 0.2) {
            self.header?.searchBar.alpha = 0
        }
        mainQueue {
            self.header?.searchBar.isHidden = true
            self.header?.headerLabel.isHidden = false
            self.header?.filterView.isHidden = false
            self.header?.searchView.isHidden = false
        }
        searchText = ""
        isSearch = false
        searchBar.resignFirstResponder()
        tableView.reloadData()
    }
}


//MARK:- Networking
extension QuestiosViewController: NetworkingHelperDeleget {
    func onHelper(getData data: DataResponse<String>, fromApiName name: String, withIdentifier id: String) {
        if id == GET_USER_QUESTION { handleGetQuestionResponse(forResponse: data) }
        else if id == GET_USER_QUESTION_BY_MATERIALS { handleGetQuestionByMaterialsResponse(forResponse: data) }
        else if id == GET_USER_SEARCH_QUESTIONS { handleGetUserSearchQuestionResponse(forResponse: data) }
        else if id == LOGOUT{ handleLogout(forResponse: data) }

    }
    
    func onHelper(getError error: String, fromApiName name: String, withIdentifier id: String) {
        displayMessage(message: localizedSitringFor(key: "unknownError"), messageError: true)
    }
    
    // MARK:- request apis from server
    /// uses for connecting to server and recive data
    func requestUserQuestionsApi(){
        
        var paramters: [String:Any] = [:]
        paramters["userid"] = Account.shared.id
        paramters["pageNum"] = page
        
        let urlData = "?userid=" + String(Account.shared.id) + "&pageNum=" + String(page)
        
        NETWORK.connectWithHeaderTo(api: ApiNames.GET_USER_QUESTIONS + urlData, andIdentifier: GET_USER_QUESTION, withLoader: true, forController: self, methodType: .get)
    }
    
    func requestUserQuestionsByMaterialsApi() {
       var paramters: [String:Any] = [:]
       paramters["userid"] = Account.shared.id
       paramters["materialid"] = getMatrialsIds()//selectedMaterialId
       paramters["pageNum"] = page
       
        print(paramters)
        
       let urlData = "?userid=" + String(Account.shared.id) + "&materialid=" + getMatrialsIds() + "&pageNum=" + String(page)
       
        NETWORK.connectWithHeaderTo(api: ApiNames.GET_USER_QUESTIONS_BY_MATERIALS + urlData, andIdentifier: GET_USER_QUESTION_BY_MATERIALS, withLoader: true, forController: self, methodType: .get)
    }
    
    func requestGetUserSearchQuestionApi() {
       let strSearch = searchText.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
       var paramters: [String:Any] = [:]
       paramters["userid"] = Account.shared.id
       paramters["strSearch"] = strSearch
       paramters["pageNum"] = searchPage
       
       let urlData = "?userid=" + String(Account.shared.id) + "&strSearch=" + strSearch + "&pageNum=" + String(searchPage)
       
        NETWORK.connectWithHeaderTo(api: ApiNames.GET_USER_SEARCH_QUESTIONS + urlData, andIdentifier: GET_USER_SEARCH_QUESTIONS, withLoader: true, forController: self, methodType: .get)
    }
    
    
    // MARK:- handle response server
    /// handle  json response from server
    ///
    /// - Parameter response: server response
    func handleGetQuestionResponse(forResponse response: DataResponse<String>)
    {
        switch response.response?.statusCode {
        case 200:

              do{
                    let questions = try JSONDecoder().decode([Question].self, from: response.data ?? Data())
                    questionList += questions
                    if questions.count == 0 &&  questionList.count == 0{
                        displayMessage(message: localizedSitringFor(key: "emptyQuestion"), messageError: false)
                        return
                    }else if questions.count == 0{
                        return
                    }
                    tableView.reloadData()
            }catch let error{
               print(error)
                displayMessage(message: localizedSitringFor(key: "unknownError"), messageError: true)
            }
            
        case 203:
            displayMessage(message: localizedSitringFor(key: "UserNotFound"), messageError: true)
        case 201:
            displayMessage(message: localizedSitringFor(key: "UserNotHaveQuestions"), messageError: true)
        case 207:
            displayMessage(message: localizedSitringFor(key: "UserISNotActive"), messageError: true)
            delay(timeSession) {
                self.logoutRequest()
            }
        case 400:
            displayMessage(message: localizedSitringFor(key: "badRequst"), messageError: true)
        case 401:
            displayMessage(message: localizedSitringFor(key: "Unauthorized"), messageError: true)
        default:
            displayMessage(message: localizedSitringFor(key: "unknownError"), messageError: true)
        }
    }
    
    func handleGetQuestionByMaterialsResponse(forResponse response: DataResponse<String>)
    {
        switch response.response?.statusCode {
        case 200:

              do{
                  let questions = try JSONDecoder().decode([Question].self, from: response.data ?? Data())
                  questionList += questions
                  if questions.count == 0 && questionList.count == 0{
                      displayMessage(message: localizedSitringFor(key: "emptyResult"), messageError: false)
                      tableView.reloadData()
                      return
                  }else if questions.count == 0{
                      return
                  }
                  tableView.reloadData()
            }catch let error{
               print(error)
                displayMessage(message: localizedSitringFor(key: "unknownError"), messageError: true)
            }
            
        case 203:
            displayMessage(message: localizedSitringFor(key: "UserNotFound"), messageError: true)
        case 201:
            questionList = []
            tableView.reloadData()
            displayMessage(message: localizedSitringFor(key: "UserNotHaveQuestions"), messageError: true)
        case 207:
            displayMessage(message: localizedSitringFor(key: "UserISNotActive"), messageError: true)
            delay(timeSession) {
                self.logoutRequest()
            }
        case 400:
            displayMessage(message: localizedSitringFor(key: "badRequst"), messageError: true)
        case 401:
            displayMessage(message: localizedSitringFor(key: "Unauthorized"), messageError: true)
        default:
            displayMessage(message: localizedSitringFor(key: "unknownError"), messageError: true)
        }
    }
    
    
    func handleGetUserSearchQuestionResponse(forResponse response: DataResponse<String>)
    {
        switch response.response?.statusCode {
        case 200:

              do{
                  let questions = try JSONDecoder().decode([Question].self, from: response.data ?? Data())
                searchQuestionList += questions
                if questions.count == 0 && searchQuestionList.count == 0{
                    displayMessage(message: localizedSitringFor(key: "emptyResult"), messageError: false)
                    tableView.reloadData()
                    return
                }else if questions.count == 0{
                    return
                }
                  tableView.reloadData()
            }catch let error{
               print(error)
                displayMessage(message: localizedSitringFor(key: "unknownError"), messageError: true)
            }
            
        case 203:
            displayMessage(message: localizedSitringFor(key: "UserNotFound"), messageError: true)
        case 201:
            searchQuestionList = []
            tableView.reloadData()
            displayMessage(message: localizedSitringFor(key: "UserNotHaveQuestions"), messageError: true)
        case 207:
            displayMessage(message: localizedSitringFor(key: "UserISNotActive"), messageError: true)
            delay(timeSession) {
                self.logoutRequest()
            }
        case 400:
            displayMessage(message: localizedSitringFor(key: "badRequst"), messageError: true)
        case 401:
            displayMessage(message: localizedSitringFor(key: "Unauthorized"), messageError: true)
        default:
            displayMessage(message: localizedSitringFor(key: "unknownError"), messageError: true)
        }
    }
    
    /// uses for connecting to server and recive data
     func logoutRequest()
     {
         var parameters: [String: Any] = [:]
         parameters["userid"] = Account.shared.id
         parameters["DeviceId"] = deviceID
         
         var url = ApiNames.LOGOUT + "?userid=" + String(Account.shared.id)
         url += "&DeviceId=" + deviceID
         
         NETWORK.connectWithHeaderTo(api: url, withParameters: parameters, andIdentifier: LOGOUT, withLoader: true, forController: self, methodType: .post)
     }
     
     
     /// handle login json response from server
     ///
     /// - Parameter response: server response
     func handleLogout(forResponse response: DataResponse<String>)
     {
         switch response.response?.statusCode {
         case 200:
             Account.shared.logout()
             pushToView(withId: baseSession)
         case 400:
             displayMessage(message: localizedSitringFor(key: "invalidUserOrPassword"), messageError: true)

         case 401:
             displayMessage(message: localizedSitringFor(key: "Unauthorized"), messageError: true)
         default:
             displayMessage(message: localizedSitringFor(key: "unknownError"), messageError: true)
         }
     }
}
