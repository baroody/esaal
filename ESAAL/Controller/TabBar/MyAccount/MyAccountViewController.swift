//
//  MyAccountViewController.swift
//  ESAAL
//
//  Created by Mina Thabet on 11/4/19.
//  Copyright © 2019 HardTask. All rights reserved.
//

import UIKit
import Alamofire

struct AccountList{
    var id: Int
    var image:String
    var label:String
    var segue:String
}

class MyAccountViewController: UIViewController {

    //MARK:- IBOutlet
    @IBOutlet var headerView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    //MARK:- Contsant
    let NETWORK = NetworkingHelper()
    let LOGOUT = "logout"
    let APP_SETTING = "appSetting"
    
    //MARK:- Variables
    var header: HeaderView!
    var headerIsLoaded = false
    var myAccountList: [AccountList] = []
    var showPayment = Account.shared.showPayment
    
    //MARK:- App life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        showPayment = Account.shared.showPayment
        if Account.shared.id != 0 {
            populateAccountList()
        }
        
        print("indsid account",Account.shared.showPayment)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        if !headerIsLoaded{
            header = HeaderView.initView(fromController: self, andView: headerView, andTitle: localizedSitringFor(key: "MyAccount"), isFromRoot: true)
            headerIsLoaded = true
        }
        
    }
}


//MARK:- helpers
extension MyAccountViewController{
    func initView(){
        if Account.shared.id != 0 {
            initCommonCollection()
//            populateAccountList()
            NETWORK.deleget = self
        }
    }
    
    func initCommonCollection(){
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(UINib(nibName: CellIdentifier.AccountSeetingCell.rawValue, bundle: nil), forCellWithReuseIdentifier: CellIdentifier.AccountSeetingCell.rawValue)
        collectionView.register(UINib(nibName: CellIdentifier.MyAccountCenterCell.rawValue, bundle: nil), forCellWithReuseIdentifier: CellIdentifier.MyAccountCenterCell.rawValue)

    }
    
    func populateAccountList(){
        myAccountList.removeAll()
        myAccountList.append(AccountList(id: 1, image: "user_icon", label: localizedSitringFor(key:"EditAccount"), segue: Account.shared.isTeacher ?  "showEditTeacher" : "showEditStudent"))
        myAccountList.append(AccountList(id: 2, image: "accountQuestion", label: localizedSitringFor(key:"QuestionAndAnswer"), segue: "showQuestions"))
        if showPayment {
            myAccountList.append(AccountList(id: 3, image: "payment_icon", label: Account.shared.isTeacher ? localizedSitringFor(key:"myBalance") : localizedSitringFor(key:"myPackege") , segue: Account.shared.isTeacher ? "showMyBalance" : "showPaymentHistory"))
        }

        myAccountList.append(AccountList(id: 4, image: "logout", label: localizedSitringFor(key: "Logout"), segue: "LoginViewController"))
        collectionView.reloadData()
    }
}


//MARK:- collection view delegat and datasource
extension MyAccountViewController: UICollectionViewDelegate,UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        myAccountList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if !showPayment {
            if indexPath.row == 2 {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CellIdentifier.MyAccountCenterCell.rawValue, for: indexPath) as! MyAccountCenterCell
                cell.configureCellWith(accountList: myAccountList[indexPath.item])
                return cell
               
            }
        }

        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CellIdentifier.AccountSeetingCell.rawValue, for: indexPath) as! AccountSeetingCell
        cell.configureCell(accountList: myAccountList[indexPath.item])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if myAccountList[indexPath.item].id == 4 {
            logoutRequest()
        }else {
            performSegue(withIdentifier: myAccountList[indexPath.item].segue, sender: nil)
        }
    }
    
}

//MARK:- collection view flow layout
extension MyAccountViewController: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if !showPayment {
            if indexPath.row == 2{
                return CGSize(width: (screenWidth) - (iphoneXFactor * 45) , height: (screenWidth / 1.85) - (iphoneXFactor * 45))
            }
        }

        return CGSize(width: (screenWidth / 2) - (iphoneXFactor * 45) , height: (screenWidth / 1.85) - (iphoneXFactor * 45))
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        print((screenWidth * 1.00) / 1080.0,"albert")
        return UIEdgeInsets(top: 20, left: iphoneXFactor * 45 , bottom: 0, right: iphoneXFactor * 45 )
    }
}


//MARK:- Networking
extension MyAccountViewController: NetworkingHelperDeleget{
    func onHelper(getData data: DataResponse<String>, fromApiName name: String, withIdentifier id: String) {
        if id == LOGOUT{ handleLogout(forResponse: data) }
    }
    
    func onHelper(getError error: String, fromApiName name: String, withIdentifier id: String) {
        displayMessage(message: localizedSitringFor(key: "unknownError"), messageError: true)
    }
    
    
        /// uses for connecting to server and recive data
        func logoutRequest()
        {
            var parameters: [String: Any] = [:]
            parameters["userid"] = Account.shared.id
            parameters["DeviceId"] = deviceID
            
            var url = ApiNames.LOGOUT + "?userid=" + String(Account.shared.id)
            url += "&DeviceId=" + deviceID
            
            NETWORK.connectWithHeaderTo(api: url, withParameters: parameters, andIdentifier: LOGOUT, withLoader: true, forController: self, methodType: .post)
        }
        
        
        /// handle login json response from server
        ///
        /// - Parameter response: server response
        func handleLogout(forResponse response: DataResponse<String>)
        {
            switch response.response?.statusCode {
            case 200:
                Account.shared.logout()
                pushToView(withId: "LoginViewController")
            case 400:
                displayMessage(message: localizedSitringFor(key: "invalidUserOrPassword"), messageError: true)

            case 401:
                displayMessage(message: localizedSitringFor(key: "Unauthorized"), messageError: true)
            default:
                displayMessage(message: localizedSitringFor(key: "unknownError"), messageError: true)
            }
        }

}
