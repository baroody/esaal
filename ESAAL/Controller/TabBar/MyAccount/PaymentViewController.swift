//
//  PaymentViewController.swift
//  ESAAL
//
//  Created by YoussefRomany on 11/19/19.
//  Copyright © 2019 HardTask. All rights reserved.
//

import UIKit
import WebKit

class PaymentViewController: UIViewController {

     @IBOutlet var headerView: GradientView!
     @IBOutlet var webView: WKWebView!
        
        var urlString = ""
        var header: HeaderView?

        override func viewDidLoad() {
            super.viewDidLoad()

            let finalOne = urlString.replacingOccurrences(of: "\"", with: "")
            if let url = URL(string: finalOne) {
                print(finalOne)
              webView.load(URLRequest(url: url))
            }
            webView.navigationDelegate = self
            webView.uiDelegate = self

        }
        
        override func viewDidAppear(_ animated: Bool) {
             super.viewDidAppear(animated)
             if header == nil{
                 header = HeaderView.initView(fromController: self, andView: headerView, andTitle: localizedSitringFor(key: "Payment"), isFromRoot: true)
                 header?.backView.isHidden = false
             }
         }

    }



extension PaymentViewController: WKUIDelegate{
//     func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
//        print("Finished navigating to url \(webView.url)");
//    }
}

extension PaymentViewController: WKNavigationDelegate{
    func webViewWebContentProcessDidTerminate(_ webView: WKWebView) {
        print(#function)
    }
    func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!) {
        print("didCommit==\(#function)")
    }
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print("didFinish==\(#function)")
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: ((WKNavigationActionPolicy) -> Void)) {
      let urli = navigationAction.request.url
        print(urli?.absoluteString ?? "m", "mmmmmmmmmmmmmmmm")
        if let urlString = urli?.absoluteString{
            if (urlString.containsIgnoringCase(find: "PaymentConfermation")) {
                print("succcccccces")
                Account.shared.isSubscribe = true
                Account.shared.storeData()
                goToView(withId: "TabBarViewController")
                displayMessage(message: localizedSitringFor(key: "paymentComplete"), messageError: false)
            }else if (urlString.containsIgnoringCase(find: "/Error")){
                dismiss(animated: true) {
                    displayMessage(message: localizedSitringFor(key: "notComplete"), messageError: true)
                }
            }
        }
// 4508750015741019

        
        if let url = navigationAction.request.url {
            print(url.absoluteString)
        }
        decisionHandler(.allow)
    }

    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        print(#function)
    }
    func webView(_ webView: WKWebView, didReceiveServerRedirectForProvisionalNavigation navigation: WKNavigation!) {
        print(#function)
    }
}


//extension String {
//
//    func containsWithoutIgnoringCase(find: String) -> Bool{
//        return self.range(of: find) != nil
//    }
//
//    func containsIgnoringCase(find: String) -> Bool{
//        return self.range(of: find, options: .caseInsensitive) != nil
//    }
//
//    func haveTextInField() -> Bool {
//        if (self.count>=1) {
//            return true
//        }
//
//        return false
//    }
//
//    func capitalizingFirstLetter() -> String {
//        return prefix(1).uppercased() + self.lowercased().dropFirst()
//    }
//
//    mutating func capitalizeFirstLetter() {
//        self = self.capitalizingFirstLetter()
//    }
//}
//

public func addRedDotAtTabBarItemIndex(index: Int, parent: UIViewController, isShow: Bool) {
    for subview in parent.tabBarController!.tabBar.subviews {

        if let subview = subview as? UIView {

            if subview.tag == 1234 {
                subview.removeFromSuperview()
                break
            }
        }
    }
    
    if isShow{
        
    let RedDotRadius: CGFloat = 5
    let RedDotDiameter = RedDotRadius * 2
    
    let TopMargin:CGFloat = 5
    
    let TabBarItemCount = CGFloat(parent.tabBarController!.tabBar.items!.count)
    
    let screenSize = UIScreen.main.bounds
    let HalfItemWidth = (screenSize.width) / (TabBarItemCount * 2)
    
    let  xOffset = HalfItemWidth * CGFloat(index * 2 + 1)
    
    let imageHalfWidth: CGFloat = (parent.tabBarController!.tabBar.items![index] as! UITabBarItem).selectedImage!.size.width / 2
    
    let redDot = UIView(frame: CGRect(x: xOffset + imageHalfWidth - 7, y: TopMargin, width: RedDotDiameter, height: RedDotDiameter))
    
    redDot.tag = 1234
    if redDot.backgroundColor != UIColor.red && isShow{
        print("reeeeed")
        redDot.backgroundColor = isShow ? UIColor.red : UIColor.clear
        redDot.isHidden = false

    }else{
        print("cleeeer")
//        redDot.backgroundColor = UIColor.clear
        redDot.isHidden = true

    }
    redDot.layer.cornerRadius = RedDotRadius
    
    parent.tabBarController?.tabBar.addSubview(redDot)
    }
    
}
