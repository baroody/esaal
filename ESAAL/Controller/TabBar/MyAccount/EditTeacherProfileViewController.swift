//
//  EditTeacherProfileViewController.swift
//  ESAAL
//
//  Created by YoussefRomany on 11/9/19.
//  Copyright © 2019 HardTask. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import TagListView
import Alamofire
import DropDown


class EditTeacherProfileViewController: UIViewController {
    
    //MARK:- IBOutlet
    @IBOutlet var firstNameTextField: SkyFloatingLabelTextField!
    @IBOutlet var midleNameTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var lastNameTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var phoneTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var emailTextField: SkyFloatingLabelTextField!
    @IBOutlet var countryName: SkyFloatingLabelTextField!
    @IBOutlet var countryView: UIView!
    @IBOutlet weak var banckNameTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var accountNumberTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var swiftCodeTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var homeAddressTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var banckAddressView: SkyFloatingLabelTextField!
    @IBOutlet weak var userNameTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var IBANTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet var registerButton: UIButton!
    @IBOutlet weak var tagList: TagListView!
    @IBOutlet var Blabel: UILabel!
    @IBOutlet var BlabelD: UILabel!
    @IBOutlet var headerView: UIView!
    
    //MARK:- Constant
    let NETWORK = NetworkingHelper()
    let countryDropDown = DropDown()
    let GET_COUNTRY = "getCountry"
    let GET_MATRIAL = "getMatrial"
    let GET_ALL_MATRIAL = "getAllMatrial"
    let GET_USER_DATA = "getUserData"
    let UPDATE_TEACH = "updateTeach"
    let LOGOUT = "logout"

    //MARK:- Variables
    var header: HeaderView?
    var matrialList: [Matrial] = []
    var tags: [String] = []
    var selectedTag: [String] = []
    var countryList: [Country] = []
    var selectedCountry: Country?
    var isGulf = false
    var currentUser: User!
    
    
    //MARK:- App life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        tagList.delegate = self
        
        if header == nil{
            header = HeaderView.initView(fromController: self, andView: headerView, andTitle: localizedSitringFor(key: "EditAccount"), isFromRoot: true)
            header?.backView.isHidden = false
            
        }
    }
    
    //MARK:- IBAction
    @IBAction func showCountriesAction(_ sender: Any) {
        countryDropDown.show()
    }
    
    @IBAction func registerAction(_ sender: Any) {
        if ValidateField() {
            requestUpdateTeacherApi()
        }
    }
    
}

//MARK:- Helpers
extension EditTeacherProfileViewController{
    
    /// init view fro first time
    func initView() {
        NETWORK.deleget = self
        initTagsView()
        initDropDown()
        requestCountryApi()
        requestAllMatrialApi()
        setupFonts()
       }
       /// init fonts depend on language
       func setupFonts(){
           mainQueue {
            self.userNameTextField.font = UIFont(name: localizedSitringFor(key: "AppRegularFont"), size: iphoneXFactor*45 )
           self.firstNameTextField.font = UIFont(name: localizedSitringFor(key: "AppRegularFont"), size: iphoneXFactor*45 )
           self.midleNameTextField.font = UIFont(name: localizedSitringFor(key: "AppRegularFont"), size: iphoneXFactor*45 )
           self.lastNameTextField.font = UIFont(name: localizedSitringFor(key: "AppRegularFont"), size: iphoneXFactor*45 )
           self.emailTextField.font = UIFont(name: localizedSitringFor(key: "AppRegularFont"), size: iphoneXFactor*45 )
           self.countryName.font = UIFont(name: localizedSitringFor(key: "AppRegularFont"), size: iphoneXFactor*45 )
           self.banckNameTextField.font = UIFont(name: localizedSitringFor(key: "AppRegularFont"), size: iphoneXFactor*45 )
           self.accountNumberTextField.font = UIFont(name: localizedSitringFor(key: "AppRegularFont"), size: iphoneXFactor*45 )
           self.swiftCodeTextField.font = UIFont(name: localizedSitringFor(key: "AppRegularFont"), size: iphoneXFactor*45 )
           self.homeAddressTextField.font = UIFont(name: localizedSitringFor(key: "AppRegularFont"), size: iphoneXFactor*45 )
           self.banckAddressView.font = UIFont(name: localizedSitringFor(key: "AppRegularFont"), size: iphoneXFactor*45 )
           self.banckNameTextField.font = UIFont(name: localizedSitringFor(key: "AppRegularFont"), size: iphoneXFactor*45 )
           self.IBANTextField.font = UIFont(name: localizedSitringFor(key: "AppRegularFont"), size: iphoneXFactor*45 )
           self.descriptionTextView.font = UIFont(name: localizedSitringFor(key: "AppRegularFont"), size: iphoneXFactor*45 )
           self.Blabel.font = UIFont(name: localizedSitringFor(key: "AppBoldFont"), size: iphoneXFactor*45 )
           self.BlabelD.font = UIFont(name: localizedSitringFor(key: "AppBoldFont"), size: iphoneXFactor*45 )
           self.registerButton.titleLabel?.font = UIFont(name: localizedSitringFor(key: "AppBoldFont"), size: iphoneXFactor*50)
           }
        
        setSkyFloatingLanguage()
       }
    
    func setSkyFloatingLanguage(){
        handleLocaliztion(ForTextField: firstNameTextField, withPlaceholder: localizedSitringFor(key: "firstName"), andSelectedTitle: localizedSitringFor(key: "firstName") , andTitle: localizedSitringFor(key: "firstName"))
        handleLocaliztion(ForTextField: midleNameTextField, withPlaceholder: localizedSitringFor(key: "midName"), andSelectedTitle: localizedSitringFor(key: "midName") , andTitle: localizedSitringFor(key: "midName"))
        handleLocaliztion(ForTextField: lastNameTextField, withPlaceholder: localizedSitringFor(key: "lastName"), andSelectedTitle: localizedSitringFor(key: "lastName") , andTitle: localizedSitringFor(key: "lastName"))
        handleLocaliztion(ForTextField: userNameTextField, withPlaceholder: localizedSitringFor(key: "userName"), andSelectedTitle: localizedSitringFor(key: "userName") , andTitle: localizedSitringFor(key: "userName"))
        handleLocaliztion(ForTextField: phoneTextField, withPlaceholder: localizedSitringFor(key: "phoneNumber"), andSelectedTitle: localizedSitringFor(key: "phoneNumber") , andTitle: localizedSitringFor(key: "phoneNumber"))
        handleLocaliztion(ForTextField: emailTextField, withPlaceholder: localizedSitringFor(key: "email"), andSelectedTitle: localizedSitringFor(key: "email") , andTitle: localizedSitringFor(key: "email"))
        handleLocaliztion(ForTextField: countryName, withPlaceholder: localizedSitringFor(key: "counrty"), andSelectedTitle: localizedSitringFor(key: "counrty") , andTitle: localizedSitringFor(key: "counrty"))
        handleLocaliztion(ForTextField: banckNameTextField, withPlaceholder: localizedSitringFor(key: "BankName"), andSelectedTitle: localizedSitringFor(key: "BankName") , andTitle: localizedSitringFor(key: "BankName"))
        handleLocaliztion(ForTextField: swiftCodeTextField, withPlaceholder: localizedSitringFor(key: "SwiftCode"), andSelectedTitle: localizedSitringFor(key: "SwiftCode") , andTitle: localizedSitringFor(key: "SwiftCode"))
        handleLocaliztion(ForTextField: IBANTextField, withPlaceholder: localizedSitringFor(key: "IBAN"), andSelectedTitle: localizedSitringFor(key: "IBAN") , andTitle: localizedSitringFor(key: "IBAN"))
        handleLocaliztion(ForTextField: accountNumberTextField, withPlaceholder: localizedSitringFor(key: "AcountNumber"), andSelectedTitle: localizedSitringFor(key: "AcountNumber") , andTitle: localizedSitringFor(key: "AcountNumber"))
        handleLocaliztion(ForTextField: banckAddressView, withPlaceholder: localizedSitringFor(key: "BankAddress"), andSelectedTitle: localizedSitringFor(key: "BankAddress") , andTitle: localizedSitringFor(key: "BankAddress"))
        handleLocaliztion(ForTextField: banckAddressView, withPlaceholder: localizedSitringFor(key: "Address"), andSelectedTitle: localizedSitringFor(key: "Address") , andTitle: localizedSitringFor(key: "Address"))
  
    }
       
       /// init tags view
       func initTagsView(){
           
            self.tagList.textFont = isIpad ? UIFont.systemFont(ofSize: 30) : UIFont.systemFont(ofSize: 15)
            //self.tagList.addTags(self.tags)
            tagList.tagViews.forEach { (tagView) in
               if let title = tagView.titleLabel?.text {
                   selectedTag.filter({ $0 == title
                       }).forEach({ _ in
                       tagView.isSelected = true
                   })
               }
           }
           
       }
       
       /// init drop downs
       func initDropDown() {
           if UIScreen.main.traitCollection.horizontalSizeClass == .regular
           {
               DropDown.appearance().cellHeight = 60
               DropDown.appearance().textFont = UIFont.systemFont(ofSize: 34)
           }
           else
           {
               DropDown.appearance().cellHeight = 40
               DropDown.appearance().textFont = UIFont.systemFont(ofSize: 17)
           }
           DropDown.appearance().semanticContentAttribute = .forceLeftToRight
           
           initCountryDropDown()
          
       }
       
       /// init country drop down
       func initCountryDropDown() {
          countryDropDown.anchorView = countryView
          countryDropDown.dataSource = [localizedSitringFor(key: "loading")]
          countryDropDown.direction = .any
          countryDropDown.selectionAction = {[unowned self] (index:Int,item:String) in
          if self.countryList.count == 0 {
              return
          }
          
          self.selectedCountry = self.countryList[index]
          self.isGulf = self.countryList[index].isGulfCountry ?? false
          self.countryName.text = self.countryList[index].name
          self.countrySelectedAction(isGulf: self.isGulf)
         }
     }
       
       // to show and hide some detail depend on gulf
      func countrySelectedAction(isGulf:Bool){
           print("isgulf ", isGulf)
           if isGulf {
               mainQueue {
                   self.banckNameTextField.isHidden = false
                   self.accountNumberTextField.isHidden = true
                   self.swiftCodeTextField.isHidden = false
                   self.banckAddressView.isHidden = true
                   self.homeAddressTextField.isHidden = true
                   self.IBANTextField.isHidden = false
               }
           }else {
               mainQueue {
                   self.banckNameTextField.isHidden = false
                   self.accountNumberTextField.isHidden = false
                   self.swiftCodeTextField.isHidden = false
                   self.banckAddressView.isHidden = false
                   self.homeAddressTextField.isHidden = false
                   self.IBANTextField.isHidden = true
               }
           }
       }
       
       
       /// get matrials ids form selected tags
       func getMatrialsIds() -> String {
           var ids = ""
           for (index,title) in selectedTag.enumerated() {
               if index == selectedTag.count - 1 {
                   ids += String(matrialList.filter { $0.name == title}.first?.id ?? 0)
               }else {
                   ids += String(matrialList.filter { $0.name == title}.first?.id ?? 0) + ","
               }
               
           }
           return ids
       }
    
        /// set text field text from get user data response
        func initProfile(user: User){
            firstNameTextField.text = user.firstName
            midleNameTextField.text = user.middleName
            lastNameTextField.text = user.lastName
            phoneTextField.text = user.mobile
            emailTextField.text = user.email
            selectedCountry = countryList.filter({ $0.id == user.countryId }).first
            countryName.text = selectedCountry?.name
            banckNameTextField.text = user.bankName
            accountNumberTextField.text = user.accountNumber
            swiftCodeTextField.text = user.swiftCode
            homeAddressTextField.text = user.personalAddress
            banckAddressView.text = user.bankAddress
            userNameTextField.text = user.userName
            IBANTextField.text = user.iban
            descriptionTextView.text = user.description
            
            isGulf = selectedCountry?.isGulfCountry ?? false
            countrySelectedAction(isGulf: selectedCountry?.isGulfCountry ?? false)
            
        }
    
    
       /// validate Fields
       ///
       /// - Returns: true or false
       func ValidateField() -> Bool{
           if firstNameTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty == true {
              displayMessage(message: localizedSitringFor(key: "emptyFirstName"), messageError: true)
              return false
          }

          if midleNameTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty == true {
              displayMessage(message: localizedSitringFor(key: "emptyMidName"), messageError: true)
              return false
          }

          if lastNameTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty == true {
              displayMessage(message: localizedSitringFor(key: "emptyLastName"), messageError: true)
              return false
          }

        if phoneTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty == true && phoneTextField.text?.count ?? 0 < 8{
              displayMessage(message: localizedSitringFor(key: "emptyPhone"), messageError: true)
              return false
          }

           if !isValidEmail(emailTextField.text ?? ""){
              displayMessage(message: localizedSitringFor(key: "emptyEmail"), messageError: true)
              return false
          }
           
           if countryName.text?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty == true {
               displayMessage(message: localizedSitringFor(key: "emptyCountry"), messageError: true)
               return false
           }
           
           if isGulf {
              
              if IBANTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty == true {
                  displayMessage(message: localizedSitringFor(key: "emptyIban"), messageError: true)
                  return false
              }
           }else {
               if accountNumberTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty == true {
                   displayMessage(message: localizedSitringFor(key: "emptyAccountNumber"), messageError: true)
                   return false
               }
               if homeAddressTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty == true {
                   displayMessage(message: localizedSitringFor(key: "emptyhomeAddrees"), messageError: true)
                   return false
               }
               if banckAddressView.text?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty == true {
                   displayMessage(message: localizedSitringFor(key: "emptybankAddrees"), messageError: true)
                   return false
               }
           }
           
           if banckNameTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty == true {
              displayMessage(message: localizedSitringFor(key: "emptyBankName"), messageError: true)
              return false
           }
           
          if swiftCodeTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty == true {
             displayMessage(message: localizedSitringFor(key: "emptyswiftCode"), messageError: true)
             return false
          }
        if userNameTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty == true {
           displayMessage(message: localizedSitringFor(key: "emptyUserName"), messageError: true)
           return false
        }

           if descriptionTextView.text?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty == true {
               displayMessage(message: localizedSitringFor(key: "emptyDescription"), messageError: true)
               return false
           }
           
           if selectedTag.count < 1 {
               displayMessage(message: localizedSitringFor(key: "emptyMatrial"), messageError: true)
               return false
           }
           
           return true
       }
}



//MARK:- tag list view delegate
extension EditTeacherProfileViewController: TagListViewDelegate {
    func tagPressed(_ title: String, tagView: TagView, sender: TagListView) {
             print("Tag pressed: \(title), \(sender)")

        var isFound = false
        selectedTag.filter({ $0 == title
        }).forEach({_ in
            isFound = true
            selectedTag = selectedTag.filter { $0 != title}
            tagView.isSelected = !tagView.isSelected

        })
        
        if !isFound{
            selectedTag.append(title)
            tagView.isSelected = !tagView.isSelected
        }

    }
        
}



//MARK:- Network
extension EditTeacherProfileViewController: NetworkingHelperDeleget {
    
  func onHelper(getData data: DataResponse<String>, fromApiName name: String, withIdentifier id: String) {
       if id == GET_COUNTRY { handleGetCountryResponse(response: data) }
       else if id == GET_MATRIAL { handleMatrialResponse(response: data) }
       else if id == GET_USER_DATA { handleUserDataResponse(response: data) }
       else if id == GET_ALL_MATRIAL { handleAllMatrialResponse(response: data) }
       else if id == UPDATE_TEACH { handleUpdateTeacherResponse(response: data) }
       else if id == LOGOUT{ handleLogout(forResponse: data) }
   }
   
   func onHelper(getError error: String, fromApiName name: String, withIdentifier id: String) {
       displayMessage(message: localizedSitringFor(key: "unkwonError") , messageError: true)
   }
   
   // MARK:- request apis from server
   
   func requestCountryApi() {
       NETWORK.connectWithHeaderTo(api: ApiNames.COUNTRIES, andIdentifier: GET_COUNTRY,forController: self, methodType: .get)
   }
   
   func requestMatrialApi() {
        let url = ApiNames.USER_MATERIAL + "?userid=" + String(Account.shared.id)
        NETWORK.connectWithHeaderTo(api: url ,andIdentifier: GET_MATRIAL, forController: self, methodType: .get)
   }
    
    func requestAllMatrialApi() {
        NETWORK.connectWithHeaderTo(api: ApiNames.MATERIALS,  andIdentifier: GET_ALL_MATRIAL, forController: self, methodType: .get)
    }
    
    func requestUserDataApi() {
        let url = ApiNames.GET_USER_DATA + "?userid=" + String(Account.shared.id)
        NETWORK.connectWithHeaderTo(api: url ,andIdentifier: GET_USER_DATA, withLoader: true, forController: self, methodType: .get)
    }
       
    func requestUpdateTeacherApi() {
           var paramters: [String: Any] = [:]
           paramters["userid"] = Account.shared.id
           paramters["firstname"] = firstNameTextField.text ?? ""
           paramters["middlename"] = midleNameTextField.text ?? ""
           paramters["lastname"] = lastNameTextField.text ?? ""
           paramters["mobile"] = phoneTextField.text ?? ""
           paramters["email"] = emailTextField.text ?? ""
           paramters["username"] = userNameTextField.text ?? ""
           paramters["materialids"] = getMatrialsIds()
           paramters["countryid"] = String(selectedCountry?.id ?? 0)
           paramters["iban"] = IBANTextField.text ?? ""
           paramters["swiftcode"] = swiftCodeTextField.text ?? ""
           paramters["accountnumber"] = accountNumberTextField.text ?? ""
           paramters["bankname"] = banckNameTextField.text ?? ""
           paramters["bankaddress"] = banckAddressView.text ?? ""
           paramters["personaladdress"] = homeAddressTextField.text ?? ""
           paramters["description"] = descriptionTextView.text
          

           print("edit",paramters)

            NETWORK.connectWithHeaderTo(api: ApiNames.UPDATE_TEACHER, withParameters: paramters, andIdentifier: UPDATE_TEACH, withLoader: true, forController: self, methodType: .post)
       }

       
    
       // MARK:- handle response server
       func handleGetCountryResponse(response: DataResponse<String>){
           
           switch response.response?.statusCode {
           case 200:
               do{
                 let country = try JSONDecoder().decode([Country].self, from: response.data ?? Data())
                 countryList = country
                 countryDropDown.dataSource = country.compactMap({$0.name})
                 countryDropDown.reloadAllComponents()
                 requestUserDataApi()
             }catch let error{
                print("hedra",error)
                 displayMessage(message: localizedSitringFor(key: "unknownError"), messageError: true)
             }
           case 202:
               displayMessage(message: localizedSitringFor(key: "EmptyCountries"), messageError: true)
           case 207:
                displayMessage(message: localizedSitringFor(key: "UserISNotActive"), messageError: true)
                delay(timeSession) {
                    self.logoutRequest()
                }
           case 400:
               displayMessage(message: localizedSitringFor(key: "badRequst"), messageError: true)
           case 401:
               displayMessage(message: localizedSitringFor(key: "Unauthorized"), messageError: true)
           default:
               displayMessage(message: localizedSitringFor(key: "unknownError"), messageError: true)
           }
       }
       
       func handleMatrialResponse(response: DataResponse<String>) {
           switch response.response?.statusCode {
           case 200:
               do{
                   let matrial = try JSONDecoder().decode([Matrial].self, from: response.data ?? Data())
                   selectedTag = matrial.compactMap({$0.name})
                   initTagsView()
             }catch{
                 displayMessage(message: localizedSitringFor(key: "unknownError"), messageError: true)
             }
           case 201:
                displayMessage(message: localizedSitringFor(key: "NoUserFound"), messageError: true)
           case 202:
               displayMessage(message: localizedSitringFor(key: "MisMaterial"), messageError: true)
           case 207:
                displayMessage(message: localizedSitringFor(key: "UserISNotActive"), messageError: true)
                delay(timeSession) {
                    self.logoutRequest()
                }
           case 400:
               displayMessage(message: localizedSitringFor(key: "badRequst"), messageError: true)
           case 401:
               displayMessage(message: localizedSitringFor(key: "Unauthorized"), messageError: true)
           default:
               displayMessage(message: localizedSitringFor(key: "unknownError"), messageError: true)
           }
       }
    
    func handleAllMatrialResponse(response: DataResponse<String>) {
              
          switch response.response?.statusCode {
          case 200:
              do{
                  let matrial = try JSONDecoder().decode([Matrial].self, from: response.data ?? Data())
                  matrialList = matrial
                  tags = matrial.compactMap({$0.name})
                  self.tagList.addTags(self.tags)
                  requestMatrialApi()
            }catch{
                displayMessage(message: localizedSitringFor(key: "unknownError"), messageError: true)
            }
          case 202:
              displayMessage(message: localizedSitringFor(key: "MisMaterial"), messageError: true)
        case 207:
            displayMessage(message: localizedSitringFor(key: "UserISNotActive"), messageError: true)
            delay(timeSession) {
                self.logoutRequest()
            }
          case 400:
              displayMessage(message: localizedSitringFor(key: "badRequst"), messageError: true)
          case 401:
              displayMessage(message: localizedSitringFor(key: "Unauthorized"), messageError: true)
          default:
              displayMessage(message: localizedSitringFor(key: "unknownError"), messageError: true)
          }
}
    
  func handleUserDataResponse(response: DataResponse<String>){
        switch response.response?.statusCode {
        case 200:
            do{
                let user = try JSONDecoder().decode(User.self, from: response.data ?? Data())
                currentUser = user
                Account.shared.userName = user.userName ?? ""
                Account.shared.storeData()
                initProfile(user: currentUser)
          }catch{
              displayMessage(message: localizedSitringFor(key: "unknownError"), messageError: true)
          }
        case 207:
            displayMessage(message: localizedSitringFor(key: "UserISNotActive"), messageError: true)
            delay(timeSession) {
                self.logoutRequest()
            }
        case 400:
            displayMessage(message: localizedSitringFor(key: "badRequst"), messageError: true)
        case 401:
            displayMessage(message: localizedSitringFor(key: "Unauthorized"), messageError: true)
        default:
            displayMessage(message: localizedSitringFor(key: "unknownError"), messageError: true)
        }
    }
       
       func handleUpdateTeacherResponse(response: DataResponse<String>){
           switch response.response?.statusCode {
           case 200:
               do{
                  let user = try JSONDecoder().decode(User.self, from: response.data ?? Data())
                  currentUser = user
                  Account.shared.userName = user.userName ?? ""
                  Account.shared.storeData()
                self.navigationController?.popViewController(animated: true)
                displayMessage(message: localizedSitringFor(key: "updateSuccess"), messageError: false)
             }catch{
                 displayMessage(message: localizedSitringFor(key: "unknownError"), messageError: true)
             }
            
           case 201:
               displayMessage(message: localizedSitringFor(key: "ExistMail"), messageError: true)
           case 202:
               displayMessage(message: localizedSitringFor(key: "ExistUserName"), messageError: true)
           case 203:
               displayMessage(message: localizedSitringFor(key: "ExistPhone"), messageError: true)
            case 204:
            displayMessage(message: localizedSitringFor(key: "NoUserFound"), messageError: true)
            case 207:
                displayMessage(message: localizedSitringFor(key: "UserISNotActive"), messageError: true)
                delay(timeSession) {
                    self.logoutRequest()
                }
           case 400:
            displayMessage(message: localizedSitringFor(key: "badRequst"), messageError: true)
           case 401:
               displayMessage(message: localizedSitringFor(key: "Unauthorized"), messageError: true)
           default:
               displayMessage(message: localizedSitringFor(key: "unknownError"), messageError: true)
           }
       }
    
    /// uses for connecting to server and recive data
    func logoutRequest()
    {
        var parameters: [String: Any] = [:]
        parameters["userid"] = Account.shared.id
        parameters["DeviceId"] = deviceID
        
        var url = ApiNames.LOGOUT + "?userid=" + String(Account.shared.id)
        url += "&DeviceId=" + deviceID
        
        NETWORK.connectWithHeaderTo(api: url, withParameters: parameters, andIdentifier: LOGOUT, withLoader: true, forController: self, methodType: .post)
    }
    
    
    /// handle login json response from server
    ///
    /// - Parameter response: server response
    func handleLogout(forResponse response: DataResponse<String>)
    {
        switch response.response?.statusCode {
        case 200:
            Account.shared.logout()
            pushToView(withId: baseSession)
        case 400:
            displayMessage(message: localizedSitringFor(key: "invalidUserOrPassword"), messageError: true)

        case 401:
            displayMessage(message: localizedSitringFor(key: "Unauthorized"), messageError: true)
        default:
            displayMessage(message: localizedSitringFor(key: "unknownError"), messageError: true)
        }
    }

}
