//
//  MyBalanceViewController.swift
//  ESAAL
//
//  Created by YoussefRomany on 11/19/19.
//  Copyright © 2019 HardTask. All rights reserved.
//

import UIKit
import Alamofire
import IBAnimatable


class MyBalanceViewController: UIViewController {
    
    //MARK:- IBoutlet
    @IBOutlet var headerView: GradientView!
    @IBOutlet var bLabel_B: UILabel!
    @IBOutlet var lLabel_R: UILabel!
    @IBOutlet var balanceLAbel_R: UILabel!
    @IBOutlet var questionNumberLAbel: UILabel!
    @IBOutlet var button_B: UIButton!
    @IBOutlet weak var requestDateLabel: UILabel!
    @IBOutlet weak var buttonContentView: AnimatableView!
    
    
    //MARK:- Variables
    var header: HeaderView?
    
    
    ///CONSTANTS
    let NETWORK = NetworkingHelper()
    let teacherCredit = "teacherCredit"
    let AskCredit = "AskCredit"
    let LOGOUT = "logout"


    //MARK:- App life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getTeacherCreditRequest()
    }
    
    override func viewDidAppear(_ animated: Bool) {
         super.viewDidAppear(animated)
         if header == nil{
             header = HeaderView.initView(fromController: self, andView: headerView, andTitle: localizedSitringFor(key: "MyBalance"), isFromRoot: true)
             header?.backView.isHidden = false
         }
     }
    
    
    //MARK:- IBAction
    @IBAction func requestCreditAction(_ sender: Any) {
        if Account.shared.isRequestes {
            displayAlert("",localizedSitringFor(key: "requestDone"))
        }else {
            addCreditRequest()
        }
        
    }
    
}


//MARK:- Helpers
extension MyBalanceViewController{
    
    func initView(){
        NETWORK.deleget = self
        setupFonts()
        setRequestState()
    }
    
    func setupFonts() {
        mainQueue {
            self.bLabel_B.font = UIFont(name: localizedSitringFor(key: "AppBoldFont"), size: iphoneXFactor*45 )
            self.lLabel_R.font = UIFont(name: localizedSitringFor(key: "AppBoldFont"), size: iphoneXFactor*45 )
            self.balanceLAbel_R.font = UIFont(name: localizedSitringFor(key: "AppRegularFont"), size: iphoneXFactor*45 )
            self.questionNumberLAbel.font = UIFont(name: localizedSitringFor(key: "AppRegularFont"), size: iphoneXFactor*45 )
            self.requestDateLabel.font = UIFont(name: localizedSitringFor(key: "AppRegularFont"), size: iphoneXFactor*45 )
            self.button_B.titleLabel?.font = UIFont(name: localizedSitringFor(key: "AppBoldFont"), size: iphoneXFactor*45)
        }
    }
    
    func setRequestState() {
        if Account.shared.isRequestes{
            self.buttonContentView.isHidden = true
            self.requestDateLabel.isHidden = false

//            var dString : String = Account.shared.requestDate
//            dString = dString.replacingOccurrences(of: "T", with: " ")
//            dString = UTCToLocal(date: dString, returnFormat: "MM/dd/yyyy")

            mainQueue {
                self.requestDateLabel.text = localizedSitringFor(key: "requestComplete") //+ dString
            }
            
        }else {
            self.buttonContentView.isHidden = false
            self.requestDateLabel.isHidden = true

        }
    }
}



// MARK: - NetworkingHelperDeleget
extension MyBalanceViewController: NetworkingHelperDeleget{
    func onHelper(getData data: DataResponse<String>, fromApiName name: String, withIdentifier id: String) {
        if id == teacherCredit{handleTeacherCredit(forResponse: data)}
        else if id == AskCredit {handleAddCredit(forResponse: data)}
        else if id == LOGOUT{ handleLogout(forResponse: data) }

    }
    
    func onHelper(getError error: String, fromApiName name: String, withIdentifier id: String) {
        displayMessage(message: localizedSitringFor(key: "unknownError"), messageError: true)
    }
    
    /// uses for connecting to server and recive data
    func getTeacherCreditRequest()
    {
        NETWORK.connectWithHeaderTo(api: ApiNames.TEACHER_CREDIT+"?userid=\(Account.shared.id)", andIdentifier : teacherCredit, withLoader: true, forController: self, methodType: .get)
    }
    
    
    /// handle  response from server
    ///
    /// - Parameter response: server response
    func handleTeacherCredit(forResponse response: DataResponse<String>)
    {
        switch response.response?.statusCode {
        case 200:
              do{
                  let sett = try JSONDecoder().decode(TeacherCredit.self, from: response.data ?? Data())
                balanceLAbel_R.text = String(format: "%.3f", sett.totalAmount ?? 0) + " \(localizedSitringFor(key: "currency"))"
                questionNumberLAbel.text = "\(sett.totalQuestionAnswer ?? 0)"
                Account.shared.isRequestes = sett.isRequest ?? false
                Account.shared.storeData()

              }catch{
                displayMessage(message: localizedSitringFor(key: "unknownError"), messageError: true)
            }
            
        case 201:
             displayMessage(message: localizedSitringFor(key: "UserNotFound"), messageError: true)
        case 202:
                displayMessage(message: localizedSitringFor(key: "LessCredit"), messageError: true)
        case 400:
         displayMessage(message: localizedSitringFor(key: "badRequst"), messageError: true)
        case 207:
            displayMessage(message: localizedSitringFor(key: "UserISNotActive"), messageError: true)
            delay(timeSession) {
                self.logoutRequest()
            }
        case 401:
            displayMessage(message: localizedSitringFor(key: "Unauthorized"), messageError: true)
        default:
            displayMessage(message: localizedSitringFor(key: "unknownError"), messageError: true)
        }
    }
    
    /// uses for connecting to server and recive data
    func addCreditRequest()
    {
        var parameters: [String: Any] = [:]
        parameters["userid"] = Account.shared.id
        NETWORK.connectWithHeaderTo(api: ApiNames.REQUEST_CREDIT+"?userid=\(Account.shared.id)",withParameters: parameters, andIdentifier : AskCredit, withLoader: true, withEncoding: URLEncoding.default, forController: self, methodType: .post)
    }
    
    
    /// handle info response from server
    ///
    /// - Parameter response: server response
    func handleAddCredit(forResponse response: DataResponse<String>)
    {
        switch response.response?.statusCode {
        case 200:
            do{

                let sett = try JSONDecoder().decode(User.self, from: response.data ?? Data())
                Account.shared.isRequestes = sett.isRequest ?? false
                Account.shared.requestDate = sett.requestDate ?? ""
                Account.shared.storeData()
                setRequestState()
                
            }catch{
              displayMessage(message: localizedSitringFor(key: "unknownError"), messageError: true)
          }
        case 201:
            displayMessage(message: localizedSitringFor(key: "UserNotFound"), messageError: true)
        case 202:
            displayMessage(message: localizedSitringFor(key: "creditLess"), messageError: true)
        case 207:
            displayMessage(message: localizedSitringFor(key: "UserISNotActive"), messageError: true)
            delay(timeSession) {
                self.logoutRequest()
            }
        case 400:
         displayMessage(message: localizedSitringFor(key: "badRequst"), messageError: true)
        case 401:
            displayMessage(message: localizedSitringFor(key: "Unauthorized"), messageError: true)
        default:
            displayMessage(message: localizedSitringFor(key: "unknownError"), messageError: true)
        }
    }
    
    /// uses for connecting to server and recive data
    func logoutRequest()
    {
        var parameters: [String: Any] = [:]
        parameters["userid"] = Account.shared.id
        parameters["DeviceId"] = deviceID
        
        var url = ApiNames.LOGOUT + "?userid=" + String(Account.shared.id)
        url += "&DeviceId=" + deviceID
        
        NETWORK.connectWithHeaderTo(api: url, withParameters: parameters, andIdentifier: LOGOUT, withLoader: true, forController: self, methodType: .post)
    }
    
    
    /// handle login json response from server
    ///
    /// - Parameter response: server response
    func handleLogout(forResponse response: DataResponse<String>)
    {
        switch response.response?.statusCode {
        case 200:
            Account.shared.logout()
            pushToView(withId: baseSession)
        case 400:
            displayMessage(message: localizedSitringFor(key: "invalidUserOrPassword"), messageError: true)

        case 401:
            displayMessage(message: localizedSitringFor(key: "Unauthorized"), messageError: true)
        default:
            displayMessage(message: localizedSitringFor(key: "unknownError"), messageError: true)
        }
    }

}
