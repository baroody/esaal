//
//  FilterViewController.swift
//  ESAAL
//
//  Created by Mina Thabet on 11/8/19.
//  Copyright © 2019 HardTask. All rights reserved.
//

import UIKit
import Alamofire

protocol FilterSelectedMatrialDeleget: NSObjectProtocol {
    func selectedMatrialClicked(selectedMatrials: [String])
}

class FilterViewController: UIViewController {

    // MARK:- IBOutlet
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var filterLabel: UILabel!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var filterBtn: UIButton!
    
    /// constant
    let NETWORK = NetworkingHelper()
    let GET_MATRIAL = "getMatrial"
    let LOGOUT = "logout"

    /// variables
    weak var filterDeleget: FilterSelectedMatrialDeleget?
    var matrialList: [Matrial] = []
    var selectedMatrialId: Int?
    var selectedMatrial: [String] = []
    
    
    //MARK:- App Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func dissmisAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func FilterAction(_ sender: Any) {
        //if selectedMatrial.count > 0{
            self.filterDeleget?.selectedMatrialClicked(selectedMatrials: selectedMatrial)
            self.dismiss(animated: true, completion: nil)
        //}
    }
}


//MARK:- Helpers
extension FilterViewController {
    func initView(){
        NETWORK.deleget = self
        setViewBackgroundTansparent()
        setupFonts()
        initCommonCollectionView()
        requestUserMatrialApi()
    }
    
    func setViewBackgroundTansparent() {
        self.view.backgroundColor = UIColor.clear
        self.contentView.backgroundColor = UIColor.black.withAlphaComponent(0.7)
    }
    
    func setupFonts()  {
        mainQueue {
            //self.filterLabel.text = localizedSitringFor(key: "filterBy")
            self.filterLabel.font = UIFont(name: localizedSitringFor(key: "AppBoldFont"), size: iphoneXFactor*45 )
            self.filterBtn.titleLabel?.font = UIFont(name:  localizedSitringFor(key: "AppBoldFont"), size: 45*iphoneXFactor)
        }
    }
    
    func setMatrialFromQuestion() {
        if selectedMatrial.count > 0 {
            for (index,value) in matrialList.enumerated(){
                if selectedMatrial.filter({ $0 == String(value.id ?? 0)
                }).count > 0 {
                    let indexPath = IndexPath(row: index, section: 0)
                    collectionView.selectItem(at: indexPath, animated: true, scrollPosition: [])
                }
            }
        }
    }
    
    func initCommonCollectionView() {
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(UINib(nibName: CellIdentifier.FilterCell.rawValue, bundle: nil), forCellWithReuseIdentifier: CellIdentifier.FilterCell.rawValue)
        collectionView.allowsMultipleSelection = true
        matrialList.append(Matrial(id: 0, arabicName: "الكل", englishName: "All"))
    }
}


//MARK:- collection view delegat and datasource
extension FilterViewController: UICollectionViewDelegate,UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return matrialList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CellIdentifier.FilterCell.rawValue, for: indexPath) as! FilterCell
        cell.configureCell(subjectName: matrialList[indexPath.item].name ?? "")
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            collectionView.indexPathsForSelectedItems?.forEach({
                if $0.row != 0{
                   collectionView.deselectItem(at: $0, animated: true)
                }
            })
            selectedMatrial = []
        }else {
            collectionView.indexPathsForSelectedItems?.forEach({
                if $0.row == 0{
                   collectionView.deselectItem(at: $0, animated: true)
                }
            })
            selectedMatrial.append(String(matrialList[indexPath.item].id ?? 0))
        }

        
        print("selected mataril",selectedMatrial)
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        let matrialId = String(matrialList[indexPath.item].id ?? 0)
        selectedMatrial.filter({ $0 == matrialId
        }).forEach({_ in
            selectedMatrial = selectedMatrial.filter { $0 != matrialId }
        })
        print("disselected mataril",selectedMatrial)
    }
    
    
}

//MARK:- collection view flow layout
extension FilterViewController: UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.frame.width / 3) - 5, height: screenWidth * 0.13)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0

    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 20, left: 0 , bottom: 0, right: 0 )
    }
}


//MARK:- Network
extension FilterViewController: NetworkingHelperDeleget {
    
  func onHelper(getData data: DataResponse<String>, fromApiName name: String, withIdentifier id: String) {
       if id == GET_MATRIAL { handleMatrialResponse(response: data) }
       else if id == LOGOUT{ handleLogout(forResponse: data) }

   }
   
   func onHelper(getError error: String, fromApiName name: String, withIdentifier id: String) {
       displayMessage(message: localizedSitringFor(key: "unkwonError") , messageError: true)
   }
   
   // MARK:- request apis from server

    func requestUserMatrialApi() {
        let url = ApiNames.USER_MATERIAL + "?userid=" + String(Account.shared.id)
        NETWORK.connectWithHeaderTo(api: url ,andIdentifier: GET_MATRIAL,withLoader: true ,forController: self, methodType: .get)
    }
    
    
    // MARK:- handle response server

    func handleMatrialResponse(response: DataResponse<String>) {
          switch response.response?.statusCode {
          case 200:
              do{
                let matrial = try JSONDecoder().decode([Matrial].self, from: response.data ?? Data())
                matrialList += matrial
                collectionView.reloadData()
                setMatrialFromQuestion()
            }catch let error{
                print(error)
                displayMessage(message: localizedSitringFor(key: "unknownError"), messageError: true)
            }
          case 202:
              displayMessage(message: localizedSitringFor(key: "MisMaterial"), messageError: true)
        case 207:
            displayMessage(message: localizedSitringFor(key: "UserISNotActive"), messageError: true)
            delay(timeSession) {
                self.logoutRequest()
            }
          case 400:
              displayMessage(message: localizedSitringFor(key: "badRequst"), messageError: true)
          case 401:
              displayMessage(message: localizedSitringFor(key: "Unauthorized"), messageError: true)
          default:
              displayMessage(message: localizedSitringFor(key: "unknownError"), messageError: true)
          }
    }
    

  /// uses for connecting to server and recive data
  func logoutRequest()
  {
      var parameters: [String: Any] = [:]
      parameters["userid"] = Account.shared.id
      parameters["DeviceId"] = deviceID
      
      var url = ApiNames.LOGOUT + "?userid=" + String(Account.shared.id)
      url += "&DeviceId=" + deviceID
      
      NETWORK.connectWithHeaderTo(api: url, withParameters: parameters, andIdentifier: LOGOUT, withLoader: true, forController: self, methodType: .post)
  }
  
  
  /// handle login json response from server
  ///
  /// - Parameter response: server response
  func handleLogout(forResponse response: DataResponse<String>)
  {
      switch response.response?.statusCode {
      case 200:
          Account.shared.logout()
          pushToView(withId: baseSession)
      case 400:
          displayMessage(message: localizedSitringFor(key: "invalidUserOrPassword"), messageError: true)

      case 401:
          displayMessage(message: localizedSitringFor(key: "Unauthorized"), messageError: true)
      default:
          displayMessage(message: localizedSitringFor(key: "unknownError"), messageError: true)
      }
  }

       
      
}

