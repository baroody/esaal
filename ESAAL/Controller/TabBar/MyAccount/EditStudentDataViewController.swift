//
//  EditStudentDataViewController.swift
//  ESAAL
//
//  Created by Mina Thabet on 11/9/19.
//  Copyright © 2019 HardTask. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import Alamofire

class EditStudentDataViewController: UIViewController {
    
    @IBOutlet var firstNameTextField: SkyFloatingLabelTextField!
    @IBOutlet var midleNameTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var lastNameTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var phoneTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var emailTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var userNameTextField: SkyFloatingLabelTextField!
    @IBOutlet var registerButton: UIButton!
    @IBOutlet var headerView: UIView!

    
    //MARK:- Constant
    let NETWORK = NetworkingHelper()
    let GET_USER_DATA = "getUserData"
    let UPDATE_STUDENT = "updateStudent"
    let LOGOUT = "logout"

    //MARK:- Variables
    var header: HeaderView?
    var currentUser: User!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        initView()

    }

    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        if header == nil{
            header = HeaderView.initView(fromController: self, andView: headerView, andTitle: localizedSitringFor(key: "EditAccount"), isFromRoot: true)
            header?.backView.isHidden = false
            
        }
    }
    
    
    @IBAction func registerAction(_ sender: Any) {
        if ValidateField() {
            requestUpdateStudentApi()
        }
    }
        
}


//MARK:- Helpers
extension EditStudentDataViewController{
    
    /// init view fro first time
    func initView() {
        NETWORK.deleget = self
        setupFont()
        requestUserDataApi()
       }

    /// init fonts depend on language
    func setupFont() {
        mainQueue {
        self.firstNameTextField.font = UIFont(name: localizedSitringFor(key: "AppRegularFont"), size: iphoneXFactor*45 )
        self.midleNameTextField.font = UIFont(name: localizedSitringFor(key: "AppRegularFont"), size: iphoneXFactor*45 )
        self.lastNameTextField.font = UIFont(name: localizedSitringFor(key: "AppRegularFont"), size: iphoneXFactor*45 )
        self.emailTextField.font = UIFont(name: localizedSitringFor(key: "AppRegularFont"), size: iphoneXFactor*45 )
        self.userNameTextField.font = UIFont(name: localizedSitringFor(key: "AppRegularFont"), size: iphoneXFactor*45 )
        self.phoneTextField.font = UIFont(name: localizedSitringFor(key: "AppRegularFont"), size: iphoneXFactor*45 )
        self.registerButton.titleLabel?.font = UIFont(name: localizedSitringFor(key: "AppBoldFont"), size: iphoneXFactor*50)
        }
        setSkyFloatingLanguage()
    }
    
    func setSkyFloatingLanguage(){
          handleLocaliztion(ForTextField: firstNameTextField, withPlaceholder: localizedSitringFor(key: "firstName"), andSelectedTitle: localizedSitringFor(key: "firstName") , andTitle: localizedSitringFor(key: "firstName"))
          handleLocaliztion(ForTextField: midleNameTextField, withPlaceholder: localizedSitringFor(key: "midName"), andSelectedTitle: localizedSitringFor(key: "midName") , andTitle: localizedSitringFor(key: "midName"))
          handleLocaliztion(ForTextField: lastNameTextField, withPlaceholder: localizedSitringFor(key: "lastName"), andSelectedTitle: localizedSitringFor(key: "lastName") , andTitle: localizedSitringFor(key: "lastName"))
          handleLocaliztion(ForTextField: userNameTextField, withPlaceholder: localizedSitringFor(key: "userName"), andSelectedTitle: localizedSitringFor(key: "userName") , andTitle: localizedSitringFor(key: "userName"))
          handleLocaliztion(ForTextField: phoneTextField, withPlaceholder: localizedSitringFor(key: "phoneNumber"), andSelectedTitle: localizedSitringFor(key: "phoneNumber") , andTitle: localizedSitringFor(key: "phoneNumber"))
          handleLocaliztion(ForTextField: emailTextField, withPlaceholder: localizedSitringFor(key: "email"), andSelectedTitle: localizedSitringFor(key: "email") , andTitle: localizedSitringFor(key: "email"))
    
      }
    
    
    
    /// validate Fields
    ///
    /// - Returns: true or false
    func ValidateField() -> Bool{
        if firstNameTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty == true {
           displayMessage(message: localizedSitringFor(key: "emptyFirstName"), messageError: true)
           return false
       }

       if midleNameTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty == true {
           displayMessage(message: localizedSitringFor(key: "emptyMidName"), messageError: true)
           return false
       }

       if lastNameTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty == true {
           displayMessage(message: localizedSitringFor(key: "emptyLastName"), messageError: true)
           return false
       }

       if phoneTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty == true && phoneTextField.text?.count ?? 0 < 8{
             displayMessage(message: localizedSitringFor(key: "emptyPhone"), messageError: true)
             return false
         }
        if userNameTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty == true && phoneTextField.text?.count ?? 0 < 8{
              displayMessage(message: localizedSitringFor(key: "emptyUserName"), messageError: true)
              return false
          }
        if !isValidEmail(emailTextField.text ?? ""){
           displayMessage(message: localizedSitringFor(key: "emptyEmail"), messageError: true)
           return false
       }
        
        return true
    }
    

    /// set text field text from get user data response
    func initProfile(user: User){
        firstNameTextField.text = user.firstName
        midleNameTextField.text = user.middleName
        lastNameTextField.text = user.lastName
        phoneTextField.text = user.mobile
        emailTextField.text = user.email
        userNameTextField.text = user.userName
    }

}


//MARK:- Network
extension EditStudentDataViewController: NetworkingHelperDeleget {
    
  func onHelper(getData data: DataResponse<String>, fromApiName name: String, withIdentifier id: String) {
        if id == GET_USER_DATA { handleUserDataResponse(response: data) }
        else if id == UPDATE_STUDENT { handleUpdateStudentResponse(response: data) }
        else if id == LOGOUT{ handleLogout(forResponse: data) }

       
   }
   
   func onHelper(getError error: String, fromApiName name: String, withIdentifier id: String) {
       displayMessage(message: localizedSitringFor(key: "unkwonError") , messageError: true)
   }
   
   // MARK:- request apis from server

    
    func requestUserDataApi() {
        let url = ApiNames.GET_USER_DATA + "?userid=" + String(Account.shared.id)
        NETWORK.connectWithHeaderTo(api: url ,andIdentifier: GET_USER_DATA, withLoader: true, forController: self, methodType: .get)
    }
       
       func requestUpdateStudentApi() {
           var paramters: [String: Any] = [:]
           paramters["userid"] = Account.shared.id
           paramters["firstname"] = firstNameTextField.text ?? ""
           paramters["middlename"] = midleNameTextField.text ?? ""
           paramters["lastname"] = lastNameTextField.text ?? ""
           paramters["mobile"] = phoneTextField.text ?? ""
           paramters["email"] = emailTextField.text ?? ""
           paramters["username"] = userNameTextField.text ?? ""
          

           print(paramters)

        NETWORK.connectWithHeaderTo(api: ApiNames.UPDATE_STUDENT, withParameters: paramters, andIdentifier: UPDATE_STUDENT, withLoader: true, forController: self, methodType: .post)
       }

       
    
       // MARK:- handle response server
       
    
    
  func handleUserDataResponse(response: DataResponse<String>){
        switch response.response?.statusCode {
        case 200:
            do{
                let user = try JSONDecoder().decode(User.self, from: response.data ?? Data())
                currentUser = user
                Account.shared.userName = user.userName ?? ""
                Account.shared.storeData()
                initProfile(user: currentUser)
          }catch{
              displayMessage(message: localizedSitringFor(key: "unknownError"), messageError: true)
          }
            
        case 207:
            displayMessage(message: localizedSitringFor(key: "UserISNotActive"), messageError: true)
            delay(timeSession) {
                self.logoutRequest()
            }
        case 400:
            displayMessage(message: localizedSitringFor(key: "badRequst"), messageError: true)
        case 401:
            displayMessage(message: localizedSitringFor(key: "Unauthorized"), messageError: true)
        default:
            displayMessage(message: localizedSitringFor(key: "unknownError"), messageError: true)
        }
    }
       
    
       func handleUpdateStudentResponse(response: DataResponse<String>){
           switch response.response?.statusCode {
           case 200:
               do{
                  let user = try JSONDecoder().decode(User.self, from: response.data ?? Data())
                  currentUser = user
                  Account.shared.userName = user.userName ?? ""
                  Account.shared.storeData()
                self.navigationController?.popViewController(animated: true)
                displayMessage(message: localizedSitringFor(key: "updateSuccess"), messageError: false)
             }catch{
                 displayMessage(message: localizedSitringFor(key: "unknownError"), messageError: true)
             }
            
           case 201:
               displayMessage(message: localizedSitringFor(key: "ExistMail"), messageError: true)
           case 202:
               displayMessage(message: localizedSitringFor(key: "ExistUserName"), messageError: true)
           case 203:
               displayMessage(message: localizedSitringFor(key: "ExistPhone"), messageError: true)
            case 204:
            displayMessage(message: localizedSitringFor(key: "NoUserFound"), messageError: true)
           case 207:
                displayMessage(message: localizedSitringFor(key: "UserISNotActive"), messageError: true)
                delay(timeSession) {
                    self.logoutRequest()
                }
           case 400:
            displayMessage(message: localizedSitringFor(key: "badRequst"), messageError: true)
           case 401:
               displayMessage(message: localizedSitringFor(key: "Unauthorized"), messageError: true)
           default:
               displayMessage(message: localizedSitringFor(key: "unknownError"), messageError: true)
           }
       }
    
    /// uses for connecting to server and recive data
    func logoutRequest()
    {
        var parameters: [String: Any] = [:]
        parameters["userid"] = Account.shared.id
        parameters["DeviceId"] = deviceID
        
        var url = ApiNames.LOGOUT + "?userid=" + String(Account.shared.id)
        url += "&DeviceId=" + deviceID
        
        NETWORK.connectWithHeaderTo(api: url, withParameters: parameters, andIdentifier: LOGOUT, withLoader: true, forController: self, methodType: .post)
    }
    
    
    /// handle login json response from server
    ///
    /// - Parameter response: server response
    func handleLogout(forResponse response: DataResponse<String>)
    {
        switch response.response?.statusCode {
        case 200:
            Account.shared.logout()
            pushToView(withId: baseSession)
        case 400:
            displayMessage(message: localizedSitringFor(key: "invalidUserOrPassword"), messageError: true)

        case 401:
            displayMessage(message: localizedSitringFor(key: "Unauthorized"), messageError: true)
        default:
            displayMessage(message: localizedSitringFor(key: "unknownError"), messageError: true)
        }
    }

}
