//
//  TabBarViewController.swift
//  ESAAL
//
//  Created by Mina Thabet on 10/31/19.
//  Copyright © 2019 HardTask. All rights reserved.
//

import UIKit

class TabBarViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        addLineInBottom()
        addShadow()
        // Do any additional setup after loading the view.
    }

}

// MARK:- Helpers
extension TabBarViewController {
    
    func addLineInBottom(){
        let lineWidth = (tabBar.frame.width/CGFloat(tabBar.items!.count)) / 3
        let lineHeight = tabBar.frame.height
        tabBar.selectionIndicatorImage = UIImage().createSelectionIndicator(color: hexStringToUIColor(hex: "#0A68B0"), size: CGSize(width: lineWidth, height: lineHeight ), lineWidth: 2.0)
    }
    
    func addShadow() {
        tabBar.isTranslucent = false
        tabBar.layer.shadowColor = UIColor.darkGray.cgColor
        tabBar.layer.shadowRadius = 4
        tabBar.layer.shadowOpacity = 0.4
        tabBar.layer.shadowOffset = CGSize(width: 1, height: 1)
        tabBar.backgroundColor = UIColor.white
    }
}


//MARK:- UITabBar Controller Delegate
extension TabBarViewController: UITabBarControllerDelegate {
    
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        if Account.shared.id == 0 {
            if tabBarController.selectedIndex == 1 || tabBarController.selectedIndex == 2 {
//                viewController.tabBarController?.selectedIndex = 0
//                viewController.navigationController?.popToRootViewController(animated: false)
//                viewController.navigationController?.popViewController(animated: true)
//                displayAlertConfirmation(localizedSitringFor(key: "Attention"), localizedSitringFor(key: "notLogin")) {
                    // go to login if user not login
                    pushToView(withId: "LoginViewController")
              //  }
            }
        }
    }
    
}
