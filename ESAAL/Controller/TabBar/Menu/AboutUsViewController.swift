//
//  AboutUsViewController.swift
//  ESAAL
//
//  Created by Mina Thabet on 11/5/19.
//  Copyright © 2019 HardTask. All rights reserved.
//

import UIKit
import Alamofire
import WebKit

class AboutUsViewController: UIViewController {
    
    //MARK:- IBOutlet
    @IBOutlet weak var headerView: GradientView!
    @IBOutlet var webView: WKWebView!
    
    
    ///Constant
    let NETWORK = NetworkingHelper()
    let LOGOUT = "logout"

    
    ///variables
    var header: HeaderView?
    var htmlType: HtmlType = HtmlType.AboutUs
    lazy var headerTitle = localizedSitringFor(key: "aboutUs")
    var fontColor001:String = "#000000" // dark blue
    
    //MARK:- App life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()

        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if header == nil{
            header = HeaderView.initView(fromController: self, andView: headerView, andTitle: headerTitle, isFromRoot: true)
            header?.backView.isHidden = false
        }
    }
}

//MARK:- Helpers
extension AboutUsViewController{
    func initView(){
        NETWORK.deleget = self
        requestAboutUsApi()
    }
}

//MARK:- Networking
extension AboutUsViewController: NetworkingHelperDeleget{
    
    func onHelper(getData data: DataResponse<String>, fromApiName name: String, withIdentifier id: String) {
        if id == LOGOUT{ handleLogout(forResponse: data) }
        else{
            handleAboutUsResponse(forResponse: data)
        }
    }

    func onHelper(getError error: String, fromApiName name: String, withIdentifier id: String) {
       displayMessage(message: localizedSitringFor(key: "unknownError"), messageError: true)
    }


   // MARK:- request apis from server
   func requestAboutUsApi() {
        NETWORK.connectWithHeaderTo(api: ApiNames.ABOUT_US, withLoader: true, forController: self, methodType: .get)
   }
   
   // MARK:- handle response server
   /// handle login json response from server
   ///
   /// - Parameter response: server response
   func handleAboutUsResponse(forResponse response: DataResponse<String>) {
    print(response.response?.statusCode, "mmmmmmmmmmmmmmm")

       switch response.response?.statusCode {

       case 200:
        do{
            let aboutUs = try JSONDecoder().decode(AboutUs.self, from: response.data ?? Data())
            
            var webString = ""
           
            if htmlType == HtmlType.AboutUs {
                webString = aboutUs.aboutUs ?? ""
            }else if htmlType == HtmlType.StudentTerms {
                webString =  "<p>\(localizedSitringFor(key: "studentTerms"))</p> \n" + (aboutUs.studentTerms ?? "")
            }else if htmlType == HtmlType.TeatcherTerms {
                webString = "<p>\(localizedSitringFor(key: "teacherTerms"))</p> \n"  + (aboutUs.teacherTerms ?? "")
            }else if htmlType == HtmlType.All {
                webString = "<p>\(localizedSitringFor(key: "teacherTerms"))</p> \n" + (aboutUs.teacherTerms ?? "") + "<br /><hr><br>  <p>\(localizedSitringFor(key: "studentTerms"))</p> \n" + (aboutUs.studentTerms ?? "")
            }
            
            let innerFontSize1: CGFloat = 140*iphoneXFactor
            let innerDirection: String = localizedSitringFor(key: "myDir")
            print("direction",innerDirection)
            
            let htmlString: String = "<html><head><style type='text/css'> p {font-size: \(innerFontSize1)px !important; }</style></head><body dir='\(innerDirection)'>\(webString)<br /><br /><style type='text/css'> iframe { width: 100%% !important; } img {width: 100%% !important; height: auto;} a {text-decoration: none; color : \(self.fontColor001)} table {width: 100%% !important;} }</style></body></html>";
            
            self.webView.loadHTMLString(htmlString , baseURL: nil)
            
            
         }catch{
             displayMessage(message: localizedSitringFor(key: "unknownError"), messageError: true)
         }
        
        case 207:
            displayMessage(message: localizedSitringFor(key: "UserISNotActive"), messageError: true)
            delay(timeSession) {
                self.logoutRequest()
            }

       default:
           displayMessage(message: localizedSitringFor(key: "unknownError"), messageError: true)
       }
   }
    
    
    /// uses for connecting to server and recive data
    func logoutRequest()
    {
        var parameters: [String: Any] = [:]
        parameters["userid"] = Account.shared.id
        parameters["DeviceId"] = deviceID
        
        var url = ApiNames.LOGOUT + "?userid=" + String(Account.shared.id)
        url += "&DeviceId=" + deviceID
        
        NETWORK.connectWithHeaderTo(api: url, withParameters: parameters, andIdentifier: LOGOUT, withLoader: true, forController: self, methodType: .post)
    }
    
    
    /// handle login json response from server
    ///
    /// - Parameter response: server response
    func handleLogout(forResponse response: DataResponse<String>)
    {
        switch response.response?.statusCode {
        case 200:
            Account.shared.logout()
            pushToView(withId: baseSession)
        case 400:
            displayMessage(message: localizedSitringFor(key: "invalidUserOrPassword"), messageError: true)

        case 401:
            displayMessage(message: localizedSitringFor(key: "Unauthorized"), messageError: true)
        default:
            displayMessage(message: localizedSitringFor(key: "unknownError"), messageError: true)
        }
    }
}
