//
//  VideoPlayerViewController.swift
//  ESAAL
//
//  Created by Mina Thabet on 12/25/19.
//  Copyright © 2019 HardTask. All rights reserved.
//

import UIKit
import JPVideoPlayer

class VideoPlayerViewController: UIViewController {
    
    //MARK:- IBOutlet
    @IBOutlet weak var videoContiner: UIView!
    
    
    //MARK:- Variables
    var videoLink: URL?
    

    //MARK:- Controller life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if let link = videoLink{
            videoContiner.jp_playVideo(with: link, bufferingIndicator: nil, controlView: nil, progressView: nil, configuration: nil)

        }

    }
    

    
    @IBAction func backAction(_ sender: Any) {
        videoContiner.jp_stopPlay()
        self.dismiss(animated: true, completion: nil)
    }
    
}
