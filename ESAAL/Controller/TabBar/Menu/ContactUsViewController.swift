//
//  ContactUsViewController.swift
//  ESAAL
//
//  Created by Mina Thabet on 11/5/19.
//  Copyright © 2019 HardTask. All rights reserved.
//

import UIKit
import Alamofire

class ContactUsViewController: UIViewController {
    
    //MARK:- IBOutlet
    @IBOutlet weak var headerView: GradientView!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var messageTextField: UITextView!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet var subjectTextField: UITextField!
    @IBOutlet var phoneTextField: UITextField!
    
    ///variables
    var header: HeaderView?
    var facebook = ""
    var twiter = ""
    var youtube = ""
    var insta = ""
    
    
    ///CONSTANTS
    let NETWORK = NetworkingHelper()
    let CONTACT = "contact"
    let socialInfo = "social"
    let LOGOUT = "logout"

    //MARK:- App life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initView()
    }
    

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if header == nil{
            header = HeaderView.initView(fromController: self, andView: headerView, andTitle: localizedSitringFor(key: "contactUs"), isFromRoot: true)
            header?.backView.isHidden = false
        }
    }

    @IBAction func faceBookAction(_ sender: Any) {
        if  facebook != ""{
            let main = UIStoryboard(name: "Main", bundle: nil)
            let vc = main.instantiateViewController(withIdentifier: "MediaViewController") as! MediaViewController
            vc.urlString = facebook
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
           displayMessage(message: localizedSitringFor(key: "emptyLink"), messageError: true)
        }
    }
    @IBAction func youtubeAction(_ sender: Any) {
        if  youtube != ""{
            let main = UIStoryboard(name: "Main", bundle: nil)
            let vc = main.instantiateViewController(withIdentifier: "MediaViewController") as! MediaViewController
            vc.urlString = youtube
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            displayMessage(message: localizedSitringFor(key: "emptyLink"), messageError: true)
        }
    }
    
    @IBAction func instaAction(_ sender: Any) {
        if  insta != ""{
            let main = UIStoryboard(name: "Main", bundle: nil)
            let vc = main.instantiateViewController(withIdentifier: "MediaViewController") as! MediaViewController
            vc.urlString = insta
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            displayMessage(message: localizedSitringFor(key: "emptyLink"), messageError: true)
        }
    }
    @IBAction func twiterAction(_ sender: Any) {
        if  twiter != ""{
            let main = UIStoryboard(name: "Main", bundle: nil)
            let vc = main.instantiateViewController(withIdentifier: "MediaViewController") as! MediaViewController
            vc.urlString = twiter
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            displayMessage(message: localizedSitringFor(key: "emptyLink"), messageError: true)
        }

    }
    
    @IBAction func sendAction(_ sender: Any) {
        if validate(){
            sendMessageRequest()
        }
    }
    
}

//MARK:- Helpers
extension ContactUsViewController{
    
    /// init view
    func initView(){
        hideKeyboardWhenTappedAround()
        messageTextField.textColor = UIColor(red: 143/255, green: 143/255, blue: 143/255, alpha: 1)
        messageTextField.text = localizedSitringFor(key: "Message")
        messageTextField.delegate = self
        NETWORK.deleget = self
        getSocialRequest()
        mainQueue {
            self.nameTextField.font = UIFont(name: localizedSitringFor(key: "AppRegularFont"), size: iphoneXFactor*45 )
            self.messageTextField.font = UIFont(name: localizedSitringFor(key: "AppRegularFont"), size: iphoneXFactor*45 )
            self.emailTextField.font = UIFont(name: localizedSitringFor(key: "AppRegularFont"), size: iphoneXFactor*45 )
            self.subjectTextField.font = UIFont(name: localizedSitringFor(key: "AppRegularFont"), size: iphoneXFactor*45 )
            self.phoneTextField.font = UIFont(name: localizedSitringFor(key: "AppRegularFont"), size: iphoneXFactor*45 )
            self.sendButton.titleLabel?.font = UIFont(name: localizedSitringFor(key: "AppBoldFont"), size: iphoneXFactor*50)
        }

    }
    
    
    func validate() -> Bool{
        
        if nameTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty == true {
            displayMessage(message: localizedSitringFor(key: "emptyName"), messageError: true)

            return false
        }
        
        if phoneTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty == true && phoneTextField.text?.count ?? 0 < 8{
            displayMessage(message: localizedSitringFor(key: "emptyPhone"), messageError: true)

            return false
        }

        if !isValidEmail(emailTextField.text ?? "")  {
            displayMessage(message: localizedSitringFor(key: "emptyEmail"), messageError: true)
            return false
        }
        
        if subjectTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty == true {
            displayMessage(message: localizedSitringFor(key: "emptySubject"), messageError: true)

            return false
        }
        
        if messageTextField.textColor == UIColor(red: 143/255, green: 143/255, blue: 143/255, alpha: 1) {
            displayMessage(message: localizedSitringFor(key: "emptyDescription"), messageError: true)

            return false
        }
        
        return true
    }
}


// MARK: - NetworkingHelperDeleget
extension ContactUsViewController: NetworkingHelperDeleget{
    func onHelper(getData data: DataResponse<String>, fromApiName name: String, withIdentifier id: String) {
        if id == CONTACT{handleSendMessage(forResponse: data)}
        else if id == socialInfo {handleSocial(forResponse: data)}
        else if id == LOGOUT{ handleLogout(forResponse: data) }

    }
    
    func onHelper(getError error: String, fromApiName name: String, withIdentifier id: String) {
        displayMessage(message: localizedSitringFor(key: "unknownError"), messageError: true)
    }
    
    /// uses for connecting to server and recive data
    func sendMessageRequest()
    {
        
        var parameters: [String: Any] = [:]
        parameters["email"] = emailTextField.text ?? ""
        parameters["message"] = messageTextField.text ?? ""
        parameters["name"] = nameTextField.text ?? ""
        parameters["subject"] = subjectTextField.text ?? ""
        parameters["number"] = phoneTextField.text ?? ""

        print(parameters)

        NETWORK.connectWithHeaderTo(api: ApiNames.CONTACT_US, withParameters: parameters, andIdentifier : CONTACT, withLoader: true, forController: self, methodType: .post)
    }
    
    /// handle register response from server
    ///
    /// - Parameter response: server response
    func handleSendMessage(forResponse response: DataResponse<String>)
    {
        switch response.response?.statusCode {
        case 200:
                self.navigationController?.popViewController(animated: true)
            displayMessage(message: localizedSitringFor(key: "MessageSentSuccessfully"), messageError: false)
            
        case 207:
            displayMessage(message: localizedSitringFor(key: "UserISNotActive"), messageError: true)
            delay(timeSession) {
                self.logoutRequest()
            }
        case 400:
         displayMessage(message: localizedSitringFor(key: "badRequst"), messageError: true)
        case 401:
            displayMessage(message: localizedSitringFor(key: "Unauthorized"), messageError: true)
        default:
            displayMessage(message: localizedSitringFor(key: "unknownError"), messageError: true)
        }
    }
    
    /// uses for connecting to server and recive data
    func getSocialRequest()
    {
        NETWORK.connectWithHeaderTo(api: ApiNames.CONTACT_US, andIdentifier : socialInfo, withLoader: true, withEncoding: URLEncoding.default, forController: self, methodType: .get)
    }
    
    
    /// handle info response from server
    ///
    /// - Parameter response: server response
    func handleSocial(forResponse response: DataResponse<String>)
    {
        print(response.response?.statusCode, "mmmmmmmmmmmmmmm")
        switch response.response?.statusCode {
        case 200:
            do{
                let sett = try JSONDecoder().decode([ContactUS].self, from: response.data ?? Data())
                print(sett.count)
                for set in sett {
                    if set.name == "facebook"{
                        facebook = set.value ?? ""
                    }
                    
                    if set.name == "youtube"{
                        youtube = set.value ?? ""
                    }
                    
                    if set.name == "twitter"{
                        twiter = set.value ?? ""
                    }
                    if set.name == "instagram"{
                        insta = set.value ?? ""
                    }
                }
                
            }catch let error{
              print("ErrorAlbert: ",error)
              displayMessage(message: localizedSitringFor(key: "unknownError"), messageError: true)
          }
        case 400:
         displayMessage(message: localizedSitringFor(key: "badRequst"), messageError: true)
        case 207:
            displayMessage(message: localizedSitringFor(key: "UserISNotActive"), messageError: true)
            delay(timeSession) {
                self.logoutRequest()
            }

        case 401:
            displayMessage(message: localizedSitringFor(key: "Unauthorized"), messageError: true)
        default:
            displayMessage(message: localizedSitringFor(key: "unknownError"), messageError: true)
        }
        
    }
    
    /// uses for connecting to server and recive data
    func logoutRequest()
    {
        var parameters: [String: Any] = [:]
        parameters["userid"] = Account.shared.id
        parameters["DeviceId"] = deviceID
        
        var url = ApiNames.LOGOUT + "?userid=" + String(Account.shared.id)
        url += "&DeviceId=" + deviceID
        
        NETWORK.connectWithHeaderTo(api: url, withParameters: parameters, andIdentifier: LOGOUT, withLoader: true, forController: self, methodType: .post)
    }
    
    
    /// handle login json response from server
    ///
    /// - Parameter response: server response
    func handleLogout(forResponse response: DataResponse<String>)
    {
        switch response.response?.statusCode {
        case 200:
            Account.shared.logout()
            pushToView(withId: baseSession)
        case 400:
            displayMessage(message: localizedSitringFor(key: "invalidUserOrPassword"), messageError: true)

        case 401:
            displayMessage(message: localizedSitringFor(key: "Unauthorized"), messageError: true)
        default:
            displayMessage(message: localizedSitringFor(key: "unknownError"), messageError: true)
        }
    }

}


//MARK: - text view Delegate
extension ContactUsViewController : UITextViewDelegate
{
    func textViewDidBeginEditing( _ textView: UITextView)
    {
        if messageTextField.textColor ==  UIColor(red: 143/255, green: 143/255, blue: 143/255, alpha: 1)
        {
            messageTextField.text = nil
            messageTextField.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing( _ textView: UITextView)
    {
        if messageTextField.text.isEmpty
        {
            messageTextField.textColor = UIColor(red: 143/255, green: 143/255, blue: 143/255, alpha: 1)
            messageTextField.text = localizedSitringFor(key: "Message")
        }
    }
}
