//
//  MediaViewController.swift
//  ESAAL
//
//  Created by YoussefRomany on 11/9/19.
//  Copyright © 2019 HardTask. All rights reserved.
//

import UIKit
import WebKit


class MediaViewController: UIViewController {

    @IBOutlet var headerView: GradientView!
    @IBOutlet var webView: WKWebView!
    
    var urlString = ""
    var header: HeaderView?
    var headerTitle = localizedSitringFor(key: "contactUs")
    var isFromVideo = false

    override func viewDidLoad() {
        super.viewDidLoad()
         
        let finalOne = urlString.replacingOccurrences(of: "\"", with: "")
        if let url = URL(string: finalOne) {
            print(finalOne)
          webView.load(URLRequest(url: url))
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
         super.viewDidAppear(animated)
         if header == nil{
             header = HeaderView.initView(fromController: self, andView: headerView, andTitle: headerTitle, isFromRoot: true)
             header?.backView.isHidden = false
         }
     }

}
