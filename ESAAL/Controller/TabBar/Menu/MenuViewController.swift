//
//  MenuViewController.swift
//  ESAAL
//
//  Created by Mina Thabet on 11/4/19.
//  Copyright © 2019 HardTask. All rights reserved.
//

import UIKit
import IBAnimatable

class MenuViewController: UIViewController {

      //MARK:- IBOutlet
    @IBOutlet var headerView: UIView!
    @IBOutlet var aboutUsLabel: UILabel!
    @IBOutlet var contactUsLabel: UILabel!
    @IBOutlet var termAndConditionsLabel: UILabel!
    @IBOutlet var termAndConditionsStudentLabel: UILabel!
    @IBOutlet var languageLabel: UILabel!
    @IBOutlet var changeLabel: UILabel!
    @IBOutlet var ArrowImage1View: UIImageView!
    @IBOutlet var ArrowImage2View: UIImageView!
    @IBOutlet var ArrowImage3View: UIImageView!
    @IBOutlet var arrowImageView4: UIImageView!
    @IBOutlet var arrowImageView5: UIImageView!
    @IBOutlet weak var termsConditionTeacher: AnimatableView!
    
    ///variables
    var header: HeaderView!
    var headerIsLoaded = false

    
    //MARK:- App life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        if !headerIsLoaded{
            header = HeaderView.initView(fromController: self, andView: headerView, andTitle: localizedSitringFor(key: "More"), isFromRoot: true)
            headerIsLoaded = true
        }
    }
    
    //MARK:- IBAction
    @IBAction func changePasswordAction(_ sender: Any) {
        if Account.shared.id == 0 {
            //displayAlertConfirmation(localizedSitringFor(key: "Attention"), localizedSitringFor(key: "notLogin")) {
                // go to login if user not login
                pushToView(withId: "LoginViewController")
            //}
            return
        }
        performSegue(withIdentifier: "showChangePassword", sender: nil)
    }
    
    @IBAction func changeLanguageAction(_ sender: Any) {
        changeAppLanguage()
    }
    
    @IBAction func termsAndConditionAction(_ sender: Any) {
        let main = UIStoryboard(name: "Main", bundle: nil)
        let vc = main.instantiateViewController(withIdentifier: "AboutUsViewController") as! AboutUsViewController
        if Account.shared.id == 0 {
            vc.htmlType = HtmlType.All
        }else {
            if Account.shared.isTeacher {
                vc.htmlType = HtmlType.TeatcherTerms
            }else {
                vc.htmlType = HtmlType.StudentTerms
            }
        }
        
        vc.headerTitle = localizedSitringFor(key: "terms")
        present(vc, animated: true)
    }
    
}


//MARK:- Helpers
extension MenuViewController{

    func initView(){
//       loginView.isHidden = Account.shared.id != 0
       setupFont()
        if L102Language.isRTL{

        }else{
            mainQueue {
                self.ArrowImage1View.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
                self.ArrowImage2View.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
                self.ArrowImage3View.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
                self.arrowImageView4.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
                self.arrowImageView5.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
            }
        }
        
    }

    func setupFont() {
        self.aboutUsLabel.font = UIFont(name: localizedSitringFor(key: "AppRegularFont"), size: iphoneXFactor*50 )
        self.contactUsLabel.font = UIFont(name: localizedSitringFor(key: "AppRegularFont"), size: iphoneXFactor*50 )
        self.languageLabel.font = UIFont(name: localizedSitringFor(key: "AppRegularFont"), size: iphoneXFactor*50 )
        self.changeLabel.font = UIFont(name: localizedSitringFor(key: "AppRegularFont"), size: iphoneXFactor*50 )
        self.termAndConditionsLabel.font = UIFont(name: localizedSitringFor(key: "AppRegularFont"), size: iphoneXFactor*50 )
        self.changeLabel.text = localizedSitringFor(key: "Lang")
        if Account.shared.id == 0{
            self.languageLabel.text = localizedSitringFor(key: "login")
        }
    }
    
    /// change current app language
    func changeAppLanguage() {
        //let alert = UIAlertController(title: localizedSitringFor(key: "changeLanguageTitle"), message: localizedSitringFor(key: "changeLanguageBody"), preferredStyle: .alert)
        
        //alert.addAction(UIAlertAction(title: localizedSitringFor(key: "yes"), style: .destructive, handler: { (_) in
            let newLang = L102Language.currentAppleLanguage() == "ar" ? "en" : "ar"
            L102Language.setAppleLAnguageTo(lang: newLang)
            UIView.appearance().semanticContentAttribute = newLang == "ar" ? .forceRightToLeft : .forceLeftToRight
            goToView(withId: "TabBarViewController")
        //}))
        
        //alert.addAction(UIAlertAction(title: localizedSitringFor(key: "no"), style: .cancel, handler: nil))
        
//        mainQueue {
//            self.present(alert, animated: true, completion: nil)
//        }
    }
}
