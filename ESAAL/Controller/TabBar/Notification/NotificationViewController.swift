//
//  NotificationViewController.swift
//  ESAAL
//
//  Created by Mina Thabet on 10/31/19.
//  Copyright © 2019 HardTask. All rights reserved.
//

import UIKit
import Alamofire


class NotificationViewController: UIViewController {

    //MARK:- IBOutlet
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var headerView: UIView!
    
    
    ///variables
    var header: HeaderView!
    var headerIsLoaded = false
    var notifications: [NewNotification] = []
    var page = 1

    ///CONSTANTS
    let NETWORK = NetworkingHelper()
    let GET_NOTIFICATIONS = "GETNOTIFICATIONS"
    let UPDATE_NOTIFICATIONS = "UPDATE_NOTIFICATIONS"
    let LOGOUT = "logout"

    //MARK:- App life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        page = 1
        notifications = []
        getNotificationsRequest()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        if !headerIsLoaded{
            header = HeaderView.initView(fromController: self, andView: headerView, andTitle: localizedSitringFor(key: "Notifications"), isFromRoot: true)
            headerIsLoaded = true
        }
    }


}

// MARK:- Helpers
extension NotificationViewController{
    
    /// init view for first time
    func initView() {
        if Account.shared.id != 0 {
            NETWORK.deleget = self
            setupTableView()
        }
    }
    
    /// init tables view
    func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.estimatedRowHeight = 100
        tableView.register(UINib(nibName: CellIdentifier.QuestionListCell.rawValue, bundle: nil), forCellReuseIdentifier: CellIdentifier.QuestionListCell.rawValue)
        tableView.estimatedRowHeight = 100
    }
    
    
    /// get next page for the list
    ///
    /// - Parameter index: the current cell indexPath
    func getNextPageIfNeeded(fromIndex index: IndexPath) {
        if index.row == notifications.count - 1 {
            page += 1
            getNotificationsRequest()
        }
    }
}


// MARK:- table view delegate and data source
extension NotificationViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notifications.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier.QuestionListCell.rawValue, for: indexPath) as! QuestionListCell
        cell.initCell(withNotification: notifications[indexPath.row])
        getNextPageIfNeeded(fromIndex: indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if notifications[indexPath.row].isQuestion ?? false {
            let main = UIStoryboard(name: "Main", bundle: nil)
            let vc = main.instantiateViewController(withIdentifier: "QuestionAndReplayController") as! QuestionAndReplayController
            vc.questionId = notifications[indexPath.row].requestQuestionId ?? 0
            self.navigationController?.pushViewController(vc, animated: true)
        }

    }
    
}



//MARK:- Networking
extension NotificationViewController: NetworkingHelperDeleget{
    func onHelper(getData data: DataResponse<String>, fromApiName name: String, withIdentifier id: String) {
        if id == GET_NOTIFICATIONS { handleNotifications(forResponse: data) }
        else if id == UPDATE_NOTIFICATIONS {handleUpdateNotifications(forResponse: data)}
        else if id == LOGOUT{ handleLogout(forResponse: data) }

    }
    
    func onHelper(getError error: String, fromApiName name: String, withIdentifier id: String) {
        displayMessage(message: localizedSitringFor(key: "unknownError"), messageError: true)
    }
    
    
    /// uses for connecting to server and recive data
    func getNotificationsRequest()
    {
        var parameters: [String: Any] = [:]
        parameters["userid"] = Account.shared.id
        parameters["pageNum"] = page
        print(parameters)
        NETWORK.connectWithHeaderTo(api: ApiNames.GET_USER_NOTIFICATIONS+"?userid=\(Account.shared.id)&pageNum=\(page)", andIdentifier: GET_NOTIFICATIONS, withLoader: true, forController: self, methodType: .get)
    }
    
    
    /// handle notifications json response from server
    ///
    /// - Parameter response: server response
    func handleNotifications(forResponse response: DataResponse<String>)
    {
        switch response.response?.statusCode {
        case 200:
            do{
                let notification = try JSONDecoder().decode([NewNotification].self, from: response.data ?? Data())
                if notification.count == 0{
                    return
                }
                self.notifications += notification
                mainQueue {
                    self.tableView.reloadData()
                }
                updateNotificationsRequest()
          }catch let error{
            print("error", error)
              displayMessage(message: localizedSitringFor(key: "unknownError"), messageError: true)
          }
        case 203:
             displayMessage(message: localizedSitringFor(key: "UserNotFound"), messageError: true)
        case 204:
             displayMessage(message: localizedSitringFor(key: "emptyNotifications"), messageError: false)
        case 207:
            displayMessage(message: localizedSitringFor(key: "UserISNotActive"), messageError: true)
            delay(timeSession) {
                self.logoutRequest()
            }
        case 400:
            displayMessage(message: localizedSitringFor(key: "badRequst"), messageError: true)
        case 401:
            displayMessage(message: localizedSitringFor(key: "Unauthorized"), messageError: true)
        default:
            displayMessage(message: response.description, messageError: true)
        }
        hideLoaderForController(self)
    }
    
    
    /// uses for connecting to server and recive data
    func updateNotificationsRequest()
    {
        var parameterss: [String: Any] = [:]
        parameterss["userid"] = Account.shared.id
        print("wdedwedwed", Account.shared.id)
        NETWORK.connectWithHeaderTo(api: ApiNames.UPDATE_USER_NOTIFICATIONS+"?userid=\(Account.shared.id)",withParameters:  parameterss, andIdentifier: UPDATE_NOTIFICATIONS, withLoader: false, forController: self, methodType: .post)
    }
    
    /// handle update notifications json response from server
    ///
    /// - Parameter response: server response
    func handleUpdateNotifications(forResponse response: DataResponse<String>)
    {
        switch response.response?.statusCode {
        case 200:
            removeRedDotAtTabBarItemIndex(parant: self, index: 2)
            print(response.description, "xasxasxa")
        case 203:
                displayMessage(message: localizedSitringFor(key: "UserNotFound"), messageError: true)
        case 207:
            displayMessage(message: localizedSitringFor(key: "UserISNotActive"), messageError: true)
            delay(timeSession) {
                self.logoutRequest()
            }
        case 400:
            displayMessage(message: localizedSitringFor(key: "badRequst"), messageError: true)
        case 401:
            displayMessage(message: localizedSitringFor(key: "Unauthorized"), messageError: true)
        default:
            displayMessage(message: response.description, messageError: true)
        }
    }

    
    /// uses for connecting to server and recive data
    func logoutRequest()
    {
        var parameters: [String: Any] = [:]
        parameters["userid"] = Account.shared.id
        parameters["DeviceId"] = deviceID
        
        var url = ApiNames.LOGOUT + "?userid=" + String(Account.shared.id)
        url += "&DeviceId=" + deviceID
        
        NETWORK.connectWithHeaderTo(api: url, withParameters: parameters, andIdentifier: LOGOUT, withLoader: true, forController: self, methodType: .post)
    }
    
    
    /// handle login json response from server
    ///
    /// - Parameter response: server response
    func handleLogout(forResponse response: DataResponse<String>)
    {
        switch response.response?.statusCode {
        case 200:
            Account.shared.logout()
            pushToView(withId: baseSession)
        case 400:
            displayMessage(message: localizedSitringFor(key: "invalidUserOrPassword"), messageError: true)

        case 401:
            displayMessage(message: localizedSitringFor(key: "Unauthorized"), messageError: true)
        default:
            displayMessage(message: localizedSitringFor(key: "unknownError"), messageError: true)
        }
    }
}
