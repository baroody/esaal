//
//  AppDelegate.swift
//  ESAAL
//
//  Created by Mina Thabet on 10/28/19.
//  Copyright © 2019 HardTask. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import Alamofire
import NotificationCenter
import UserNotifications


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    var pushData : NSDictionary?
    
    var pushId : String = ""
    var pushType : String = ""

    let NETWORK = NetworkingHelper()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        IQKeyboardManager.shared.enable = true
        L102Localizer.DoTheMagic()
        
        NETWORK.deleget = self

        
        if (UserDefaults.standard.string(forKey: "deviceToken")==nil) {
            UserDefaults.standard.set("", forKey: "deviceToken")
            UserDefaults.standard.synchronize()
        }
        
        // push notification
        if #available(iOS 10, *) {
            UNUserNotificationCenter.current().requestAuthorization(options:[.badge, .alert, .sound]){ (granted, error) in }
            application.registerForRemoteNotifications()
            
        }
            // iOS 9 support
        else {
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.badge, .sound, .alert], categories: nil))
            UIApplication.shared.registerForRemoteNotifications()
        }
        
        let center = UNUserNotificationCenter.current()
        let options: UNAuthorizationOptions = [.alert, .sound, .badge];
        
        center.requestAuthorization(options: options) {
            (granted, error) in
            if !granted {
                print("push now allowed, show message if you want")
            } else {
                print("push allowed")
            }
        }
        
        center.getNotificationSettings { (settings) in
            switch settings.soundSetting {
            case .enabled:
                print("enabled sound setting")
                
            case .disabled:
                print("setting has been disabled")
                
            case .notSupported:
                print("something vital went wrong here")
            }
        }
        
        if let launchOptions = launchOptions {
            if let info = launchOptions[UIApplication.LaunchOptionsKey.remoteNotification] {
                let dic = info as! NSDictionary
                print("dic from closed app",dic)
                PushNotificationResponse(Data: dic)
            }
        }
        
        //if Account.shared.Token != ""{
        if !Account.shared.isTeacher && !Account.shared.isSubscribe && Account.shared.id != 0{
            goToStudentPackage()
        }else{
            resetRoot()
        }

        // ##fapa##.
        // this is alternate to send push. Run app and after 5 seconds, testPush will get called which will be same like u click on push.
       //Timer.scheduledTimer(timeInterval: 3.00, target: self, selector: #selector(testPush), userInfo: nil, repeats: false)
        getAppSetting()
        return true
    }
    
    
    @objc func testPush() {
        
        let mDic : Dictionary<String, Any> = ["aps" : ["alert": "Enter message", "badge" : 1], "T" : "Q", "I" : "11"]
        
        PushNotificationResponse(Data: mDic as NSDictionary)
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        print("didRegisterForRemoteNotificationsWithDeviceToken")
        
        // Convert token to string
        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        
        // Print it to console
        print("APNs device token: \(deviceTokenString)")
        // Persist it in your backend in case it's new
        
        //        GlobalViewController().AddToken(DeviceToken:deviceTokenString,completion: { (json) in
        //            print("AddToken",json)
        //
        //        })
        
        UserDefaults.standard.set(deviceTokenString, forKey: "deviceToken")
        UserDefaults.standard.synchronize()
        
        addTokenNow()
        
    }
    
    func addTokenNow() {
        
      //  ##fapa##
        getTokenRequest()
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        // Print the error to console (you should alert the user that registration failed)
        print("APNs registration failed: \(error)")
        UserDefaults.standard.set("", forKey: "deviceToken")
        UserDefaults.standard.synchronize()
    }
    
    // Push notification received
    func application(_ application: UIApplication, didReceiveRemoteNotification data: [AnyHashable : Any]) {
        // Print notification payload data
        print("Push notification received: \(data)==\(application.applicationState)")
        
        switch application.applicationState {
        case .active:
            print("active mode")
            // PushNotificationResponse(Data:data as NSDictionary)
            
            break
        case .inactive:
            print("inactive mode")
            PushNotificationResponse(Data:data as NSDictionary)
            
            break
        case .background:
            print("background mode")
            PushNotificationResponse(Data:data as NSDictionary)
            break
        default:
            break
        }
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        print("didRec 001")
        let userInfo = response.notification.request.content.userInfo
        PushNotificationResponse(Data:userInfo as NSDictionary)
    }
    
    public func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Swift.Void){
        print("didRec 002")
        completionHandler([.alert, .badge, .sound])
    }
    
    func PushNotificationResponse(Data:NSDictionary){
        
        pushData = Data 
        
        print("PushNotificationResponse==\(pushData!)")
        
        print("faapaa\(Data["T"] as! String)")
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) { // change 2 to desired number of seconds
            
            // ##fapa##
            // Below I & T will change in self.pushData["
            // Note : in testPush I have kept I & T as variable for test
            
            var tId = self.pushData!["I"] as! String
            self.pushId = "\(tId)"
            
            tId = self.pushData!["T"] as! String
            self.pushType = "\(tId)"
            
            print("ppp==\(self.pushId)==\(self.pushType)")
            
            if self.pushType == "Q"{
                /// go to question details screen
                 let main = UIStoryboard(name: "Main", bundle: nil)
                 let vc = main.instantiateViewController(withIdentifier: "QuestionAndReplayController") as! QuestionAndReplayController
                vc.questionId = Int(self.pushId ) ?? 0
                 
                 UIApplication.getTopMostViewController()?.present(vc, animated: true, completion: nil)

            }else {
                /// this notification is not question
            }
            
            UIApplication.shared.applicationIconBadgeNumber = 0
            
            // ##fapa##
            // based on pushType write code below.
            // e.x. if pushType=product, go to product details screen. check pushType with web developer
            
        }
        
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    
}


extension AppDelegate{
    func resetRoot() {
        guard let rootVC = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TabBarViewController") as? TabBarViewController else {
            return
        }
        
        UIApplication.shared.windows.first?.rootViewController = rootVC
        UIApplication.shared.windows.first?.makeKeyAndVisible()
        
    }
    func goToStudentPackage() {
        guard let rootVC = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "StudentRegisterPackageController") as? StudentRegisterPackageController else {
            return
        }
        rootVC.isFromRegister = true
        UIApplication.shared.windows.first?.rootViewController = rootVC
        UIApplication.shared.windows.first?.makeKeyAndVisible()
    }
}



extension UIApplication {
    class func getTopMostViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return getTopMostViewController(base: nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return getTopMostViewController(base: selected)
            }
        }
        if let presented = base?.presentedViewController {
            return getTopMostViewController(base: presented)
        }
        return base
    }
}



// MARK: - NetworkingHelperDeleget
extension AppDelegate: NetworkingHelperDeleget{
    func onHelper(getData data: DataResponse<String>, fromApiName name: String, withIdentifier id: String) {
        if id == "addToken" { handleToken(forResponse: data) }
        else if id == "appSetting" { handleAppSetting(forResponse: data) }
        
    }

    //MARK: - onHelper
    func onHelper(getError error: String, fromApiName name: String, withIdentifier id: String) {
        //failure code
//        self.view.makeToast("unknownErrorOccurred".localized)
        print("uybubuybuybuy")
    }
    //MARK: - GetCurrentStoreRequest
    func getTokenRequest(){
        NETWORK.deleget = self
        NETWORK.connectWithHeaderTo(api: ApiNames.TOKEN + "?userid=\(Account.shared.id)&DeviceType=1&token=\(UserDefaults.standard.string(forKey: "deviceToken") ?? "")&DeviceId=\(deviceID)",andIdentifier: "addToken" ,withLoader: false, methodType: .post)
    }
    
    func getAppSetting() {
        NETWORK.deleget = self
        NETWORK.connectWithHeaderTo(api: ApiNames.Appsetting,andIdentifier: "appSetting" ,withLoader: false, methodType: .get)

    }
    
    //MARK: - HandleCurrentStore
    func handleToken(forResponse response: DataResponse<String>){
            print("dddddddddddddddddddddddddddddddddddddddddd")
            print("token_added")
            print(response.description)
    }
    
    func handleAppSetting(forResponse response: DataResponse<String>){
        switch response.response?.statusCode {
        case 200:
                 do{
                      let appSetting = try JSONDecoder().decode([AppSetting].self, from: response.data ?? Data())
                    for item in appSetting {
                        switch item.name {
                        case "AppleTest":
                            print("appel test",item.value == "on" ? false : true )
                            Account.shared.showPayment = item.value == "on" ? false : true
                            Account.shared.storeData()
                        case "AppVersion":
                            print("app version",item.value ?? "no data found")
                            Account.shared.onlineAppVersion = item.value ?? ""
                            Account.shared.storeData()
                        default:
                            print("not defiend")
                        }
                        
                        print("showPayment",Account.shared.showPayment)
                        print("onlineAppVersion",Account.shared.onlineAppVersion)
                    }
                  }catch let error{
                    print("app setting parsing error",error)
                }
        default:
            print("app setting response",response.response?.statusCode ?? 0)
        }
    }

}
