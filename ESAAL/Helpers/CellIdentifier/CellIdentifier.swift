//
//  CellIdentifier.swift
//  ESAAL
//
//  Created by Mina Thabet on 10/9/19.
//  Copyright © 2019 HardTask. All rights reserved.
//

import Foundation

enum CellIdentifier: String {
    case ChoosePackageCell = "ChoosePackageCell"
    case QuestionListCell = "QuestionListCell"
    case AccountSeetingCell = "AccountSeetingCell"
    case MyAccountCenterCell = "MyAccountCenterCell"
    case PaymentHistoryCell = "PaymentHistoryCell"
    case EmptyQuestionCell = "EmptyQuestionCell"
    case StudingSubjectCell = "StudingSubjectCell"
    case FilterCell = "FilterCell"
    case QuestionMediaCell = "QuestionMediaCell"
    case TeacherHomeTableViewCell = "TeacherHomeTableViewCell"
    case QuestionHeaderCell = "QuestionHeaderCell"
    case QuestionReplayCell = "QuestionReplayCell"
    case HeaderLabelCell = "HeaderLabelCell"
}
