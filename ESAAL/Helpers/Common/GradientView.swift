//
//  GradientView.swift
//  ESAAL
//
//  Created by Mina Thabet on 2/15/19.
//  Copyright © 2019 HardTask. All rights reserved.
//

import UIKit

@IBDesignable
class GradientView: UIView {
    
    @IBInspectable var startColor:   UIColor = .black { didSet { updateColors() }}
    @IBInspectable var endColor:     UIColor = .white { didSet { updateColors() }}
    @IBInspectable var startColorAlpha: CGFloat = 1.0 { didSet { updateColors() }}
    @IBInspectable var endColorAlpha: CGFloat = 1.0 { didSet { updateColors() }}
    @IBInspectable var startLocation: Double =   0.05 { didSet { updateLocations() }}
    @IBInspectable var endLocation:   Double =   0.95 { didSet { updateLocations() }}
    @IBInspectable var horizontalMode:  Bool =  false { didSet { updatePoints() }}
    @IBInspectable var diagonalMode:    Bool =  false { didSet { updatePoints() }}
    @IBInspectable var borderRadius:CGFloat  =  0.0   { didSet { updateBorder() }}
    //@IBInspectable var borderColor: UIColor  = .clear { didSet { updateBorderColor() }}
    
    override class var layerClass: AnyClass { return CAGradientLayer.self }
    
    var gradientLayer: CAGradientLayer { return layer as! CAGradientLayer }
    
    func updatePoints() {
        if horizontalMode {
            gradientLayer.startPoint = diagonalMode ? CGPoint(x: 1, y: 0) : CGPoint(x: 0, y: 0.5)
            gradientLayer.endPoint   = diagonalMode ? CGPoint(x: 0, y: 1) : CGPoint(x: 1, y: 0.5)
        } else {
            gradientLayer.startPoint = diagonalMode ? CGPoint(x: 0, y: 0) : CGPoint(x: 0.5, y: 0)
            gradientLayer.endPoint   = diagonalMode ? CGPoint(x: 1, y: 1) : CGPoint(x: 0.5, y: 1)
        }
    }
    func updateLocations() {
        gradientLayer.locations = [startLocation as NSNumber, endLocation as NSNumber]
    }
    func updateColors() {
        gradientLayer.colors    = [startColor.withAlphaComponent(startColorAlpha).cgColor, endColor.withAlphaComponent(endColorAlpha).cgColor]
    }
    
    func updateBorder(){
        self.layer.cornerRadius = borderRadius
        self.layer.masksToBounds = false
    }
    
    //    func updateBorderColor(){
    //        self.layer.borderWidth = 2
    //        self.layer.borderColor = borderColor.cgColor
    //    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        updatePoints()
        updateLocations()
        updateColors()
        updateBorder()
    }
    
}
