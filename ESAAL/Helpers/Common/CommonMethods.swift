//
//  CommonMethods.swift
//  ESAAL
//
//  Created by Mina Thabet on 10/22/18.
//  Copyright © 2018 HardTask. All rights reserved.
//

import UIKit
import SystemConfiguration
import IBAnimatable
import Alamofire
import SwiftMessages
import SkyFloatingLabelTextField
import UIKit
import AVKit


extension UIImage {
    class func videoPreviewImage(url: URL) -> UIImage? {
        let asset = AVAsset(url: url)
        let assetImgGenerate = AVAssetImageGenerator(asset: asset)
        assetImgGenerate.appliesPreferredTrackTransform = true
        //Can set this to improve performance if target size is known before hand
        //assetImgGenerate.maximumSize = CGSize(width,height)
        let time = CMTimeMakeWithSeconds(1.0, preferredTimescale: 600)
        do {
            let img = try assetImgGenerate.copyCGImage(at: time, actualTime: nil)
            let thumbnail = UIImage(cgImage: img)
            return thumbnail
        } catch {
            print(error.localizedDescription)
            return nil
        }
    }
}

public var screenWidth:CGFloat { get { return UIScreen.main.bounds.size.width } }
public var screenHeight:CGFloat { get { return UIScreen.main.bounds.size.height } }
public var isIpad: Bool { get { return UIScreen.main.traitCollection.horizontalSizeClass == .regular } }
public var iphoneXFactor:CGFloat { get {return ((screenWidth * 1.00) / 1080.0)} }
public var deviceID:String { get { return (UIDevice.current.identifierForVendor?.uuidString) ?? "" } }
public var appVersion:String { get { return Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? ""} }

public var baseSession = "TabBarViewController"
public var timeSession = 3.0


//MARK: - corner radius
func cornerRadius(view: UIView ,radi: Float){
    view.layer.cornerRadius = CGFloat(Float(view.layer.bounds.height) / (radi))
}

//MARK: - Make Shadow
func makeShadow(view: UIView , shadowRadius: Double , shadowOpacity: Double){
    view.layer.shadowColor = UIColor.black.cgColor
    view.layer.shadowOffset = CGSize(width: 0, height: 1)
    view.layer.shadowRadius = CGFloat(shadowRadius)
    view.layer.shadowOpacity = Float(shadowOpacity)
}

//MARK: - call number
public func makeCall(forNumber number:String)->Bool {
    let url:URL = URL(string:"tel://\(number)")!
    if UIApplication.shared.canOpenURL(url) {
        UIApplication.shared.openURL(url)
        return true
    }
    return false
}

//MARK: - copy string
public func makeCopy(ofString string:String){
    UIPasteboard.general.string = string
}


//MARK: - open url
public func ESOpen(url:URL)->Bool{
    //check if the url is valid
    if UIApplication.shared.canOpenURL(url) == true {
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
        return true
    }
    //else will return false
    return false
}


/// share items like string or images to all available social media
///
/// - Parameters:
///   - items: the items to share
///   - controller: the controller that will be responsable for displaying the ActivityController
///   - types: the excluded activity types, default value is nil
func share(items:[Any], forController controller:UIViewController, excludedActivityTypes types:[UIActivity.ActivityType]? = nil) {
    let activityViewController = UIActivityViewController(activityItems: items, applicationActivities: nil)
    activityViewController.popoverPresentationController?.sourceView = controller.view // so that iPads won't crash
    
    // exclude some activity types from the list (optional)
    activityViewController.excludedActivityTypes = types
    
    // present the view controller
    controller.present(activityViewController, animated: true, completion: nil)
}

public func handleLocaliztion(ForTextField textField: SkyFloatingLabelTextField, withPlaceholder placeholder: String, andSelectedTitle selectedTitle: String = "", andTitle title: String = "")
{
    textField.placeholder = placeholder
    textField.selectedTitle = selectedTitle != "" ? selectedTitle : placeholder
    textField.title = selectedTitle != "" ? title : placeholder
    
}

//MARK:- change current app language
public func changeAppLanguage(controller: UIViewController,resetControllerId: String) {
       let alert = UIAlertController(title: localizedSitringFor(key: "changeLanguageTitle"), message: localizedSitringFor(key: "changeLanguageBody"), preferredStyle: .alert)
       
       alert.addAction(UIAlertAction(title: localizedSitringFor(key: "yes"), style: .destructive, handler: { (_) in
           let newLang = L102Language.currentAppleLanguage() == "ar" ? "en" : "ar"
           L102Language.setAppleLAnguageTo(lang: newLang)
           UIView.appearance().semanticContentAttribute = newLang == "ar" ? .forceRightToLeft : .forceLeftToRight
           pushToView(withId: resetControllerId)
           //goToView(withId: resetControllerId)
       }))
       
       alert.addAction(UIAlertAction(title: localizedSitringFor(key: "no"), style: .cancel, handler: nil))
       
       mainQueue {
           controller.present(alert, animated: true, completion: nil)
       }
   }

//MARK:- Tab bar
public func addRedDotAtTabBarItemIndex(parant: UIViewController,index: Int) {
    for subview in parant.tabBarController!.tabBar.subviews {

        if let subview = subview as? UIView {
            if subview.tag == 1234 {
                subview.removeFromSuperview()
                break
            }
        }
    }

    let RedDotRadius: CGFloat = 5
    let RedDotDiameter = RedDotRadius * 2

    let TopMargin:CGFloat = 8

    let TabBarItemCount = CGFloat(parant.tabBarController!.tabBar.items!.count)

    let screenSize = UIScreen.main.bounds
    let HalfItemWidth = (screenSize.width) / (TabBarItemCount * 2)

    let  xOffset = HalfItemWidth * CGFloat(index * 2 + 1)

    let imageHalfWidth: CGFloat = (parant.tabBarController!.tabBar.items![index] as! UITabBarItem).selectedImage!.size.width / 2

    let redDot = UIView(frame: CGRect(x: xOffset + imageHalfWidth - 7, y: TopMargin, width: RedDotDiameter, height: RedDotDiameter))

    redDot.tag = 1234
    redDot.backgroundColor = UIColor.red
    redDot.layer.cornerRadius = RedDotRadius

        parant.tabBarController?.tabBar.addSubview(redDot)

}

public func removeRedDotAtTabBarItemIndex(parant: UIViewController,index: Int) {
    for subview in parant.tabBarController!.tabBar.subviews {

        if let subview = subview as? UIView {
            if subview.tag == 1234 {
                subview.removeFromSuperview()
                break
            }
        }
    }
}


/// flip any view 180 degree in arabic language
///
/// - Parameter view: any view to flip
public func flip(view:UIView){
    if UIApplication.isRTL()
    {
        UIView.animate(withDuration: 0.25, animations: {
            view.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
        })
    }
}
//MARK:-
///uses to handling starView to set star view rating,starSize ,filledColort , emptyBorderColor and to get raing after user set rate
//func handleStarView(forView rateView: CosmosView, withRate rate: Double,isTouch:Bool)
//{
//    rateView.settings.updateOnTouch = isTouch
//    rateView.settings.totalStars = 5
//    rateView.settings.fillMode = .full
//    rateView.rating = Double(rate)
//    rateView.settings.starSize = Double(screenWidth * 0.09)
//    rateView.settings.starMargin = 1.0
//}

//MARK: - dates

//get current date
public func getCurrentDate()->String{
    let format = DateFormatter()
    format.dateFormat = "dd-MM-yyyy-hh:mm:ss"
    format.locale = Locale(identifier: "en")
    let date = format.string(from: Date())
    return date
}

public func getDayName(_ dateString:String,formatString:String) -> String{
    if let date = getDateFromStringWithFormat(dateString, formatString: formatString) {
        let format = DateFormatter()
        format.dateFormat = "EE"
        return format.string(from: date)
    }
    return ""
}

// get Date And Time As String From String With Format
public func getDateAndTimeAsStringFromStringWithFormat(_ dateString:String,formatString:String)->(date:String,time:String)? {
    var output:(date:String,time:String)
    
    if let date = getDateFromStringWithFormat(dateString, formatString: formatString) {
        let format = DateFormatter()
        format.dateFormat = "d MMMM YYYY"
        
        if getCurrentDateWithFormat("d MMMM YYYY") == format.string(from: date) {
            output.date = "Today"
        }else{
            output.date = format.string(from: date)
        }
        
        format.dateFormat = "EEEE, h:mm a"
        format.amSymbol = "am"
        format.pmSymbol = "pm"
        
        output.time = format.string(from: date)
        return output
    }
    
    return nil
}

// get Date And Time As String From String With Format
public func getDateTimeAsStringFromStringWithFormat(_ dateString:String,formatString:String)-> String {
    
    var dateConvert = ""
    if let date = getDateFromStringWithFormat(dateString, formatString: formatString) {
        let format = DateFormatter()
        format.dateFormat = "dd-MMM-YYYY  hh:mm a"
       
        dateConvert = format.string(from: date)

        return dateConvert
    }
    
    return dateConvert
}


// get Current Date With Format
public func getCurrentDateWithFormat(_ formatString:String)->String{
    let format = DateFormatter()
    format.dateFormat = formatString
    format.locale = Locale(identifier: "en")
    let date = format.string(from: Date())
    return date
}

// get Date From String With Format
public func getDateFromStringWithFormat(_ dateString:String,formatString:String)->Date? {
    
    let format = DateFormatter()
    format.locale = Locale(identifier: "en")
    format.dateFormat = formatString
    let date = format.date(from: dateString)
    //        print("getDateFromStringWithFormat : \(date)")
    return date
}

public func UTCToLocal(date:String,localForamt: String = "yyyy-MM-dd HH:mm:ss.SSSS",returnFormat : String) -> String {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = localForamt
    dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
    dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
    
    if let dt = dateFormatter.date(from: date) {
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = returnFormat
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
           
        return dateFormatter.string(from: dt)
    }
   return ""
}



//MARK: - images

/// convert image file to base64
///
/// - Parameter image: image file
/// - Returns: image string base64
public func convertImageToBase64(image: UIImage) -> String {
    let imageData = image.jpegData(compressionQuality: 0.5)
    //        strBase64 = (imageData?.base64EncodedString(options: .lineLength64Characters))!
    return  (imageData?.base64EncodedString(options: Data.Base64EncodingOptions.lineLength64Characters))!
}


//capture image for view
public func captureImageForView(_ _view:UIView)->UIImage {
    let frame = CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight)
    UIGraphicsBeginImageContextWithOptions(frame.size, false, UIScreen.main.scale)
    _view.layer.render(in: UIGraphicsGetCurrentContext()!)
    let image = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    return image!
}

//MARK: - alerts

// display alert
public func displayAlert(_ title: String,_ messeg:String,forController controller:UIViewController? = nil){
    guard let controller = controller ?? getCurrentViewController() else {
        return
    }
    let alert = UIAlertController(title: title, message: messeg, preferredStyle: UIAlertController.Style.alert)
    alert.addAction(UIAlertAction(title: localizedSitringFor(key: "yes"), style: UIAlertAction.Style.default, handler: nil))
    DispatchQueue.main.async(execute: {
        controller.present(alert, animated: true, completion: nil)
    })
}

// display alert with action
public func displayAlertWithAction(_ messeg:String,forController controller:UIViewController? = nil,_ Closure: @escaping () -> () ){
    guard let controller = controller ?? getCurrentViewController() else {
        return
    }
    let alert = UIAlertController(title: "", message: messeg, preferredStyle: UIAlertController.Style.alert)
    alert.addAction(UIAlertAction(title: localizedSitringFor(key: "ok"), style: UIAlertAction.Style.default, handler: {(action) in Closure()  }))
    DispatchQueue.main.async(execute: {
        controller.present(alert, animated: true, completion: nil)
    })
}

// display alert with action
public func displayAlertConfirmation(_ title: String,_ body:String,forController controller:UIViewController? = nil,_ Closure: @escaping () -> () ){
    guard let controller = controller ?? getCurrentViewController() else {
        return
    }
    let alert = UIAlertController(title: title, message: body, preferredStyle: UIAlertController.Style.alert)
    alert.addAction(UIAlertAction(title: localizedSitringFor(key: "yes"), style: .default, handler: {(action) in Closure()  }))
    alert.addAction(UIAlertAction(title: localizedSitringFor(key: "no"), style: .default, handler: nil))
    DispatchQueue.main.async(execute: {
        controller.present(alert, animated: true, completion: nil)
    })
}


//MARK: - internet

//check internet connection
public func isConnectedToNetwork() -> Bool {
    var zeroAddress = sockaddr_in()
    zeroAddress.sin_len = UInt8(MemoryLayout<sockaddr_in>.size)
    zeroAddress.sin_family = sa_family_t(AF_INET)
    
    guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
        $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
            SCNetworkReachabilityCreateWithAddress(nil, $0)
        }
    }) else {
        return false
    }
    
    var flags: SCNetworkReachabilityFlags = []
    if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
        return false
    }
    
    let isReachable = flags.contains(.reachable)
    let needsConnection = flags.contains(.connectionRequired)
    
    return (isReachable && !needsConnection)
}

// MARK: - loader
public func showLoaderForController(_ controller:UIViewController){
    DispatchQueue.main.async(execute: {
        let loader = Bundle.main.loadNibNamed("LoaderView", owner: controller, options: nil)?.last as! LoaderView
        loader.frame = CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight)
        loader.tag = 4000
        controller.view.addSubview(loader)
        loader.showLoader()
    })
}



public func hideLoaderForController(_ controller:UIViewController){
    DispatchQueue.main.async(execute: {
        if let view = controller.view.viewWithTag(4000) {
            view.removeFromSuperview()
        }
    })
}




/// Makes a progress bar with tag 5000
///   Don't call before making your loader
///
/// - Parameter controller: The view controller to present the bar on
public func makeProgressBar(_ controller : UIViewController) {
    // After this (to wait for loader creation)
    mainQueue {
        // Get loader
        let loader = controller.view.viewWithTag(4000)
        
        // Create a progress bar
        let progressBar = UIProgressView(progressViewStyle: .default)
        
        // Layout the progress bar
        progressBar.translatesAutoresizingMaskIntoConstraints = false
        progressBar.isHidden = false
        loader?.addSubview(progressBar)
        NSLayoutConstraint(item: progressBar, attribute: .centerX, relatedBy: .equal, toItem: loader, attribute: .centerX, multiplier: 1, constant: 0).isActive = true
        NSLayoutConstraint(item: progressBar, attribute: .centerY, relatedBy: .equal, toItem: loader, attribute: .centerY, multiplier: 1, constant: 64).isActive = true
        NSLayoutConstraint(item: progressBar, attribute: .width, relatedBy: .equal, toItem: loader, attribute: .width, multiplier: 0.2, constant: 0).isActive = true
        
        // Give tag to use outside this function
        progressBar.tag = 5000
        
        // Prepare progress bar
        progressBar.setProgress(0, animated: false)
    }
    
}



//MARK: - Circle view
// to make an image in circle
func makecircle(image: UIView){// UIImageView){
    image.layer.cornerRadius = image.frame.size.width/2
    image.clipsToBounds = true
    //    image.layer.borderWidth = 3
    //    image.layer.borderColor = UIColor.white.cgColor
}


func makecircleButton(but: UIButton){
    but.layer.cornerRadius = but.bounds.size.width * 0.5
    but.clipsToBounds = true
}



//MARK: - validation


//validate email
public func isValidEmail(_ testStr:String) -> Bool {
    let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
    
    let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
    let result = emailTest.evaluate(with: testStr)
    return result
}


public func validateString(_ text:String?)->String {
    return text ?? ""
}

//Validate optional int
public func validateInt(_ val:Int?)->Int {
   return val ?? 0
}


 /* use this method to be sure that this image url is a valid url string that contain "http://" or "https://"

 - Parameter string: the image url that you want to validate
 - Returns: a valid image url as String
 */
public func getValidImage(fromString string:String)->String {
    if string.contains("http://") || string.contains("https://") {
        return string
    }
    return Config.BASIMAGEURL + string
}


// MARK: - Validation with effects


func animateValidationError(errorText : UILabel) {
    errorText.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
    UIView.animate(withDuration: 0.2, animations: {
        errorText.transform = CGAffineTransform(scaleX: 1, y: 1)
    }, completion: { _ in
        DispatchQueue.main.asyncAfter(deadline: DispatchTime(uptimeNanoseconds: DispatchTime.now().uptimeNanoseconds + (3 * NSEC_PER_SEC)), execute: {
            UIView.animate(withDuration: 0.2, animations: {
                errorText.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
            }, completion: { _ in
                errorText.removeFromSuperview()
            })
        })
    })
}

func revealValidationError(over view : UIView, superView : UIView, text : String) {
    let errorText = UILabel()
    errorText.textColor = UIColor(white: 0, alpha: 0.8)
    errorText.text = text
    errorText.font = UIFont(name: "Helvetica Neue", size: 17)
    errorText.translatesAutoresizingMaskIntoConstraints = false
    errorText.isOpaque = true
    errorText.isHidden = false
    superView.addSubview(errorText)
    NSLayoutConstraint(item: errorText, attribute: .centerX, relatedBy: .equal, toItem: view, attribute: .centerX, multiplier: 1, constant: 0).isActive = true
    NSLayoutConstraint(item: errorText, attribute: .bottom, relatedBy: .equal, toItem: view, attribute: .top, multiplier: 1, constant: -8).isActive = true
    animateValidationError(errorText: errorText)
}




//MARK: - dispatch queues
//delay to time
public func delay(_ delay: Double, closure: @escaping ()->()) {
    DispatchQueue.main.asyncAfter(
        deadline: DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: closure)
}

public func mainQueue(_ closure: @escaping ()->()){
    DispatchQueue.main.async(execute: closure)
}

public func backgroundQueue(_ closure: @escaping ()->()){
    DispatchQueue.global(qos: .background).async {
        closure()
    }
}





//MARK: - active controller
// Returns the most recently presented UIViewController (visible)
public func getCurrentViewController() -> UIViewController? {
    
    // we must get the root UIViewController and iterate through presented views
    if let rootController = UIApplication.shared.keyWindow?.rootViewController {
        
        var currentController: UIViewController! = rootController
        
        // Each ViewController keeps track of the view it has presented, so we
        // can move from the head to the tail, which will always be the current view
        while( currentController.presentedViewController != nil ) {
            
            currentController = currentController.presentedViewController
        }
        return currentController
    }
    
    return nil
}

func goToView(withId:String, andStoryboard story:String = "Main", fromController controller:UIViewController? = nil) {
    let board = UIStoryboard(name: story, bundle: nil)
    let control = controller ?? getCurrentViewController() ?? UIViewController()
    control.present(board.instantiateViewController(withIdentifier: withId), animated: true, completion: nil)
}

/// go to view controller and make it the root view
///
/// - Parameter withId: the detination view controller id
func pushToView(withId:String){
    let rootviewcontroller: UIWindow = ((UIApplication.shared.delegate?.window)!)!
    let storyboard = UIStoryboard(name: "Main", bundle: nil)
    rootviewcontroller.rootViewController = storyboard.instantiateViewController(withIdentifier: withId)
    let mainwindow = (UIApplication.shared.delegate?.window!)!
    UIView.transition(with: mainwindow, duration: 0.5, options: [.transitionFlipFromLeft], animations: nil, completion: nil)
}

public func goNavigation(controllerId: String, controller: UIViewController){
    
    let main = UIStoryboard(name: "Main", bundle: nil)
    let vc = main.instantiateViewController(withIdentifier: controllerId)
    controller.navigationController?.pushViewController(vc, animated: true)
    
}

//MARK: - Localization and languages
public func localizedSitringFor(key:String)->String {
    return NSLocalizedString(key, comment: "")
}


//MARK: - Colors
public func hexStringToUIColor (hex:String) -> UIColor {
    var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
    
    if (cString.hasPrefix("#")) {
        cString.remove(at: cString.startIndex)
    }
    
    if ((cString.count) != 6) {
        return UIColor.gray
    }
    
    var rgbValue:UInt32 = 0
    Scanner(string: cString).scanHexInt32(&rgbValue)
    
    return UIColor(
        red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
        green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
        blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
        alpha: CGFloat(1.0)
    )
}

// MARK: - Convert Arabic numbers
public func getNSNumberFromString(_ text : String) -> NSNumber? {
    return NumberFormatter().number(from: text)
}

public func intFromString(_ text : String?) -> Int? {
    if text == nil { return nil }
    if let n = getNSNumberFromString(text!) { return n.intValue } else { return nil }
}
public func uintFromString(_ text : String?) -> UInt? {
    if text == nil { return nil }
    if let n = getNSNumberFromString(text!) { return n.intValue >= 0 ? n.uintValue : nil } else { return nil }
}
public func doubleFromString(_ text : String?) -> Double? {
    if text == nil { return nil }
    if let n = getNSNumberFromString(text!) { return n.doubleValue } else { return nil }
}
public func decimalFromString(_ text : String?) -> NSDecimalNumber? {
    if text == nil { return nil }
    if let n = getNSNumberFromString(text!) { return NSDecimalNumber(decimal: n.decimalValue) } else { return nil }
}

//MARK: - uilabel
extension UILabel {
    //check if the uilabel truncated
    func isTruncated() -> Bool {
        
        if let string = self.text {
            
            let size: CGSize = (string as NSString).boundingRect(
                with: CGSize(width: self.frame.size.width, height: CGFloat.greatestFiniteMagnitude),
                options: NSStringDrawingOptions.usesLineFragmentOrigin,
                attributes: [NSAttributedString.Key.font: self.font],
                context: nil).size
            
            return (size.height > self.bounds.size.height)
        }
        
        return false
    }
    
}


extension UIViewController
{
    
    func textAllignment(lable : UILabel)
    {
    if UIApplication.isRTL()  {
    lable.textAlignment = .right
    return
    }
    else
    {
     lable.textAlignment  = .left
    return
    }

    }
    func textAllignment(textview : UITextView)
    {
        if UIApplication.isRTL()  {
            textview.textAlignment = .right
            return
        }
        else
        {
            textview.textAlignment  = .left
            return
        }
        
    }
    
    /// flip any view 180 degree in arabic language
    ///
    /// - Parameter view: any view to flip
    public func flip(view:UIView){
        if UIApplication.isRTL()
        {
            UIView.animate(withDuration: 0.25, animations: {
                view.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
            })
        }
    }
    
    public func displayLoginAlert(forController controller: UIViewController? = nil, withMessage message: String? = nil) {
        guard let controller = controller ?? getCurrentViewController() else {
            return
        }
        
        let alert = UIAlertController(title: "", message: message ?? localizedSitringFor(key: "loginFirst"), preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: localizedSitringFor(key: "login"), style: .default, handler: { (_) in
            goToView(withId: "LoginViewController", fromController: controller)
        }))
        
        alert.addAction(UIAlertAction(title: localizedSitringFor(key: "ok"), style: UIAlertAction.Style.default, handler: nil))
        
        mainQueue {
            controller.present(alert, animated: true, completion: nil)
        }
    }
}



/// bold part of label
public func attributedText(withString string: String, boldString: String, font: UIFont) -> NSAttributedString {
    let attributedString = NSMutableAttributedString(string: string,
                                                 attributes: [NSAttributedString.Key.font: font])
    let boldFontAttribute: [NSAttributedString.Key: Any] = [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: font.pointSize)]
    let range = (string as NSString).range(of: boldString)
    attributedString.addAttributes(boldFontAttribute, range: range)
    return attributedString
}

/// change background color for uiview nad change image for specific image view
///
/// - Parameters:
///   - view: UIView which its background color will be changed
///   - red: red value
///   - green: green value
///   - blue: blue value
///   - image: name of image
///   - imageView: UIImageView which its image will be changed
func changeBackGroundColor(forView view: AnimatableView, withRed red:Double , green: Double, blue: Double, andImage image: String = "", forimageView imageView: UIImageView = UIImageView())
{
    view.borderColor = UIColor(red: CGFloat(red/255.0), green: CGFloat(green/255.0), blue: CGFloat(blue/255.0), alpha: 1.0)
    imageView.image = UIImage(named: image)
}


extension String
{
    func replace(target: String, withString: String) -> String
    {
        return self.replacingOccurrences(of: target, with: withString, options: NSString.CompareOptions.literal, range: nil)
    }
}

extension String {
    
    //right is the first encountered string after left
    func between(_ left: String, _ right: String) -> String? {
        guard
            let leftRange = range(of: left), let rightRange = range(of: right, options: .backwards)
            , leftRange.upperBound <= rightRange.lowerBound
            else { return nil }
        
        let sub = self[leftRange.upperBound...]
        let closestToLeftRange = sub.range(of: right)!
        return String(sub[..<closestToLeftRange.lowerBound])
    }
    
    var length: Int {
        get {
            return self.count
        }
    }
    
    func substring(to : Int) -> String {
        let toIndex = self.index(self.startIndex, offsetBy: to)
        return String(self[...toIndex])
    }
    
    func substring(from : Int) -> String {
        let fromIndex = self.index(self.startIndex, offsetBy: from)
        return String(self[fromIndex...])
    }
    
    func substring(_ r: Range<Int>) -> String {
        let fromIndex = self.index(self.startIndex, offsetBy: r.lowerBound)
        let toIndex = self.index(self.startIndex, offsetBy: r.upperBound)
        let indexRange = Range<String.Index>(uncheckedBounds: (lower: fromIndex, upper: toIndex))
        return String(self[indexRange])
    }
    
    func character(_ at: Int) -> Character {
        return self[self.index(self.startIndex, offsetBy: at)]
    }
    
    func lastIndexOfCharacter(_ c: Character) -> Int? {
        return range(of: String(c), options: .backwards)?.lowerBound.encodedOffset
    }
    
    
    
    func containsWithoutIgnoringCase(find: String) -> Bool{
        return self.range(of: find) != nil
    }
    
    func containsIgnoringCase(find: String) -> Bool{
        return self.range(of: find, options: .caseInsensitive) != nil
    }
    
    func haveTextInField() -> Bool {
        if (self.count>=1) {
            return true
        }
        
        return false
    }
    
    func capitalizingFirstLetter() -> String {
        return prefix(1).uppercased() + self.lowercased().dropFirst()
    }
    
    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
}



extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}

extension UIView{
    func addConstraintWithFormat(format: String, views: UIView...){
        var viewsDictionary = [String:UIView]()
        for (index,view) in views.enumerated(){
            let key = "v\(index)"
            view.translatesAutoresizingMaskIntoConstraints = false
            viewsDictionary[key] = view
        }
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: format, options: NSLayoutConstraint.FormatOptions(), metrics: nil, views: viewsDictionary))
    }
}


extension UIImage {
    func createSelectionIndicator(color: UIColor, size: CGSize, lineWidth: CGFloat) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        color.setFill()
        UIRectFill(CGRect(x: 0, y: size.height - lineWidth, width: size.width, height: lineWidth))
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
}


public func handleError(forResponse response: DataResponse<String>){

    if response.response?.statusCode != 200
    {
        var myTextToShow : String = ""
        
        let data = response.data
        do {
            let regResp = try JSONDecoder().decode(ErrorResponse.self, from: data!)
            let myResp : ErrorResponse = regResp
            
            var utf8Text = String(data: data!, encoding: .utf8)
            
            utf8Text = utf8Text!.replacingOccurrences(of: "\r", with: "")
            utf8Text = utf8Text!.replacingOccurrences(of: "\n", with: "")
            utf8Text = utf8Text!.replacingOccurrences(of: "\t", with: "")
            utf8Text = utf8Text!.replacingOccurrences(of: "\"", with: "")
            
            print("utf8Text=2===\(utf8Text!)")
            
            var nArr1 = utf8Text!.split(separator: "]")
            
            if (nArr1.count>=1) {
                utf8Text = "\(nArr1[0])"
                
                print("utf8Text=3===\(utf8Text!)")
                
                var nArr2 = utf8Text!.split(separator: "[")
                
                if (nArr2.count>=2) {
                    utf8Text = "\(nArr2[1])"
                }
                
            }
            
            utf8Text = utf8Text!.trimmingCharacters(in: .whitespacesAndNewlines)
            
            
            myTextToShow = utf8Text!
            
        }  catch let jsonErr {
            print("jsonErr=2==\(jsonErr)")
            UserDefaults.standard.set("\((response.response?.statusCode)!)", forKey: "AppStatusCode")
            UserDefaults.standard.synchronize()
        }
        
        displayMessage(message: myTextToShow, messageError: true)
        return
    }
}


//MARK: - handle message toast
public func displayMessage(message: String, messageError: Bool) {
    
    let view = MessageView.viewFromNib(layout: MessageView.Layout.messageView)
    if messageError == true {
        view.configureTheme(.error)
    } else {
        view.configureTheme(.success)
    }
    
    view.iconImageView?.isHidden = true
    view.iconLabel?.isHidden = true
    view.titleLabel?.isHidden = true
    view.bodyLabel?.text = message
    view.titleLabel?.textColor = UIColor.white
    view.bodyLabel?.textColor = UIColor.white
    view.button?.isHidden = true
    
    var config = SwiftMessages.Config()
    config.presentationStyle = .bottom
    SwiftMessages.show(config: config, view: view)
}

public func displayMessageWithTime(message: String, messageError: Bool, time:TimeInterval ) {
    
    let view = MessageView.viewFromNib(layout: MessageView.Layout.messageView)
    if messageError == true {
        view.configureTheme(.error)
    } else {
        view.configureTheme(.success)
    }
    
    view.iconImageView?.isHidden = true
    view.iconLabel?.isHidden = true
    view.titleLabel?.isHidden = true
    view.bodyLabel?.text = message
    view.titleLabel?.textColor = UIColor.white
    view.bodyLabel?.textColor = UIColor.white
    view.button?.isHidden = true
    
    var config = SwiftMessages.Config()
    config.duration = .seconds(seconds: time)
    config.presentationStyle = .bottom
    SwiftMessages.show(config: config, view: view)
}



extension Data {
    var html2AttributedString: NSAttributedString? {
        do {
            return try NSAttributedString(data: self, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            print("error:", error)
            return  nil
        }
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
}

extension String {
    var html2AttributedString: NSAttributedString? {
        return Data(utf8).html2AttributedString
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
}


/// get date as string from Seconds since 1970
///
/// - Parameters:
///   - fromSeconds: the seconds from 1970
///   - format: the date string format
/// - Returns: new date string
public func getDate(fromSeconds:Double, withFormat format:String)-> String {
    return getDateFormatter(withFormat: format).string(from: Date(timeIntervalSince1970: fromSeconds))
}


/// get DateFormatter object in english locale
///
/// - Parameter format: the formatter you want , the default value is "dd-MM-yyyy-hh:mm:ss"
/// - Returns: the DateFormatter object
public func getDateFormatter(withFormat format:String? = nil) -> DateFormatter {
    let format:String = format == nil ? "dd-MM-yyyy-hh:mm:ss" : format!
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = format
    dateFormatter.locale = Locale(identifier: "en")
    return dateFormatter
}



extension UISearchBar{
    
    func getTextField() -> UITextField? {
        if #available(iOS 13.0, *) {
            print("ios 13")
            return value(forKey: "searchField") as? UITextField
        }else {
            // Fallback on earlier versions
            print("ios 12")
            for view : UIView in (self.subviews[0]).subviews {
                print(view)
                if let textField = view as? UITextField {
                    print("inside found aho")
                    return textField
                }
            }
        }
        return nil
    }
    
    func setTextField(color: UIColor) {
        print("im here")
        guard let textField = getTextField() else { return }
        switch searchBarStyle {
        case .minimal:
            print("inside")
            textField.layer.backgroundColor = color.cgColor
            textField.layer.cornerRadius = 6
        case .prominent, .default: textField.backgroundColor = color
        @unknown default: break
        }
    }
}


public extension String {
    
    func indexInt(of char: Character) -> Int? {
        return firstIndex(of: char)?.utf16Offset(in: self)
    } 
}


extension Numeric{
    // func to return number as desire format
    func format(numberStyle: NumberFormatter.Style = NumberFormatter.Style.decimal, groupingSeparator: String = ",", decimalSeparator: String = ".") -> String? {
        if let num = self as? NSNumber {
            let formater = NumberFormatter()
            formater.numberStyle = numberStyle
            formater.groupingSeparator = groupingSeparator
            formater.decimalSeparator = decimalSeparator
            return formater.string(from: num)
        }
        return nil
    }
}


//extension Double {
//    /// Rounds the double to decimal places value
//    func rounded(toPlaces places:Int) -> Double {
//        let divisor = pow(10.0, Double(places))
//        return (self * divisor).rounded() / divisor
//    }
//}
