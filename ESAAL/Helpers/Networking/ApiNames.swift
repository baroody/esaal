//
//  ApiNames
//  ESAAL
//
//  Created by Mina Thabet on 7/14/19.
//  Copyright © 2019 HardTask. All rights reserved.
//


import UIKit


/*
 use this struct to add any api to use it in the project
 */
struct ApiNames {

    static let LOGIN: String = Config.BASEURL + "LogIn"
    static let FORGET_PASSWORD: String = Config.BASEURL + "ForgetPassword"
    static let CHANGE_PASSWORD: String = Config.BASEURL + "ChangePassword"
    static let MATERIALS: String = Config.BASEURL + "Matrials"
    static let SLIDER_PHOTO: String = Config.BASEURL + "SliderPhoto"
    static let CONTACT_US: String = Config.BASEURL + "ContactUs"
    static let USER_MATERIAL: String = Config.BASEURL + "UserMatrials"
    static let ABOUT_US: String = Config.BASEURL + "AboutUs"
    static let COUNTRIES: String = Config.BASEURL + "Countries"
    static let SIGN_UP_STUDENT: String = Config.BASEURL + "SignUpStudent"
    static let SIGN_UP_TEACHER: String = Config.BASEURL + "SignUpTeacher"
    static let GET_USER_DATA: String = Config.BASEURL + "GetUserData"
    static let UPDATE_STUDENT: String = Config.BASEURL + "UpdateStudent"
    static let UPDATE_TEACHER: String = Config.BASEURL + "UpdateTeacher"
    static let REQUEST_CREDIT: String = Config.BASEURL + "RequestCredit"
    static let TEACHER_CREDIT: String = Config.BASEURL + "TeacherCridit"
    static let HIDE_NOTIFICATIONS: String = Config.BASEURL + "HideNotifications"
    static let GET_USER_NOTIFICATIONS: String = Config.BASEURL + "GetUserNotifications"
    static let UPDATE_USER_NOTIFICATIONS: String = Config.BASEURL + "UpdateUserNotifications"
    static let ADD_QUESTIONS: String = Config.BASEURL + "AddQuestion"
    static let GET_QUESTION: String = Config.BASEURL + "GetQuestion"
    static let ADD_REPLY: String = Config.BASEURL + "AddReplay"
    static let GET_USER_QUESTIONS_BY_MATERIALS: String = Config.BASEURL + "GetUserQuestionsByMaterials"
    static let GET_USER_QUESTIONS: String = Config.BASEURL + "GetUserQuestions"
    static let GET_USER_SEARCH_QUESTIONS: String = Config.BASEURL + "GetUserSearshQuestions"
    static let LIKE_REPLY: String = Config.BASEURL + "LikeReplay"
    static let DIS_LIKE_REPLY: String = Config.BASEURL + "DislikeReplay"
    static let DEALETE_QUESTIONS_ATTACHMENT: String = Config.BASEURL + "DeleteQuestionAttachment"
    static let UPDATE_QUESTION: String = Config.BASEURL + "UpdateQuestion"
    static let PEND_QUESTION: String = Config.BASEURL + "PendQuestion"
    static let GET_SUBSCRIBTIONS: String = Config.BASEURL + "GetSubscriptions"
    static let GET_USER_SUBSCRIBTIONS: String = Config.BASEURL + "GetUserSubscriptions"
    static let MAKE_SUBSCRIBTIONS: String = Config.BASEURL + "MakeSubscription"
    static let REMOVE_PEND_QUESTION: String = Config.BASEURL + "RemovePendQuestion"
    static let TOKEN: String = Config.BASEURL + "AddDevicetoken"
    static let LOGOUT: String = Config.BASEURL + "logout"
    static let Appsetting: String = Config.BASEURL + "Appsetting"
    static let userDummyPackageURL : String = Config.BASEURL + "MakeDummySubscription"
}

