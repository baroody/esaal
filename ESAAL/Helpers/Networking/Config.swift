//
//  Config
//  ESAAL
//
//  Created by Mina Thabet on 7/14/19.
//  Copyright © 2019 HardTask. All rights reserved.
//


import UIKit


/*
 here put the statics for all project
 */
struct Config {
    
    
//    static let BASEURL:String = "http://192.168.1.106:802/api/"
    static let BASEURL:String = "https://esaalapp.com/api/"
    static let BASIMAGEURL:String = ""
    static let API_TOKEN: String = "Bearer " + Account.shared.Token
}
