//
//  Account.swift
//  ESAAL
//
//  Created by YoussefRomany on 11/9/19.
//  Copyright © 2019 HardTask. All rights reserved.
//

import Foundation


class Account{
    
   static let shared = Account()

    var id: Int = 0
    var Token: String = ""
    var userName: String = ""
    var isTeacher: Bool = false
    var isSubscribe: Bool = false
    var isRequestes: Bool = false
    var requestDate: String = ""
    var showPayment: Bool = false
    var onlineAppVersion: String = ""
    
    init(){
        getStoredData()
    }
    
    func storeData(){
        sharedPref.shared.setSharedValue("id", value: self.id )
        sharedPref.shared.setSharedValue("Token", value: self.Token)
        sharedPref.shared.setSharedValue("userName", value: self.userName)
        sharedPref.shared.setSharedValue("isTeacher", value: self.isTeacher)
        sharedPref.shared.setSharedValue("isSubscribe", value: self.isSubscribe)
        sharedPref.shared.setSharedValue("isRequestes", value: self.isRequestes)
        sharedPref.shared.setSharedValue("requestDate", value: self.requestDate)
        sharedPref.shared.setSharedValue("showPayment", value: self.showPayment)
        sharedPref.shared.setSharedValue("onlineAppVersion", value: self.onlineAppVersion)
    }
    
    func getStoredData(){
        self.id = sharedPref.shared.getSharedValue(forKey: "id") as? Int ?? 0
        self.Token = sharedPref.shared.getSharedValue(forKey: "Token") as? String ?? ""
        self.userName = sharedPref.shared.getSharedValue(forKey: "userName") as? String ?? ""
        self.isTeacher = sharedPref.shared.getSharedValue(forKey: "isTeacher") as? Bool ?? true
        self.isSubscribe = sharedPref.shared.getSharedValue(forKey: "isSubscribe") as? Bool ?? false
        self.isRequestes = sharedPref.shared.getSharedValue(forKey: "isRequestes") as? Bool ?? false
        self.requestDate = sharedPref.shared.getSharedValue(forKey: "requestDate") as? String ?? ""
        self.showPayment = sharedPref.shared.getSharedValue(forKey: "showPayment") as? Bool ?? false
        self.onlineAppVersion = sharedPref.shared.getSharedValue(forKey: "onlineAppVersion") as? String ?? ""
    }
    
    func logout(){
        sharedPref.shared.removeValue(forKey: "id")
        sharedPref.shared.removeValue(forKey: "Token")
        sharedPref.shared.removeValue(forKey: "userName")
        sharedPref.shared.removeValue(forKey: "isTeacher")
        sharedPref.shared.removeValue(forKey: "isSubscribe")
        sharedPref.shared.removeValue(forKey: "isRequestes")
        sharedPref.shared.removeValue(forKey: "requestDate")
        getStoredData()
    }

}

