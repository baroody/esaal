/* 
Copyright (c) 2019 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct NewNotification : Codable {
	let userNotificationId : Int?
	let arabicMessage : String?
	let englishMessage : String?
	let isRead : Bool?
	let requestQuestionId : Int?
	let materialArabicName : String?
	let materialEnglishName : String?
	let creationDate : String?
    let questionDescription: String?
    let isQuestion: Bool?
    
    var message: String?{
        if L102Language.isRTL{
            return arabicMessage
        }else{
            return englishMessage
        }
    }
    
    var material: String?{
        if L102Language.isRTL{
            return materialArabicName
        }else{
            return materialEnglishName
        }
    }
    

	enum CodingKeys: String, CodingKey {

		case userNotificationId = "userNotificationId"
		case arabicMessage = "arabicMessage"
		case englishMessage = "englishMessage"
		case isRead = "isRead"
		case requestQuestionId = "requestQuestionId"
		case materialArabicName = "materialArabicName"
		case materialEnglishName = "materialEnglishName"
		case creationDate = "creationDate"
        case questionDescription = "questionDescription"
        case isQuestion = "isQuestion"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		userNotificationId = try values.decodeIfPresent(Int.self, forKey: .userNotificationId)
		arabicMessage = try values.decodeIfPresent(String.self, forKey: .arabicMessage)
		englishMessage = try values.decodeIfPresent(String.self, forKey: .englishMessage)
		isRead = try values.decodeIfPresent(Bool.self, forKey: .isRead)
		requestQuestionId = try values.decodeIfPresent(Int.self, forKey: .requestQuestionId)
		materialArabicName = try values.decodeIfPresent(String.self, forKey: .materialArabicName)
		materialEnglishName = try values.decodeIfPresent(String.self, forKey: .materialEnglishName)
		creationDate = try values.decodeIfPresent(String.self, forKey: .creationDate)
        questionDescription = try values.decodeIfPresent(String.self, forKey: .questionDescription)
        isQuestion = try values.decodeIfPresent(Bool.self, forKey: .isQuestion)
	}

}
