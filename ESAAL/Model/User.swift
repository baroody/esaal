/* 
Copyright (c) 2019 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct User : Codable {
	let id : Int?
	let firstName : String?
	let middleName : String?
	let lastName : String?
	let mobile : String?
	let email : String?
	let userName : String?
	let password : String?
	let photoUrl : String?
	let arabicAbout : String?
	let description : String?
	let eductionPhaseId : String?
	let key : String?
	let token : String?
	let isMaster : Bool?
	let isTeacher : Bool?
	let isSuper : Bool?
	let countryId : Int?
	let iban : String?
	let swiftCode : String?
	let accountNumber : String?
	let bankName : String?
	let bankAddress : String?
	let personalAddress : String?
	let isRequest : Bool?
	let requestDate : String?
	let code : String?
	let createdBy : Int?
	let creationDate : String?
	let modifiedBy : Int?
	let modificationDate : String?
	let isActive : Bool?
	let isDeleted : Bool?

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case firstName = "firstName"
		case middleName = "middleName"
		case lastName = "lastName"
		case mobile = "mobile"
		case email = "email"
		case userName = "userName"
		case password = "password"
		case photoUrl = "photoUrl"
		case arabicAbout = "arabicAbout"
		case description = "description"
		case eductionPhaseId = "eductionPhaseId"
		case key = "key"
		case token = "token"
		case isMaster = "isMaster"
		case isTeacher = "isTeacher"
		case isSuper = "isSuper"
		case countryId = "countryId"
		case iban = "iban"
		case swiftCode = "swiftCode"
		case accountNumber = "accountNumber"
		case bankName = "bankName"
		case bankAddress = "bankAddress"
		case personalAddress = "personalAddress"
		case isRequest = "isRequest"
		case requestDate = "requestDate"
		case code = "code"
		case createdBy = "createdBy"
		case creationDate = "creationDate"
		case modifiedBy = "modifiedBy"
		case modificationDate = "modificationDate"
		case isActive = "isActive"
		case isDeleted = "isDeleted"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
		firstName = try values.decodeIfPresent(String.self, forKey: .firstName)
		middleName = try values.decodeIfPresent(String.self, forKey: .middleName)
		lastName = try values.decodeIfPresent(String.self, forKey: .lastName)
		mobile = try values.decodeIfPresent(String.self, forKey: .mobile)
		email = try values.decodeIfPresent(String.self, forKey: .email)
		userName = try values.decodeIfPresent(String.self, forKey: .userName)
		password = try values.decodeIfPresent(String.self, forKey: .password)
		photoUrl = try values.decodeIfPresent(String.self, forKey: .photoUrl)
		arabicAbout = try values.decodeIfPresent(String.self, forKey: .arabicAbout)
		description = try values.decodeIfPresent(String.self, forKey: .description)
		eductionPhaseId = try values.decodeIfPresent(String.self, forKey: .eductionPhaseId)
		key = try values.decodeIfPresent(String.self, forKey: .key)
		token = try values.decodeIfPresent(String.self, forKey: .token)
		isMaster = try values.decodeIfPresent(Bool.self, forKey: .isMaster)
		isTeacher = try values.decodeIfPresent(Bool.self, forKey: .isTeacher)
		isSuper = try values.decodeIfPresent(Bool.self, forKey: .isSuper)
		countryId = try values.decodeIfPresent(Int.self, forKey: .countryId)
		iban = try values.decodeIfPresent(String.self, forKey: .iban)
		swiftCode = try values.decodeIfPresent(String.self, forKey: .swiftCode)
		accountNumber = try values.decodeIfPresent(String.self, forKey: .accountNumber)
		bankName = try values.decodeIfPresent(String.self, forKey: .bankName)
		bankAddress = try values.decodeIfPresent(String.self, forKey: .bankAddress)
		personalAddress = try values.decodeIfPresent(String.self, forKey: .personalAddress)
		isRequest = try values.decodeIfPresent(Bool.self, forKey: .isRequest)
		requestDate = try values.decodeIfPresent(String.self, forKey: .requestDate)
		code = try values.decodeIfPresent(String.self, forKey: .code)
		createdBy = try values.decodeIfPresent(Int.self, forKey: .createdBy)
		creationDate = try values.decodeIfPresent(String.self, forKey: .creationDate)
		modifiedBy = try values.decodeIfPresent(Int.self, forKey: .modifiedBy)
		modificationDate = try values.decodeIfPresent(String.self, forKey: .modificationDate)
		isActive = try values.decodeIfPresent(Bool.self, forKey: .isActive)
		isDeleted = try values.decodeIfPresent(Bool.self, forKey: .isDeleted)
	}

}

