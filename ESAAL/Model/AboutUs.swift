
import Foundation
struct AboutUs : Codable {

    let aboutUsAr : String?
    let aboutUsEn : String?
    let studentTermsConditionAr : String?
    let studentTermsConditionEn : String?
    let teacherTermsConditionAr : String?
    let teacherTermsConditionEn : String?
    
    var aboutUs: String? {
         if L102Language.isRTL{
            return aboutUsAr
         }else {
            return aboutUsEn
        }
    }
    
    var studentTerms: String? {
         if L102Language.isRTL{
            return studentTermsConditionAr
         }else {
            return studentTermsConditionEn
        }
    }
    
    var teacherTerms: String? {
         if L102Language.isRTL{
            return teacherTermsConditionAr
         }else {
            return teacherTermsConditionEn
        }
    }

    enum CodingKeys: String, CodingKey {

        case aboutUsAr = "aboutUsAr"
        case aboutUsEn = "aboutUsEn"
        case studentTermsConditionAr = "studentTermsConditionAr"
        case studentTermsConditionEn = "studentTermsConditionEn"
        case teacherTermsConditionAr = "teacherTermsConditionAr"
        case teacherTermsConditionEn = "teacherTermsConditionEn"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        aboutUsAr = try values.decodeIfPresent(String.self, forKey: .aboutUsAr)
        aboutUsEn = try values.decodeIfPresent(String.self, forKey: .aboutUsEn)
        studentTermsConditionAr = try values.decodeIfPresent(String.self, forKey: .studentTermsConditionAr)
        studentTermsConditionEn = try values.decodeIfPresent(String.self, forKey: .studentTermsConditionEn)
        teacherTermsConditionAr = try values.decodeIfPresent(String.self, forKey: .teacherTermsConditionAr)
        teacherTermsConditionEn = try values.decodeIfPresent(String.self, forKey: .teacherTermsConditionEn)
    }
}
