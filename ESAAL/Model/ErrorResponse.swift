
struct ErrorResponse : Encodable, Decodable {
	var errors : Errors?
}


struct Errors : Encodable, Decodable {
    
    var Dto_Email : [String]?
    var Dto_Password : [String]?

    enum CodingKeys: String, CodingKey {
        
        case Dto_Email = "Dto.Email"
        case Dto_Password = "Dto.Password"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        Dto_Email = try values.decodeIfPresent([String].self, forKey: .Dto_Email)
        Dto_Password = try values.decodeIfPresent([String].self, forKey: .Dto_Password)
    }
}
