//
//  Enum.swift
//  ESAAL
//
//  Created by Mina Thabet on 11/18/19.
//  Copyright © 2019 HardTask. All rights reserved.
//

import Foundation

enum ActionType: String {
    case Edit = "Edit"
    case Replay = "Replay"
    case Reservation = "Reservation"
    case Reserved = "Reserved"
    case Cancel = "Cancel"
}

enum HtmlType: String {
    case AboutUs = "AboutUs"
    case StudentTerms = "StudentTerms"
    case TeatcherTerms = "TeatcherTerms"
    case All = "All"
}
