//
//  Matrial.swift
//  ESAAL
//
//  Created by Mina Thabet on 10/29/19.
//  Copyright © 2019 HardTask. All rights reserved.
//

import Foundation

struct Matrial : Codable {
	let id : Int?
	let arabicName : String?
	let englishName : String?
	let questionPrice : Int?
	let teacherPercentPerQuestion : Int?
	let code : String?
	let createdBy : Int?
	let creationDate : String?
	let modifiedBy : Int?
	let modificationDate : String?
	let isActive : Bool?
	let isDeleted : Bool?
    
    var name:String? {
        if L102Language.isRTL {
            return arabicName
        }else {
            return englishName
        }
    }

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case arabicName = "arabicName"
		case englishName = "englishName"
		case questionPrice = "questionPrice"
		case teacherPercentPerQuestion = "teacherPercentPerQuestion"
		case code = "code"
		case createdBy = "createdBy"
		case creationDate = "creationDate"
		case modifiedBy = "modifiedBy"
		case modificationDate = "modificationDate"
		case isActive = "isActive"
		case isDeleted = "isDeleted"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
		arabicName = try values.decodeIfPresent(String.self, forKey: .arabicName)
		englishName = try values.decodeIfPresent(String.self, forKey: .englishName)
		questionPrice = try values.decodeIfPresent(Int.self, forKey: .questionPrice)
		teacherPercentPerQuestion = try values.decodeIfPresent(Int.self, forKey: .teacherPercentPerQuestion)
		code = try values.decodeIfPresent(String.self, forKey: .code)
		createdBy = try values.decodeIfPresent(Int.self, forKey: .createdBy)
		creationDate = try values.decodeIfPresent(String.self, forKey: .creationDate)
		modifiedBy = try values.decodeIfPresent(Int.self, forKey: .modifiedBy)
		modificationDate = try values.decodeIfPresent(String.self, forKey: .modificationDate)
		isActive = try values.decodeIfPresent(Bool.self, forKey: .isActive)
		isDeleted = try values.decodeIfPresent(Bool.self, forKey: .isDeleted)
	}
    
    init(id:Int, arabicName:String, englishName:String) {
        self.id = id
        self.arabicName = arabicName
        self.englishName = englishName
        questionPrice = 0
        teacherPercentPerQuestion = 0
        code = ""
        createdBy = 0
        creationDate = ""
        modifiedBy = 0
        modificationDate = ""
        isActive = true
        isDeleted = true
    }

}
