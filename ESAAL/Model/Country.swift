/* 
Copyright (c) 2019 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct Country : Codable {
	let id : Int?
	let arabicName : String?
	let englishName : String?
	let isGulfCountry : Bool?
	let code : String?
	let createdBy : Int?
	let creationDate : String?
	let modifiedBy : Int?
	let modificationDate : String?
	let isActive : Bool?
	let isDeleted : Bool?
    
    var name: String?{
        if L102Language.isRTL{
            return arabicName
        }else{
            return englishName
        }
    }

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case arabicName = "arabicName"
		case englishName = "englishName"
		case isGulfCountry = "isGulfCountry"
		case code = "code"
		case createdBy = "createdBy"
		case creationDate = "creationDate"
		case modifiedBy = "modifiedBy"
		case modificationDate = "modificationDate"
		case isActive = "isActive"
		case isDeleted = "isDeleted"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
		arabicName = try values.decodeIfPresent(String.self, forKey: .arabicName)
		englishName = try values.decodeIfPresent(String.self, forKey: .englishName)
		isGulfCountry = try values.decodeIfPresent(Bool.self, forKey: .isGulfCountry)
		code = try values.decodeIfPresent(String.self, forKey: .code)
		createdBy = try values.decodeIfPresent(Int.self, forKey: .createdBy)
		creationDate = try values.decodeIfPresent(String.self, forKey: .creationDate)
		modifiedBy = try values.decodeIfPresent(Int.self, forKey: .modifiedBy)
		modificationDate = try values.decodeIfPresent(String.self, forKey: .modificationDate)
		isActive = try values.decodeIfPresent(Bool.self, forKey: .isActive)
		isDeleted = try values.decodeIfPresent(Bool.self, forKey: .isDeleted)
	}

}
