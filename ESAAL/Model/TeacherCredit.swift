//
//  TeacherCredit.swift
//  ESAAL
//
//  Created by Mina Thabet on 11/24/19.
//  Copyright © 2019 HardTask. All rights reserved.
//

import Foundation

struct TeacherCredit : Codable {
    
    var totalAmount : Double?
    var totalQuestionAnswer : Int?
    var isRequest: Bool?

}



