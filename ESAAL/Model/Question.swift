

import Foundation
struct Question : Codable {
	let id : Int?
	let materialId : Int?
	let userId : Int?
	let subject : String?
	let description : String?
	let attachment : [Attachement]?
    var replayQuestions : [ReplayQuestions]?
	let material : Material?
	var isPending : Bool?
	var pendingUserId : Int?
	var pendingDate : String?
	var pendingEndDate : String?
	var remainTime : Double?
	let code : String?
	let createdBy : Int?
	let creationDate : String?
	let modifiedBy : Int?
	let modificationDate : String?
	let isActive : Bool?
	let isDeleted : Bool?

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case materialId = "materialId"
		case userId = "userId"
		case subject = "subject"
		case description = "description"
		case attachment = "attachment"
		case replayQuestions = "replayQuestions"
		case material = "material"
		case isPending = "isPending"
		case pendingUserId = "pendingUserId"
		case pendingDate = "pendingDate"
		case pendingEndDate = "pendingEndDate"
		case remainTime = "remainTime"
		case code = "code"
		case createdBy = "createdBy"
		case creationDate = "creationDate"
		case modifiedBy = "modifiedBy"
		case modificationDate = "modificationDate"
		case isActive = "isActive"
		case isDeleted = "isDeleted"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
		materialId = try values.decodeIfPresent(Int.self, forKey: .materialId)
		userId = try values.decodeIfPresent(Int.self, forKey: .userId)
		subject = try values.decodeIfPresent(String.self, forKey: .subject)
		description = try values.decodeIfPresent(String.self, forKey: .description)
		attachment = try values.decodeIfPresent([Attachement].self, forKey: .attachment)
		replayQuestions = try values.decodeIfPresent([ReplayQuestions].self, forKey: .replayQuestions)
		material = try values.decodeIfPresent(Material.self, forKey: .material)
		isPending = try values.decodeIfPresent(Bool.self, forKey: .isPending)
		pendingUserId = try values.decodeIfPresent(Int.self, forKey: .pendingUserId)
		pendingDate = try values.decodeIfPresent(String.self, forKey: .pendingDate)
		pendingEndDate = try values.decodeIfPresent(String.self, forKey: .pendingEndDate)
		remainTime = try values.decodeIfPresent(Double.self, forKey: .remainTime)
		code = try values.decodeIfPresent(String.self, forKey: .code)
		createdBy = try values.decodeIfPresent(Int.self, forKey: .createdBy)
		creationDate = try values.decodeIfPresent(String.self, forKey: .creationDate)
		modifiedBy = try values.decodeIfPresent(Int.self, forKey: .modifiedBy)
		modificationDate = try values.decodeIfPresent(String.self, forKey: .modificationDate)
		isActive = try values.decodeIfPresent(Bool.self, forKey: .isActive)
		isDeleted = try values.decodeIfPresent(Bool.self, forKey: .isDeleted)
	}

}
